<!-- DIALOG REGISTARATOR START -->
<script type='text/javascript' src='libs/js/site_auth.js'></script>

<!--API_START /-->	
	<script type='text/javascript' src='http://vkontakte.ru/js/api/openapi.js'></script>
	<script type='text/javascript' src='http://maps.api.2gis.ru/1.0'></script>
	<script type='text/javascript' src='libs/js/site_connect_api.js'></script>
<!--API_FINISH /-->



<div class='papper' id='license'>
	<h3>Лицензионное соглашение</h3>
	<p>
		<ol>
			<li>Регистрация участника автоматизированной системы контроля спортсменов на сайте происходит добровольно</li>
		</ol>
	</p>
	<hr>
	<p style='text-align: center;'>
		<button id='but_license'>Я принимаю условия лицензионного соглашения</button>
		<button href='index.php'>Не принимаю</button>
	</p>
</div>


<form id="regForm" method="post" action="" hidden>
<fieldset>
	<div id='div_osn_info'>
		<h4>Основная информация:</h4>	
			<input type='hidden' value='' id='reg_id_user'>
		<p>
			<label class='for_info' for='reg_telephone'>Номер мобильного телефона*:</label>
			<input required name='reg_telephone' id='reg_telephone' type='tel'>
			<img src='images/ajax_load.gif' hidden id='img_ajax_checktel'>
			<span class='for_info' for='reg_telephone'>Федеральный номер</span>
			<span class='for_info error' id='err_telephone' >Зарегестрирован.</span>
		</p>
		<p>
			<label class='for_info' for='reg_name'>Фамилия*:</label>
			<input required id='reg_name' name='reg_name'>
			<img src='images/ajax_load.gif' hidden id='img_ajax_name'>
			<span class='for_info' for='reg_name'>Фамилия русскими буквами</span>
			
		</p>
		<p>
			<label class='for_info' for='reg_second_name'>Имя*:</label>
			<input required id='reg_second_name' name='reg_second_name' >
			<span class='for_info' for='reg_second_name'>Полное имя русскими буквами</span>
		</p>
		<p>
			<label class='for_info' for='reg_third_name'>Отчество:</label>
			<input id='reg_third_name' name='reg_third_name'>
		</p>
		<p>
			<label class='for_info' for='vk_auth'>Авторизация вконтакте:</label>
			<div id="vk_auth"></div>
			<input id='reg_vk_id' type='hidden' value=''>
		</p>
	</div>
	
	<div id='div_reg_init' hidden>
		<h4>Возможные синхронизации:</h4>
	</div>
		
	<div id='div_dop_info'>
		<h4>Дополнительно:</h4>	
		<div class='div_adress'>
			<p>
				<label class='for_info' for='reg_city'>Город проживания*:</label>
				<input required class='org_city auto_city' required type='text' name='reg_city'  data-theme='dialog' id='reg_city'>
				<input type='hidden' class='geo_city_title' name='geo_city_title' id='geo_city_title'>
				<input type='hidden' class='geo_index' name='reg_adress_index' id='reg_adress_index'>
				<input type='hidden' class='geo_id' name='reg_adress_id' id='reg_adress_id' disabled>
			</p>
			<p>
				<label class='for_info' for='reg_adress_title'>Улица или м/район*:</label>
				<input required class='geo_title' name='reg_adress_title' id='reg_adress_title'  style=''> 
				<img src='images/ajax_load.gif' hidden id='img_ajax_adress'>
				<span class='for_info' for='reg_name'Обязательное условие</span>
			</p>
			<p>
				<label class='for_info' for='reg_adress_house'>Номер дома и квартиры*:</label>
				<input required class='geo_house' name='reg_adress_house' id='reg_adress_house'> - 
				<input type='number' min='1' max='300' step='1' class='geo_flat' name='reg_adress_flat' id='reg_adress_flat'>
			</p>
		</div>
		<p>
			<label class='for_info' for='reg_born'>Дата рождения*:</label>
			<input required type='text' name='reg_born' id='reg_born' class='datepicker'>
			<span class='for_info' for='reg_born'>В формате дд.мм.гггг</span>
		</p>
		<p>
			<label class='for_info' for='reg_sex'>Пол*:</label>
			<select required name='reg_sex' id='reg_sex'>
				<option value=''>НЕ УКАЗАНО</option>
				<option value='1'>Мужской</option>
				<option value='0'>Женский</option>
			</select>
		</p>
		
	</div>
	<div id='div_reg_sms'>
		<h4>Подтверждение профиля по SMS</h4>
		<p>
			<label class='for_info' for='reg_born'>Код подтверждения:</label>
			<input type='text' name='reg_sms' id='reg_sms' disabled='disabled'>
			<span class='for_info' for='reg_sms'>5 символов из SMS</span>
		</p>
	</div>
</fieldset>

<div style='margin: 20px auto; text-align: center; width: 100%'>
	<button id='but_reg_sms'>Выслать код подтверждения</button>
	<button id='but_reg'>Зерегестрироваться</button>		
</div>




	





						<!-- DIALOG REGISTARATOR FINISH -->
						