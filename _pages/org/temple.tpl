<? if($_GET[type]='section' && $_GET[id]>0){?>

	<script type="text/javascript" src="http://maps.api.2gis.ru/1.0"></script>
	<script type="text/javascript" src="/libs/js/site_connet_api.js"></script>
    <script type="text/javascript">
        // Создаем обработчик загрузки страницы:
        DG.autoload(function() {
			// Создаем объект карты, связанный с контейнером:
			var myMap = new DG.Map('sectionMap');
			// Добавляем элемент управления коэффициентом масштабирования:
			myMap.controls.add(new DG.Controls.Zoom());
			// Добавляем элемент управления полноэкранным режимом отображения карты:
			myMap.controls.add(new DG.Controls.FullScreen());
			// Включение выделения области на карте правой кнопкой мыши:
			myMap.enableRightButtonMagnifier();
			
			
			
		function gis_geocode(query, limit){
			limit = limit || 1;
	
			DG.Geocoder.get(query, {
				types: ['house'],
				limit: limit,
				success: function(geocoderObjects) {
				 // Обходим циклом все полученные геообъекты
				 for(var i = 0, len = geocoderObjects.length; i < len; i++) {
					var geocoderObject = geocoderObjects[i];
					var lon = geocoderObject.getCenterGeoPoint().getLon();
					var lat = geocoderObject.getCenterGeoPoint().getLat();
					var CenterGeoPoint = new DG.GeoPoint(lon,lat);
					var marker = new DG.Markers.Common({geoPoint: CenterGeoPoint});
					// Добавим маркер на карту:
					myMap.markers.add(marker);
					 // Получаем маркер из геообъекта с помощью метода getMarker.
					 // Первый параметр - иконка маркера, второй параметр - функция, которая сработает при клике на маркер
					 //var markerIcon = new DG.Icon('http://api.2gis.ru/upload/images/2314.png', new DG.Size(53, 25));
					 //var marker = geocoderObject.getMarker(markerIcon, (function(geocoderObject) {
					//	 return function () {
					//		 var info = '';
		  
							 // Основная информация о геообъекте
					//		 info += 'Type: ' + geocoderObject.getType() + '\n';
					//		 info += 'Name: ' + geocoderObject.getName() + '\n';
					//		 info += 'Short name: ' + geocoderObject.getShortName() + '\n';
		  
							 // Дополнительная информация о геообъекте
					//		 var attributes = geocoderObject.getAttributes();
					//		 for (var attribute in attributes) {
					//			 if (attributes.hasOwnProperty(attribute)) {
					//				  info += attribute + ': ' + attributes[attribute] + '\n';
					//			  }
					//		 }
		  
							 // Географические координаты центра геообъекта
					//		 var centerGeoPoint = geocoderObject.getCenterGeoPoint();
					//		 info += 'Longitude: ' + centerGeoPoint.getLon() + '\n';
					//		 info += 'Latitude: ' + centerGeoPoint.getLat();
		  
					//		alert(info);
					//		}
				//})(geocoderObject));
						
						// Устанавливаем центр карты, и коэффициент масштабирования:
						//myMap.markers.add(marker);
					}
					
					myMap.setCenter(CenterGeoPoint, 15);
					
				},
				 // Обработка ошибок
				 failure: function(code, message) {
					 alert(code + ' ' + message);
				 }
			});
	
	
	};
			
			// Выполнение поиска:
			gis_geocode("<? echo $zal_adress ?>", 1);
		});
	</script>
	
	
	
	
	
	

<div id='info_section'>
	<h4>Расписание</h4>
	<table id='raspisanie'>
		<thead>
			<tr style='background: #e2e4ff; text-align:center;'>
				<th width='130'>Название</th>
				<th width='80'>Возраст</th>
				<th width='80'>Пн</th>
				<th width='80'>Вт</th>
				<th width='80'>Ср</th>
				<th width='80'>Чт</th>
				<th width='80'>Пт</th>
				<th width='80'>Сб</th>
				<th width='80'>Вс</th>
			</tr>
		</thead>
		<tbody>
			<? echo $groups_table ?>
		</tbody>
	</table>

	<h4>Информация о спортивной секции</h4>
	<p><label>Спортклуб: </label><?  echo $club_title;?></p>
	<p><label>Тип спорклуба: </label><?  echo $club_type;?></p>
	<p><label>Адрес: </label><? echo $zal_adress ?></p>
	<p><label>Название зала:</label> <? echo $zal_title ?></p>
	<p><label>Виды спорта:</label><? echo $sports_title ?></p>
	<p><label>Тренеры:</label><? echo $treners ?></p>
	<p><label>Стоимость занятий за месяц:</label><? echo $price ?></p>
	<p><label>Стоимость одного занятия:</label><? echo $price_one ?></p>
	<p><label>Стоимость персональной тренировки:</label><? echo $price_pt ?></p>
	<p><label>Контакный телефон:</label> <? echo $zal_tel  ?></p>
	
	<div id="sectionMap" class='map'></div>

</div>










<? }elseif($_GET[type]='section' && !$_GET[id]){ ?>

	<link rel='stylesheet' type="text/css" media='screen' href='css/jquery.dataTables_themeroller.css'>
	<script type='text/javascript' src='libs/js/jquery.dataTables.min.js'></script>
	<script type='text/javascript' src='libs/js/site_datatable.js'></script>

	<h4 class='h4_show'>Расширенный поиск</h4>
	<div id='search_sportgroup' style='text-align:center;' hidden>
		<form id='search_section' action='/' method='GET'>
			<input type='hidden' name='page' value='org' >
			<input type='hidden' name='type' value='section' >
			<p>
				<label class='for_info' for='sect_city'>Город:</label>
				<input class='auto_city org_city' type='text' data-theme='dialog' name='sect_city'  id='sect_city'>
				<input type='hidden' value='<? echo $_POST[sect_city]?>'  for='sect_city' data-type-token='city'>
			</p>
			<p>
				<label class='for_info' for='sect_rayon'>Район:</label>
				<select name='sect_rayon' id='sect_rayon' disabled>
					<?php echo $search_select_rayon?>
				</select>
			</p>
			<p>
				<label class='for_info' for='sect_sport'>Спортивное направление:</label>
				<?php echo $search_select_sports_type?>
			</p>
			<p>
				<label class='for_info' for='sect_sport'>Виды спорта:</label>
				<?php echo $search_select_sports?>
			</p>
			<button type='submit' class='but_search_full'><?php echo BUT_SEARCH ?></button>
		</form>
	</div>
	
	
	<table id='sections'>
		<thead>
			<tr>
				<th>Город</th>
				<th>Район</th>
				<th>Адрес</th>
				<th>Клуб</th>
				<th>Виды спорта</th>
			</tr>
		</thead>
		<tbody>
			<?php echo $section_table ?>
		</tbody>
	</table>
<? };  ?>