<div id='content_filter'>

	<? if($_ENV['admin'] || check_prava($_ENV['id_user'], 'sec') || check_prava($_ENV['id_user'], 'dir')){ ?> 
		<a href='/sor/add'><div class='button'>{LT_CREATE}</div></a>
	<? };?>
	
	<h3>{LT_STATUS}</h3>
	<div class='buttonset' >
		<div class='button' status='4'>{LT_FILTE_SOR_NOW}</div>
		<div class='button' status='3'>{LT_FILTE_REG_NOW}</div>
		<div class='button' status='1'>{LT_FILTE_SOR_LAST}</div>
		<div class='button' status='5'>{LT_FILTE_SOR_NEXT}</div>
	</div>

	<h3>{LT_SPORT}</h3>
	{SELECT_SPORTS}

	<h3>{LT_REGION}</h3>
	{SELECT_REGION}

	<h3>{LT_FILTER_DATE}</h3>
	<input type='text' value='' class='datepicker_from' name='date_start'> - 
	<input type='text' value='' class='datepicker_to' disabled='true' name='date_finish'>
</div>