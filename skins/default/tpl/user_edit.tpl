<h4>{NAME} {SECOND_NAME} {THIRD_NAME} </h4>

<div id='response'></div>

<form action='/ajax/ajax_users.php?action=userSave' method='POST' id='form_user_edit'>
<div id='user_edit'>
	<ul>
		<li><a href="#tab_1">Основные данные</a></li>
		<li><a href="#tab_2">Контакты</a></li>
		<li><a href="#tab_3">Зачётка</a></li>
		<li><a href="#tab_4">Родители</a></li>
		<li><a href="#tab_5">Карьера</a></li>
		<li><a href="#tab_6">Биография</a></li>
	</ul>
	
	<div id='tab_1'>
		<p><label>{LT_NAME}</label>				<input type='text' value='{NAME}' name='user[name]' id='user_name' required></p>
		<p><label>{LT_SECOND_NAME}</label>		<input type='text' value='{SECOND_NAME}' name='user[second_name]' id='user_second_name' required></p>
		<p><label>{LT_THIRD_NAME}</label>		<input type='text' value='{THIRD_NAME}' name='user[third_name]' id='user_third_name'></p>
		<p><label>{LT_BORN}</label><input type='text' value='{BORN}' name='user[born]' id='user_born' class='datepicker'></p>
		<p><label>{LT_SEX}</label><select name='user[sex]'><option value='1' {SEL_SEX_M}>{LT_SEX_M}</option><option value='0'  {SEL_SEX_W}>{LT_SEX_W}</option></select></p>
		<p><label>{LT_CITY}</label>
			<input data-defval='{ID_CITY}' type='text' value='{CITY}' name='' id='' class='city' for='user_id_city'>
			<input type='hidden' name='user[id_city]' value='{ID_CITY}' id='user_id_city'>
		</p>
		
		<p><label>{LT_ADRESS}</label>
			<input type='text' value='{STREET}' name='user[adress_street]' id='' class='street' placeholder='{LT_STREET}'>
			<input type='number' min='0' max='400' step='1' value='{HOUSE}' name='user[adress_house]' id='' class='house' placeholder='{LT_HOUSE}'> /
			<input type='text' value='{BLOCK}' name='user[adress_block]' id='' class='block' placeholder='{LT_BLOCK}'> - 
			<input type='number' min='0' max='400' step='1' value='{FLAT}' name='user[adress_flat]' id='' class='flat' placeholder='{LT_FLAT}'>
		</p>
		
		<p><label>{LT_WORK}</label><input type='text' value='{WORK}' name='user[work]' id=''></p>
		<p><label>{LT_PASSPORT}</label><input type='text' value='{PASSPORT}' name='user[passport]' id=''></p>
		<p><label>{LT_POLIS}	</label><input type='text' value='{POLIS}' name='user[polis]' id=''></p>
	</div>
	
	<div id='tab_2'>
		<p><label>{LT_PHONE}</label><input type='text' class='phone' value='{PHONE}' name='user[phone]' id='' required></p>
		<p><label>{LT_PHONE_ADD}</label><input type='text' value='{PHONE_ADD}' name='user[phone_add]' id=''></p>
		<p><label>{LT_EMAIL}</label><input type='email' value='{EMAIL}' class='email' name='user[email]' id=''></p>
		
		<hr>
		<p><label>{LT_VK}</label><input type='text' value='{VK}' name='user[vk]' id=''></p>
		<p><label>{LT_WWW}</label><input type='text' value='{WWW}' name='user[www]' id=''></p>
		<p><label>{LT_FACEBOOK}</label><input type='text' value='{FB}' name='user[fb]' id=''></p>
		<p><label>{LT_ICQ}</label><input type='text' value='{ICQ}' name='user[icq]' id=''></p>
		<p><label>{LT_SKYPE} </label><input type='text' value='{SKYPE}' name='user[skype]' id=''></p>
	</div>
	<div id='tab_3'>

		<h4>{LT_USER_BLOCK_SPORT_OTHER}</h4>
		<p><label>{LT_WEIGHT}</label><input type='number' step='0.1' max='200' min='0' value='{WEIGHT}' name='user_sport_data[weight]' id=''></p>
		<p><label>{LT_HEIGTH}</label><input type='number' step='1' max='230' min='0' value='{HEIGHT}' name='user_sport_data[height]' id=''></p>
		<p><label>{LT_POYAS}</label>{POYAS}</p>
		<p><label>{LT_DISPANSER}</label><input type='text' value='{DISPANSER}' name='user_sport_data[dispanser]' id=''  class='datepicker'></p>
		
		<h4>{LT_USER_BLOCK_SPORT_ZVANIYA}</h4>
		{RANGS}
		
		<h4>{LT_USER_BLOCK_SPORT_RESULT_OLD}</h4>
		<textarea name='user_sport_data[info]' id='sport_data_info' class='autoresize'>{SPORT_INFO_OLD}</textarea>
		
	</div>
	<div id='tab_4'>
		<h4>{LT_PARENTS}</h4>
		
		<h3>{LT_MOTHER}</h3>
		<p><label>{LT_FIO}</label><input type='text' value='{MOTHER_NAME}' name='parents[mother_name]' id=''></p>
		<p><label>{LT_WORK}</label><input type='text' value='{MOTHER_WORK}' name='parents[mother_work]' id=''></p>
		<p><label>{LT_PHONE}</label><input type='text' class='phone' value='{MOTHER_PHONE}' name='parents[mother_phone]' id=''></p>
		
		<h3>{LT_FATHER}</h3>
		<p><label>{LT_FIO}</label><input type='text' value='{FATHER_NAME}' name='parents[father_name]' id=''></p>
		<p><label>{LT_WORK}</label><input type='text' value='{FATHER_WORK}' name='parents[father_work]' id=''></p>
		<p><label>{LT_PHONE}</label><input type='text' class='phone' value='{FATHER_PHONE}' name='parents[father_phone]' id=''></p>
		
		<h3>{LT_FAMILY}</h3>
		<p><label>{LT_NUM}</label><input type='text' value='{NUM_FAMILY}' name='parents[num_family]' id=''></p>

	</div>
	
	<div id='tab_5'>
		<h4>{LT_USER_BLOCK_CAREERS_SPORT}</h4>
		{CAREERS_SPORT}
		
		<h4>{LT_USER_BLOCK_CAREERS_ORG}</h4>
		{CAREERS_ORG}
		
	</div>
	<div id='tab_6'>
		<h4>{LT_USER_BLOCK_BOIGRAPHY}</h4>
		<textarea id='user_biography_edit' name='user[biography]' class='autoresize'>{BIOGRAPHY}</textarea>
	</div>
	
</div>

<input type='hidden' name='id_user' value='{ID_USER}'>
<input type='submit' class='but_save' value='{LT_SAVE}'>

</form>
