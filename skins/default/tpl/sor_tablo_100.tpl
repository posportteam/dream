<link rel="stylesheet" type="text/css" media="screen" href="/skins/default/css/sor_tablo.css"/>
<script type="text/javascript" src="/libs_js/site_sor_tablo.js"></script>

<div data-id-sor='{SOR_ID}'>
	<a id='butFightSettings'>Настройки</a>
	<div id='fightSettings' hidden>
		<div><label>Время поединка: </label>			<input id='setTimeFight' type='number' step='1' min='60' value='300'></div>
		<div><label>Лимит разницы баллов: </label>		<input id='setRazLimit'  type='number' step='5' min='12' value='10'></div>
		<div><label>Лимит нокдаунов: 		</label>	<input id='setNkdLimit'  type='number' step='2' min='4' value='3'></div>
		<div><label>Лимит неполных удержаний: </label>	<input id='setUderLimit' type='number' step='2' min='4' value='2'></div>
	</div>
	
	<audio id='gong'>
		<source src="/files/audio/tablo/gong.wav" type='audio/mpeg;'>
		<source src="/files/audio/tablo/gong.mp3" type='audio/ogg; codecs="mp3"'>
	</audio>
	
	<audio id='sec'>
		<source src="/files/audio/tablo/sec.mp3" type='audio/ogg; codecs="mp3"'>
	</audio>
	
	
	
	<div id='fight'>
		<h3>Текущий бой</h3>
		<table>
			<tr>
				<td class='red name'>&nbsp;</td>
				<td>Техническое действие</td>
				<td class='blue name'>&nbsp;</td>
			</tr>
			
			<tr>
				<td class='red'>
					<button class='event' data-ugol='1' data-key='A'>A</button>
					<button class='event' data-ugol='1' data-key='1'>1</button>
					<button class='event' data-ugol='1' data-key='2'>2</button>
				</td>
				<td>Удары, броски</td>
				<td class='blue'>
					<button class='event' data-ugol='0' data-key='A'>A</button>
					<button class='event' data-ugol='0' data-key='1'>1</button>
					<button class='event' data-ugol='0' data-key='2'>2</button>
				</td>
			</tr>
			
			<tr>
				<td class='red'>
					<div class='buttonset'>
						<input name='uder1' type='button' id='uder1' value = 'Удержание'>
						<input name='uder1' type='button' id='uderTimer1'  value=':00' disabled>
						<input name='uder1' type='hidden' id='uderSec1' value='0'>
						<input name='uder1' type='hidden' id='uderLimit1' value='2'>
					</div>
				</td>
				<td class='uder'>Удержание</td>
				<td class='blue'>
					<div class='buttonset'>
						<input name='uder0' type='button' id='uder0' value = 'Удержание'>
						<input name='uder0' type='button' id='uderTimer0' disabled value=':00'>
						<input name='uder0' type='hidden' id='uderSec0' value='0'>
						<input name='uder0' type='hidden' id='uderLimit0' value='2'>
					</div>

				</td>
			</tr>
			<tr>
				<td class='red'>
					<div class='buttonset'>
						<button name='chp1' class='event' data-ugol='1' data-key='nkd'>Нкд</button>
						<input name='chp1' type='hidden' id='nkdLimit1' value='3'>
						<button name='chp1' class='event' data-ugol='1' data-key='tn'>Тн</button> 
						<button name='chp1' class='event' data-ugol='1' data-key='nk'>Нк</button>
						<button name='chp1' class='event' data-ugol='1' data-key='ud'>Уд</button>
						<button name='chp1' class='event' data-ugol='1' data-key='bp'>Бп</button>
					</div>
				</td>
				<td>Чистая победа</td>
				<td class='blue'>
					<div class='buttonset'>
						<button name='chp0' class='event' data-ugol='0' data-key='nkd'>Нкд</button>
						<input name='chp0' type='hidden' id='nkdLimit0' value='3'>
						<button name='chp0' class='event' data-ugol='0' data-key='tn'>Тн</button>
						<button name='chp0' class='event' data-ugol='0' data-key='nk'>Нк</button>
						<button name='chp0' class='event' data-ugol='0' data-key='ud'>Уд</button>
						<button name='chp0' class='event' data-ugol='0' data-key='bp'>Бп</button>
					</div>
				</td>
			</tr>
			<tr>
				<td class='red'>
					<div class='buttonset'>
						<input type='radio' class='event' data-ugol='1' data-key='bv' 	data-ball = '0' name='zv1' id='bv1' checked><label for='bv1'>-</label>
						<input type='radio' class='event' data-ugol='1' data-key='zv' 	data-ball = '0'  name='zv1' id='zv1'><label for='zv1'>Зв</label>
						<input type='radio' class='event' data-ugol='1' data-key='pv1' 	data-ball = '-1'  name='zv1' id='pv11'><label for='pv11'>В1</label>
						<input type='radio' class='event' data-ugol='1' data-key='pv2' 	data-ball = '-2'  name='zv1' id='pv21'><label for='pv21'>В2</label>
						<input type='radio' class='event' data-ugol='1' data-key='pv3' 	data-ball = '-'  name='zv1' id='pv31'><label for='pv31'>Вс</label>
					</div>
				</td>
				<td>Выход за ковёр</td>
				<td class='blue'>
					<div class='buttonset'>
						<input type='radio' class='event' data-ugol='0' data-key='bv' data-ball = '0' name='zv0' id='bv0' checked><label for='bv0'>-</label>
						<input type='radio' class='event' data-ugol='0' data-key='zv' data-ball = '0' name='zv0' id='zv0'><label for='zv0'>Зв</label>
						<input type='radio' class='event' data-ugol='0' data-key='pv1' data-ball = '-1' name='zv0' id='pv10'><label for='pv10'>В1</label>
						<input type='radio' class='event' data-ugol='0' data-key='pv2' data-ball = '-2' name='zv0' id='pv20'><label for='pv20'>В2</label>
						<input type='radio' class='event' data-ugol='0' data-key='pv3' data-ball = '-' name='zv0' id='pv30'><label for='pv30'>Вс</label>
					</div>
				</td>
			</tr>
			<tr>
				<td class='red'>
					<div class='buttonset'>
						<input type='radio' class='event' data-ugol='1' data-key='bz' data-ball = '0' name='zp1' id='bz1' checked><label for='bz1'>-</label>
						<input type='radio' class='event' data-ugol='1' data-key='zp' data-ball = '0' name='zp1' id='zp1'><label for='zp1'>Зп</label>
						<input type='radio' class='event' data-ugol='1' data-key='pr1' data-ball = '-1' name='zp1' id='pr11'><label for='pr11'>П1</label>
						<input type='radio' class='event' data-ugol='1' data-key='pr2' data-ball = '-2' name='zp1' id='pr21'><label for='pr21'>П2</label>
						<input type='radio' class='event' data-ugol='1' data-key='pr3' data-ball = '-' name='zp1' id='pr31'><label for='pr31'>Пс</label>
					</div>
				</td>
				<td>Замечания</td>
				<td class='blue'>
					<div class='buttonset'>
						<input type='radio' class='event' data-ugol='0' data-key='bz' data-ball = '0' name='zp0' id='bz0' checked><label for='bz0'>-</label>
						<input type='radio' class='event' data-ugol='0' data-key='zp' data-ball = '0' name='zp0' id='zp0'><label for='zp0'>Зп</label>
						<input type='radio' class='event' data-ugol='0' data-key='pr1' data-ball = '-1' name='zp0' id='pr10'><label for='pr10'>П1</label>
						<input type='radio' class='event' data-ugol='0' data-key='pr2' data-ball = '-2' name='zp0' id='pr20'><label for='pr20'>П2</label>
						<input type='radio' class='event' data-ugol='0' data-key='pr3' data-ball = '-' name='zp0' id='pr30'><label for='pr30'>Пс</label>
					</div>
				</td>
			</tr>
		
			<tr>
				<td class='red'>
					<input class='ball' id='ball1' value='0' disabled> +
					<input class='ball' id='ballDop1' value='0' disabled>	/ 
					<input class='ball' id='act1' value='0' disabled>
				</td>
				<td>Баллы/Активности</td>
				<td class='blue'>
					<input class='ball' id='ball0' value='0' disabled> +
					<input class='ball' id='ballDop0' value='0' disabled>	/ 
					<input class='ball' id='act0' value='0' disabled>
				</td>
			</tr>

			<tr>
				<td class='red prot'><input type='text' id='prot1' disabled></td>
				<td>Протокол</td>
				<td class='blue prot'><input type='text' id='prot0' disabled></td>
			</tr>
		
			<tr>
				<td class='red win1'></td>
				<td id='timer'>
					<div class='buttonset'>
						<input type='button' name='timer' id='timerStartStop' value='Таймер'>
						<input type='button' name='timer' id='timerTime' disabled value='00:00'>
						
						<input id='nkdLimit'  type='hidden' value=3>
						<input id='timerFlag'  type='hidden' value=0>
						<input id='timerSec'  type='hidden' value='0'>
						<input id='winner'  type='hidden'>
					</div>
				</td>
				<td class='blue win0'></td>
			</tr>
		</table>
		
		<div class='buttonset'>
			<input type='button' name='fight' id='butWinner' value='Определить победителя'>
			<input type='button' name='fight' id='butNewFight' value='Новый бой'>
		</div>
		
		<div class='warning' id='log' hidden></div>
		
		<div class='error' id='logError' hidden></div>
	</div>
	
	
	
</div>