<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang='ru'> 

	<head> 
		<title>{TITLE}</title>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
		<meta name='keywords' content='Портал для Спортсменов'/>
		<meta http-equiv='content-language' content='ru-ru' />
		<meta http-equiv='content-style-type' content='text/css' />
		<meta http-equiv='imagetoolbar' content='yes' />
		<meta name='resource-type' content='document' />
		<meta name='distribution' content='global' />
		<meta http-equiv-"X-UA-Compatible" content="IE=9"/>

		<link rel="icon" href="/skins/default/images/favicon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="/skins/default/images/favicon.ico" type="image/x-icon">
		
		<!-- 
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>  -->
		
		<script type="text/javascript" src="/libs_js/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="/libs_js/jquery-ui-1.10.1.custom.js"></script>

		

                <!-- <script type="text/javascript" src="/libs_js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>


                <link rel="stylesheet" href="/libs_js/fancybox/source/jquery.fancybox.css?v=2.1.1" type="text/css" media="screen" />
                <script type="text/javascript" src="/libs_js/fancybox/source/jquery.fancybox.pack.js?v=2.1.1"></script>
                    

                <link rel="stylesheet" href="/libs_js/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.4" type="text/css" media="screen" />
                <script type="text/javascript" src="/libs_js/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.4"></script>
                <script type="text/javascript" src="/libs_js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.4"></script>
                    
                <link rel="stylesheet" href="/libs_js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
                <script type="text/javascript" src="/libs_js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

             <script src="http://www.youtube.com/player_api"></script> 
                
                 -->
		
		
		
		
		<script type="text/javascript" src="/libs_js/jquery.form.js"></script>
		<script type='text/javascript' src='/libs_js/site_auth.js'></script>
		
		<script type='text/javascript' src='/libs_js/jquery.autoresize.min.js'></script>
		<script type='text/javascript' src='/libs_js/jquery.cookie.min.js'></script>
		<script type='text/javascript' src='/libs_js/site_function.js'></script>	
		<script type='text/javascript' src='/libs_js/site_main.js'></script>
		
		<link rel='stylesheet' type="text/css" media='screen' href='/css/toolbar.css'>	
		<link rel='stylesheet' type='text/css' media='screen' href="/skins/default/css/general.css"/>  
		<link rel='stylesheet' type='text/css' media='screen' href="/skins/default/css/sidebar.css"/>  
		<link rel="stylesheet" type="text/css" media="screen" href="/skins/default/css/content.css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="/skins/default/css/footer.css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="/skins/default/css/notepad.css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="/skins/default/css/jquery.css"/> 
		<!-- <link rel="stylesheet" type="text/css" media="screen" href="/skins/default/css/jquery-ui-1.10.0.custom.css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="/skins/default/css/jquery.ui.1.10.0.ie.css"/>-->
		
		
	</head> 
	<body> 
		{TOOLBAR}
		<!--START_CONTENT /-->
			<div id='content-container'>
				<div id='upLink' hidden='true'>{LT_GO_TOP_PAGE}</div>
				<div id='content'>
					
					<div id='info'>
						{FILTER_TITLE}
						<div id='info_page'>
							{FILTER}
							<div id='content_page'>
								<div id='page_response'>123</div>	
								{CONTENT_PAGE}
							</div>
						</div>
					</div>
					<div id="sidebar" class="sidebar">
						{SIDEBAR}
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<!-- END_CONTENT /-->
		{FOOTER}
	</body> 
</html>