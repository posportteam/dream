<div id='user_{ID_USER}' class='users_preview'>

	<div class='update_user_info'>
		<input type='text' class='name' value='{NAME}' placeholder='{LT_NAME}'>
		<input type='text' class='second_name' value='{SECOND_NAME}' placeholder='{LT_SECOND_NAME}'>
		<input type='text' class='phone' value='{PHONE}' placeholder='{LT_PHONE}'>
		{SELECT}
		
	</div>

	<div class='update_user_buttons'>
		<a href = "/users/edit?id_user={ID_USER}" ><button class='but_my_small'>{LT_GO_USER_PAGE}</button></a>
		<button class='but_save_small save_user' data-id-user='{ID_USER}'>{LT_BUT_SAVE}</button>
	</div>
	
	<div class='loading'></div>
</div>