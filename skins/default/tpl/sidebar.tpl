<!-- SIDEBAR_START /-->
<ul>
		<li>
			<h2>{LT_MENU_TITLE_FREE}</h2>
			<ul>
				<li><a href='/orgs/section'>{LT_SECTIONS}</a> </li>
				<li><a href='/reiting'>{LT_REITING}</a> </li>
				<li><a href='/articles/azbuka'>{LT_AZBUKA}</a> </li>
				<li><a href='/sors'>{LT_SOR}</a> </li>
				<li><a href='/merops'>{LT_MEROP}</a> </li>
				<li><a href='/photo'>{LT_PHOTOGAL}</a> </li>
				<li><a href='/video'>{LT_VIDEOGAL}</a> </li>
			</ul>
		</li>
	
	<!-- IF USER_ACTIVE -->
	<? if($_ENV[id_user]){ ?>
		<li>
			<h2>{LT_MENU_TITLE_USER}</h2>
			<ul>
				<li><a href='/id{ID_USER}'>{LT_MY_PAGE}</a> </li>
				<li><a href='/articles'>{LT_MY_NOTE}</a> </li>
				<li><a href='/user/notice'>{LT_MY_NOTICE}</a> {NUM_NEW_NOTICE}</li>
				<li><a href='/user/message'>{LT_MY_MESSEGE}</a> {NUM_NEW_MESS}</li>
				<li><a href='/sor/my'>{LT_MY_RESULTS}</a> </li>
				<li><a href='/user/oplata'>{LT_MY_BILL}</a> {NUM_OST_BALANCE}</li>
			</ul>
		</li>
	
	<?};?>
	<!-- ENDIF -->

	<? if($_ENV[user_trener] || $_ENV['admin']){ ?>

	<!-- IF USER_TRENER -->
	
		<li>
			<h2>{LT_MENU_TITLE_TRENER}</h2>
			<ul>
				<li><a href='/users/my'>{LT_MY_SPORTSMENS}</a> </li>
				<li><a href='/groups/my'>{LT_MY_GROUPS}</a> </li>
				<li><a href='/groups/money'>{LT_MONEY}</a> </li>
				<li><a href='/docs/my'>{LT_MY_DOCS}</a> </li>
				
			</ul>
		</li>
		
	<?};?>	
	<!-- ENDIF -->
	
	<? if($_ENV[user_secretar] || $_ENV[admin]){ ?>
	<!-- IF USER_SECRETAR -->
	
		<li>
			<h2>{LT_MENU_TITLE_SECRETAR}</h2>
			<ul>
				<li><a href=''>{LT_PROTOKOL}</a> </li>
				<li><a href=''>{LT_REPORTS}</a> </li>
			</ul>
		</li>
		
	<?};?>
	<!-- ENDIF -->
	
	<? if($_ENV[user_director] || $_ENV[admin]){ ?>
	<!-- IF USER_DIRECTOR -->
		<li>
			<h2>{LT_MENU_TITLE_DIRECTOR}</h2>
			<ul>
				
			</ul>
		</li>
		
	<?};?>	
	<!-- ENDIF -->
	
	<? if($_ENV[admin]){ ?>
	<!-- IF USER_ADMIN -->
	
		<li>
			<h2>{LT_MENU_TITLE_ADMIN}</h2>
			<ul>
				<li><a href='/admin/oplata'>{LT_FINANS_OPER}</a> </li>
				<li><a href='/admin/sports'>{LT_SPORTS_AND_RULES}</a> </li>
				<li><a href='/admin/city_zal'>{LT_CITY_AND_GYM}</a> </li>
				<li><a href='/admin/sports'>{LT_SPORTS_TYPE}</a> </li>
				<li><a href=''>{LT_DISPANSER}</a> </li>
				<li><a href=''>{LT_ORDERS}</a> </li>
				<li><a href='phpMyAdmin' target='_blank'>{LT_MYSQL}</a> </li>
			</ul>
		</li>
		
	<?};?>
	<!-- ENDIF -->
	
</ul>

<? if($_ENV['user_config']['notepad']){ ?>
<!-- IF USER_CONFIG_NOTEPAD -->
	
	{NOTEPAD}
	
<? }; ?>	
<!-- ENDIF -->


<!-- IF USER_CONFIG_CALENDAR -->

	{CALENDAR}
	

<!-- ENDIF -->

<!-- SIDEBAR_FINISH /-->