<div id='sor_reg'>
	<ul>
		<li><a href="#team_reg">Регистрация команд</a></li>
		<li><a href="#user_reg">Регистрация участников</a></li>
		<li><a href="#referee_reg">Регистрация судей</a></li>
	</ul>
  
	<div id='team_reg'>
		<h4>Регистрация команд</h4>
		<h3>{LT_ADD}:</h3>
		<form action='/ajax/ajax_sor.php?action=addSorTeams' method='POST' id='team_add'>
			<select name='id_fedok' 	class='fedok linklist'			data-linklist-name='fedok' 		value=''	data-defval=''><option value=''>{LT_FEDOK}</option>{LT_FEDOK_SELECT}</select>
			<select name='id_obl' 		class='obl linklist'			data-linklist-name='obl'		value=''	data-defval=''><option value=''>{LT_OBL}</option></select>
			<select name='id_city' 		class='city linklist'			data-linklist-name='city'		value=''	data-defval=''><option value=''>{LT_CITY}</option></select>
			<select name='id_club' 		class='club linklist'			data-linklist-name='club'		value=''	data-defval=''><option value=''>{LT_CLUB}</option></select>
			<select name='id_section'	class='section linklist'		data-linklist-name='section'	value=''	data-defval=''><option value=''>{LT_SECTION}</option></select>
			
			<input type='text' id='team_title' disabled='true' readonly placeholder='{LT_TEAM_TITLE}'>
			<input name='id_sor' value='{ID_SOR}' type='hidden' >
			<input name='id_sport' value='{ID_SPORT}' type='hidden' >
			
		
			<button class='but_add_small' id='but_team_add'>Добавить</button>
		</form>
		<h3>Зарегестрированные команды:</h3>
		<div id='loading_sor_add' class='loading'></div>
		<table id='sor_teams'>
			<thead>
				<th>№</th>
				<th>Название</th>
				<th>Кол-во спортсменов</th>
				<th>Результат</th>
				<th>Управление</th>
			</thead>
			<tbody>
				{REG_TEAMS}
			</tbody>
		</table>
	</div>
	<div id='user_reg'>
		<h4>Регистрация участников</h4>
		<h3>{LT_ADD}:</h3>
			{REG_USER_ADD}
		
		<h3>Зарегестрированные спортсмены ({COUNT_REG_USERS}):</h3>
		{REG_USERS}
	</div>
	<div id='referee_reg'>
		<h4>Регистрация судей</h4>
	</div>
</div>
		