<div class='careers_org_edit {NEW}'>
	<input type='text' class='careers_city city' placeholder='{LT_CITY}' value='{CITY}' for='user_careers_org_city_{NUM_CAREERS}'>
	<input type='hidden' name='careers[org][city][]' value='{ID_CITY}' id='user_careers_org_city_{NUM_CAREERS}'>
	
	<input type='text' class='careers_org org' placeholder='{LT_ORG_OR_CLUB}' value='{ORG}'>
	<input type='hidden' class='id_org' name='careers[org][org][]' value='{ID_ORG}'>
	<input type='hidden' class='id_club' name='careers[org][club][]' value='{ID_CLUB}'>
	
	<input type='text' class='careers_post' placeholder='{LT_POST}'  value='{TITLE}'>
	{SELECT_ACCESS_TYPE}
	
	
	<input type='text' class='careers_date datepicker_from' value='{DATE_IN}'>
	<input type='text' class='careers_date datepicker_to' value='{DATE_OUT}'>
	
	<button class='but_save_small save_career_org'>{LT_SAVE}</button>
</div>