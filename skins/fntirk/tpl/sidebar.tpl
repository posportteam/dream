<!-- SIDEBAR_START /-->
			<h2>{LT_MENU_TITLE_FREE}</h2>
			<ul>
				<li><a href='/org/section'>{LT_SECTIONS}</a> </li>
				<li><a href='/reiting'>{LT_REITING}</a> </li>
				<li><a href='/articles/azbuka'>{LT_AZBUKA}</a> </li>
				<li><a href='/sor'>{LT_SOR}</a> </li>
				<li><a href='/merop'>{LT_MEROP}</a> </li>
				<li><a href='/user/treners'>{LT_TRENERS}</a> </li>
				<li><a href='/gallery1'>{LT_PHOTOGAL}</a> </li>
				<li><a href='/video'>{LT_VIDEOGAL}</a> </li>
			</ul>
	
	<!-- IF USER_ACTIVE -->
	<? if($_ENV[id_user]){ ?>
			<h2>{LT_MENU_TITLE_USER}</h2>
			<ul>
				<li><a href='/user'>{LT_MY_PAGE}</a> </li>
				<li><a href='/articles'>{LT_MY_NOTE}</a> </li>
				<li><a href='/notice'>{LT_MY_NOTICE}</a> {NUM_NEW_NOTICE}</li>
				<li><a href='/message'>{LT_MY_MESSEGE}</a> {NUM_NEW_MESS}</li>
				<li><a href='/sor/my'>{LT_MY_RESULTS}</a> </li>
				<li><a href='/oplata'>{LT_MY_BILL}</a> {NUM_OST_BALANCE}</li>
				<li><a href='/'>{LT_MY_TEAM}</a> </li>
			</ul>
	
	<?};?>
	<!-- ENDIF -->
	
	<? if($_ENV[user_trener] || $_ENV['admin']){ ?>
	<!-- IF USER_TRENER -->

			<h2>{LT_MENU_TITLE_TRENER}</h2>
			<ul>
				<li><a href='/users/my'>{LT_MY_SPORTSMENS}</a> </li>
				<li><a href='/groups/my'>{LT_MY_GROUPS}</a> </li>
				<li><a href='/sbornaya/my'>{LT_MY_TEAMS}</a> </li>
				<li><a href='/docs/my'>{LT_MY_DOCS}</a> </li>
				
			</ul>
		
	<?};?>	
	<!-- ENDIF -->
	
	<? if(check_prava($_ENV['id_user'],'sec') || $_ENV['admin']){ ?>
	<!-- IF USER_SECRETAR -->
	
			<h2>{LT_MENU_TITLE_SECRETAR}</h2>
			<ul>
				<li><a href=''>{LT_PROTOKOL}</a> </li>
				<li><a href=''>{LT_REPORTS}</a> </li>
			</ul>
		
	<?};?>
	<!-- ENDIF -->
	
	<? if(check_prava($_ENV['id_user'],'dir')  || $_ENV['admin']){ ?>
	<!-- IF USER_DIRECTOR -->
			<h2>{LT_MENU_TITLE_DIRECTOR}</h2>
			<ul>
				
			</ul>
		
	<?};?>	
	<!-- ENDIF -->
	
	<? if($_ENV['admin']){ ?>
	<!-- IF USER_ADMIN -->

			<h2>{LT_MENU_TITLE_ADMIN}</h2>
			<ul>
				<li><a href='/admin/oplata'>{LT_FINANS_OPER}</a> </li>
				<li><a href='/admin/sports'>{LT_SPORTS_AND_RULES}</a> </li>
				<li><a href='/admin/city_zal'>{LT_CITY_AND_GYM}</a> </li>
				<li><a href='/admin/sports'>{LT_SPORTS_TYPE}</a> </li>
				<li><a href=''>{LT_DISPANSER}</a> </li>
				<li><a href=''>{LT_ORDERS}</a> </li>
				<li><a href='phpMyAdmin' target='_blank'>{LT_MYSQL}</a> </li>
			</ul>
		
	<?};?>
	<!-- ENDIF -->
	



<!-- IF USER_CONFIG_CALENDAR -->
	{CALENDAR}
<!-- ENDIF -->

<? if($_ENV['user_config']['notepad']){ ?>
<!-- IF USER_CONFIG_NOTEPAD -->
	{NOTEPAD}
<? }; ?>	
<!-- ENDIF -->

<!-- SIDEBAR_FINISH /-->