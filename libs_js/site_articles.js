///////////////////////////////////////////
///////////////////////////////////////////
//////// СОХРАНЕНИЕ СТАТЕЙ  ///////////////
///////////////////////////////////////////

	function artSave(){
		if($('#article_title').val() && $('#art_edit').val()){
			$("#artEditForm").ajaxSubmit({
				dataType:'json',
				success: function(response, statusText, xhr, form)  { 
					$('#article_id').val(response.article_id);
					if(response.article_id){
						alert(response.article_text);
					};
				}
			});
		}else{
			alert('Не заполены обязательные поля!');
		};
	};
	
tinyMCE.init({
        // General options
		mode : "exact",
        elements : "art_edit",
		theme : "advanced",
        skin : "o2k7",
		language : "ru",
		width: "740",
		height: "600",
		relative_urls : true,
		convert_urls : true,
		remove_script_host : true,
        // plugins : "spellchecker,pagebreak,style,layer,advhr,advlink,emotions,insertdatetime,media,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		plugins : "autosave,save,autolink,preview,print,paste,fullscreen,contextmenu,iespell,table,lists,advimage,searchreplace,inlinepopups",
        theme_advanced_buttons1 : "save,newdocument,fullscreen,print,code,preview,|,undo,redo,|,cut,copy,paste|,search,replace,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,sub,sup,charmap",
        //theme_advanced_buttons2 : "outdent,indent,blockquote,|,link,unlink,anchor,cleanup,help,|,insertdate,inserttime,|,forecolor,backcolor",
        //theme_advanced_buttons3 : "hr,removeformat,visualaid,|,emotions,iespell,media,advhr,|,ltr,rtl",
        //theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "center",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,
        save_onsavecallback : "artSave",
		setup : function(ed) {
			var test = false;
			ed.onBeforeExecCommand.add(function(ed, cmd,ui,v) {
				if(cmd == "mceNewDocument"){
					test = true;
				}
			});
			ed.onExecCommand.add(function(ed,cmd,ui,v) {
				if(cmd == "mceSetContent" && !v && test) {
					// Создание новой статьи
					$('#article_author').val($.cookie('id_user'));
					$('#article_title').val('');
					$('#article_anons').val('');
					$('#article_id').val('');
				}
			});
		}
       // Example content CSS (should be your site CSS)
        //content_css : "css/example.css",

        // Drop lists for link/image/media/template dialogs
        // template_external_list_url : "js/template_list.js",
        // external_link_list_url : "js/link_list.js",
        //external_image_list_url : "js/image_list.js",
        // media_external_list_url : "js/media_list.js",

        // Replace values for the template plugin
        //template_replace_values : {
        //        username : "Some User",
        //        staffid : "991234"
        //}
});
	

///////////////////////////////////////////
///////////////////////////////////////////
//////// Статьи  //////////////////////////
///////////////////////////////////////////

$("#article_type").change(function(e){
	if($(this).val()=='sor'){
		$("#p_article_id_sor").show(300);
	}else{
		$("#p_article_id_sor").hide(300);
	};
	

	
});


// Добавить в избранное
$("a.art_fav, a.art_no_fav").click(function(e){
	var id = $(this).attr('data-id-art');
	var event = $(this).attr('data-event');
	var text_del = "Удалить из избранного";
	var text_add = "Добавить в избранное";
	var a_obj = $(this);
	$.ajax({
		type: "POST",
		url: "ajax/ajax_articles.php?action=" + event,
		data: {id: id},
		cache: false,
		success: function(response){ 
			a_obj.toggleClass('art_fav').toggleClass('art_no_fav');
			if(event=='artFavAdd'){
				a_obj.attr({'data-event':'artFavDel','title':text_del}).html(text_del);
			}else if(event=='artFavDel'){
				a_obj.attr({'data-event':'artFavAdd','title':text_add}).html(text_add);
			};
		}
	});
});
// Удаление и восстановление статей
$("a.artDel, a.artRec").click(function(e){
	var div_art = $(this).parents('div.article_preview');
	var id = div_art.attr('data-id-art');
	var event = $(this).attr('data-event');
	$.ajax({
		type: "POST",
		url: "ajax/ajax_articles.php?action=" + event,
		data: {id: id},
		cache: false,
		beforeSend: function(){
			div_art.find('div.loading').show();
		},
		success: function(response){
			if(event=='artDel'){
				div_art.find('div.art_info').hide();
				$('#comments_in_art').hide();
				$('#art_text').hide();
				div_art.toggleClass('deleting');
				div_art.find('div.art_rec').show(300);
			}else if(event=='artRec'){
				div_art.find('div.art_rec').hide();
				div_art.toggleClass('deleting');
				div_art.find('div.art_info').show(300);
				$('#comments_in_art').show(300);
				$('#art_text').show(300);
			};
			div_art.find('div.loading').hide();
		}
	});
});

	
$('#content_filter .buttonset div.button').click(function(){
	if($(this).hasClass('active')){var flag = 1};
		$(this).parent('div.buttonset').find('div.button').removeClass('active');
	if(!flag){
		$(this).addClass('active');
	}

	getArts();

});

function getArts(){
	var data = {
		status:		$('#content_filter div.buttonset div.active').attr('status'),
	};
	
	$.ajax({  
		type: "POST",  
		url: "/ajax/ajax_articles.php?action=getArts",  
		data: data,
		beforeSend: function(){
			$('#articles').html("<img src='/images/loading.gif'>");
		},
		success: function(response){
			$('#articles').html(response);
		}
	});
};