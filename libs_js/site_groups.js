$(document).ready(function(){

	$('table.group_money td.money input.money').change(function(){
		var summa = parseInt($(this).val());

		if(!summa){
			$(this).val('');
			summa = 0;
		};
		
		var data = {
			year 		: $(this).parents('table.group_money').attr('data-year'),
			month 		: $(this).attr('data-month'),
			id_user 	: $(this).attr('data-id-user'),
			summa		: summa,
			id_group	: $(this).parents('tr.user').attr('data-id-group')
		};
		
		
		$.ajax({  
			type: "POST",  
			url: "/ajax/ajax_groups.php?action=setMoney",  
			data: data,
			beforeSend: function(){
				
			},
			success: function(month){
				var s = 0;
				$("input[data-month='"+month+"']").each(function(){
					i = parseInt($(this).val());
					if(i){s+=i;};
				});
				
				$("td[data-month-itog='"+month+"']").text(s);
				

			}
		});
		
		
	});

	$('table.group_money td.money input.money').focus(function(){
		$(this).addClass('sum');
		var money_rek = $(this).parents('tr.user').attr('data-money-rek');
		if(!$(this).val() && money_rek){
			$(this).val(money_rek).change();
		};
	});
	
	$('table.group_money td.money input.money').blur(function(){
		if(!$(this).val()){
			$(this).removeClass('sum');
		};
	});

	$('#butShowMoney').click(function(){
		var year = $('#select_year').val();
		var new_href = '/groups/money?y=' + year;
		location.href = new_href;
	});
	
	$('#butShowAttendance').click(function(){
		var month = $('#select_month').val();
		var year = $('#select_year').val();
		var id_group = $(this).attr('data-id-group');
		var new_href = '/groups/attendance?id_group=' + id_group + '&m=' + month + '&y=' + year;
		location.href = new_href;
	});
	
	$('table.group_attendance td.atten_status').click(function(){
		if($(this).hasClass('checked')){
			$(this).removeClass('checked').find('input[type=checkbox]').removeAttr('checked');
			save_attendance($(this));
		}else{
			$(this).addClass('checked').find('input[type=checkbox]').attr('checked','true');
			save_attendance($(this));
		};
	});
	
	function save_attendance(td_obj_at){
		
		if(td_obj_at.find('input[type=checkbox]').attr('checked')){
			var status = 1;
		}else{
			var status = 0;
		};
		
		var data = {
			day:		td_obj_at.attr('data-day'),
			id_user:	td_obj_at.attr('data-id-user'),
			id_group:	td_obj_at.parents('table.group_attendance').attr('data-id-group'),
			month:		td_obj_at.parents('table.group_attendance').attr('data-month'),
			year:		td_obj_at.parents('table.group_attendance').attr('data-year'),
			status:		status
		};
		
		$.ajax({  
			type: "POST",  
			url: "/ajax/ajax_groups.php?action=setAttendance",  
			data: data,
			beforeSend: function(){
				
			},
			success: function(response){
				//td_obj_at.html(response);
			}
		});
	};
	
	var age_from = $( "#group_age_from" ).val();
	if($( "#group_age_to" )>0){
		var age_to = $( "#group_age_to" ).val();
	}else{
		var age_to = 19;
		$( "#group_age_to" ).val('->');
	};
	
	$( "#group_age" ).slider({
		range: true,
		min: 4,
		max: 19,
		values: [ age_from, age_to ],
		slide: function( event, ui ) {
			$( "#group_age_from" ).val(ui.values[ 0 ]);
			if(ui.values[ 1 ] == 19){
				$( "#group_age_to" ).val('->');
			}else{
				$( "#group_age_to" ).val(ui.values[ 1 ]);
			};
		}
	});
	
});