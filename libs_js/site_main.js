$(document).ready(function(){

	// Инициализация fancybox
	//$(".fancybox").fancybox();
	
	// Инициализация кнопок
	init_button();
	
	// Инициализация Календаря
	init_datepicker();
	
	// Инициализация Связанные списки
	$("select.linklist").change(function(){
		linklist($(this));
	});
	
	$('select.autoload').each(function(index) {
		linklist($(this),1)
	});
	
	
	
	// Инициализация открытие фильтра
	
	$('#but_filter').click(function(e){
		$('#content_filter').toggle();
	}); 
	
	
	

	$('input.datapicker, input.datepicker_from, input.datepicker_to').change(function(){
		var date = $(this).val();
		var reg=/(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}/;
		var result = reg.test(date) 
		if(!reg.test(date)){$(this).val('');};
	});
	
	$('input').on('change','.phone',function(e){
		var phone = reform_mobile_phone($(this).val().trim());
		$(this).val(phone);
	});
		
	$('#loginout').click(function(e){
		$.cookie('id_user', null);
		$.cookie('hash', null);
		location.href='/index';
	});
	
	$('textarea.autoresize').autoResize({
		onResize: function(){
			$(this).css({opacity: 0.8});
		},
		animateCallback: function(){
			$(this).css({opacity: 1});
		},
		animateDuration: 300,
		extraSpace:0
	});
	


	


// Фильтр географии сайта
$("#set_site_myregion").change(function(){
	if($(this).attr('checked')){
		$("#div_set_site_region").hide();
	}else{
		$("#div_set_site_region").show();
	};
});

// Фильтр спорта сайта
$("#set_site_mysport").change(function(){
	if($(this).attr('checked')){
		$("#div_set_site_sport").hide();
	}else{
		$("#div_set_site_sport").show();
	};
});


/////////////////////////////////////
//////// Toolbar ////////////////////
/////////////////////////////////////
	$("#toolbar .but_set").click(function() {	
		$("#toolbar_settings").toggle();
	});


/////////////////////////////////////
/////////////////////////////////////
/////////////////////////////////////

// function loadPage(page, type, date, per_page){
	// $('#events_loading').show();
	// $("#events_tabs_"+type).hide();

	// $.ajax({
		// type: "POST",
		// url: "/ajax/ajax_page.php",
		// data: "page="+page+"&type="+type+"&per_page="+per_page+"&date="+date,
		// success: function(response){
				// $('#events_loading').hide();
				// $("#events_tabs_"+type).html(response).show();
				// init_button();
		// }
	// });
// };

// $('#events_tabs table tr.toggle').live('click',function(e){
	// $(this).next('tr').toggle();
// }); 	



// $('#paginator button').live('click',function(e){
	// var page = $(this).attr('data-page');
	// var type = $(this).parents('table').attr('data-type-paginator');
	// var per_page = $(this).parents('table').attr('data-per-page');
	// loadPage(page, type, 0, per_page);
// }); 

// $('#select_per_page').live('change',function () {
	// var per_page = $(this).val();
	// var type = $(this).parents('table').attr('data-type-paginator');
	// loadPage(1, type, 0, per_page);
// });




	
	
	// Скроллинг
	function toTop(){
		var toTop=$('#upLink');
		if($(window).scrollTop()>'200' && $(window).width()>'1050'){
				var win_width = $(window).width();
				var win_heigth = $(window).height();
				
				var totop_width=(win_width-1024)/2;
				
				toTop.css({'width': totop_width, 'height': win_heigth, 'z-index': 99999}).fadeIn(100);

		}else{
			toTop.fadeOut(100);
		};
	}; 
	
	
	$(window).scroll(function(){
		toTop();
	});
	
	toTop();
	
	$("#upLink").click(function () {
		scroll(0,0);
	});
	
	
	
	
	//Работа виджет Tabs
	// $("#lichnoe_delo").tabs();
	// $("#sor_tabs").tabs();
	// $("#sbornaya_tabs").tabs();
	// $("#messages").tabs();
	// $("#events_tabs").tabs({
		// create: function(event, ui) {
			// var curr_tab = $(this).attr('data-type');
			// var date = $(this).attr('data-date');
			// $(this).tabs( "option", "selected", parseInt(curr_tab));
			// loadPage(curr_year, 'sor', date, 0); // For first time page load default results
			// loadPage(curr_year, 'merop', date, 0); // For first time page load default results
		// }
	// });

	
		
	// Тугл
	$(".h4_show, .text_show").click(function () {
		$(this).next('div').toggle();
	});
	


/////////////////////////////////////////
/////////////////////////////////////////
///////// Подписка на блог //////////////
/////////////////////////////////////////


	$("#podpiska_blog").click(function () {
			var id_user_to = $(this).attr('id_user_to');
			var id_user_from = $(this).attr('id_user_from');
			var type_pod=$(this).attr('type_podpiska');
	
			$.ajax({  
				type: "POST",  
				url: "/ajax/ajax_podpiska.php",  
				data: "type="+type_pod+"&id_user_to="+id_user_to+"&id_user_from="+id_user_from,  
				
						
				beforeSend: function(){
					$("#podpiska_blog").attr('disabled','disabled');
					$("#podpiska_blog").attr('value','Сохранение...'); 
					},
				success: function(html){ 
					$("#podpiska_blog").removeAttr('disabled');
	 				var type_pod=$("#podpiska_blog").attr('type_podpiska');
					
					if(type_pod=='1'){
						$("#podpiska_blog").attr('value','Отписаться от блога');
						$("#podpiska_blog").attr('type_podpiska','0');
					}else{
						$("#podpiska_blog").attr('value','Подписаться на блог');
						$("#podpiska_blog").attr('type_podpiska','1');
					}; 
					$("#podpiska_response").html(html);
					
				}
			});
		});
		

				
	// $("table[id^=table_] tr[name='tr_spis'] td[name!='area_href']").click(function(){
		// var id = $(this).parents('tr').attr('id');
		// var href = "?page=user&id_user="+id;
		// location.href=href;
	// });
	
	// $('button[href],tr[href]').live('click',function(){
		// var href = $(this).attr('href');
		// if(href){
			// $(this).addClass('href');
			// window.location.href=href;
		// };
	// });

	
	
	
///////////////////////////////////////////
///////////////////////////////////////////
//////// БЛОКНОТ //////////////////////////
///////////////////////////////////////////


	$('#notepad').blur(function(){
		var notepad_val = $(this).val();
		$.ajax({  
			type: "POST",  
			url: "/ajax/ajax_edit_notepad.php",  
			data: { notepad: notepad_val},
			success: function(response){  
				$("#edit_notepad").html(response);  
				}  
		});  
	});
	
	
	
	


	
	
	
	
	




	
		
	
///////////////////////////////////////////
///////////////////////////////////////////
//////// ОТПРАВКА ФОРМ ////////////////////
///////////////////////////////////////////
		
		
		
	// готовим объект
	// var options = {
	  // target: "#update",
	  // url: "/ajax/ajax_send_form.php",
	  // beforeSubmit: function() {
			// /* alert("Проверяю"); */
	  // },
	  // success: function(response) {
			// $("#update").show();
	  // }
	// };
	// передаем опции в  ajaxSubmit
	// $("#form_sbornaya_edit").ajaxForm(options);
	// $("#form_sp_tr_edit").ajaxForm(options);
	// $("#from_sor_edit").ajaxForm(options);

	
	
	
///////////////////////////////////////////
///////////////////////////////////////////
//////// ГОРОДА И ЗАЛЫ ////////////////////
///////////////////////////////////////////

	
	
	// Добавление города
	/* $('#add_city').click(function(){
		dialog_add_city();
	}); */
	
	
		
	// Изменение и добавление зала
	/* $('button[name=edit_zal], #add_zal').live('click',function(){
		var id_zal = $(this).parents('tr').attr('id_zal');
		dialog_add_zal(id_zal);
	}); */
	
	
	// Удаление зала
	$('button[name=del_zal]').click(function(){
		var tr_zal = $(this).parents('tr');
		var id_zal = tr_zal.attr('id_zal');
		var div_dialog = $('#dialog-del-zal');
		// Теперь вызываем диалог в котором уже все данные есть
		div_dialog.dialog({
			resizable: false,
			height:150,
			width: 300,
			modal: true,
			buttons: {
				"Удалить": function() {
					// СОХРАНЯЕМ, Отправляем данные серверу
						$.ajax({
							type: "POST",
							url: "/ajax/ajax_send_form.php",
							data: { action: 'del_zal', id_zal:parseInt(id_zal)},
							cache: false,
							success: function(response){
								alert(response);
								tr_zal.remove();
								$('#update').html("Зал успешно удалён").show();	
							}
						});
						$( this ).dialog( "close" );
				}
			}
		});
	});
	
	

	
	
	
	///////////////////////////////////////////
	///////////////////////////////////////////
	//////// СБОРНАЯ  /////////////////////////
	///////////////////////////////////////////
	
		
	// $("#sbornaya_type").change(function() {
		// $("input[id^=token_org]").parents('p').detach();
		// var val = $(this).val();
		// if(val!=0){
			// add_token_org(val);
		// };
	// });
	
	
	
	
	
	
	
	

	
// $("#id_feder").change(function () {
        // var id_feder = $(this).val();
		// var id_user = $('#id_user').val();
		// var new_sel="<option value='0'>НЕ ВЫБРАНО</option>";
		// var new_sel_load="<option value='0'>Загрузка...</option>";
		// $("#id_section").html(new_sel);
		// $("#id_section").attr('disabled','disabled');
		// $("#id_club").html(new_sel);
		// $("#id_club").attr('disabled','disabled');
		// $("#id_trener").html(new_sel);
		// $("#id_trener").attr('disabled','disabled');
		
		
        // if(id_feder!=0){
                // $.ajax({
                        // type: "POST",
                        // url: "/ajax/ajax_select.php",
                        // data: { action: 'showClub', id_feder: id_feder, id_user:id_user},
                        // cache: false,
						// beforeSend: function(){
							// $("#id_club").html(new_sel_load);
						// },
                        // success: function(response){ 
							// $("#id_club").html(new_sel);
							// $("#id_club").append(response);
							// $("#id_club").removeAttr('disabled');
						// }
                // });
		// };
	// });

	
	
	// $("#id_club").change(function () {

        // var id_club = $(this).val();
		// var id_user = $('#id_user').val();
		// var city=$('#id_city').val();
		// var new_sel="<option value='0'>НЕ ВЫБРАНО</option>";
		// var new_sel_load="<option value='0'>Загрузка...</option>";
		// $("#id_section").html(new_sel);
		// $("#id_section").attr('disabled','disabled');
		// $("#id_trener").html(new_sel);
		// $("#id_trener").attr('disabled','disabled');
				// $.ajax({
					// type: "POST",
					// url: "/ajax/ajax_select.php",
					// data: { action: 'showSection', id_club: id_club, city: city, id_user : id_user},
					// cache: false,
					// beforeSend: function(){
						// $("#id_section").html(new_sel_load);
					// },
					// success: function(response){ 
						// $("#id_section").html(new_sel);
						// $("#id_section").append(response);
						// $("#id_section").removeAttr('disabled');
					// }
			// });
	// });
	
	// $("#id_section").change(function () {
        // var id_section = $(this).val();
		// var id_user = $('#id_user').val();
		// var new_sel="<option value='0'>НЕ ВЫБРАНО</option>";
		// var new_sel_load="<option value='0'>Загрузка...</option>";
		// $("#id_trener").html(new_sel);
		// $("#id_trener").attr('disabled','disabled');
		// if(id_section!=0){
			// $.ajax({
					// type: "POST",
					// url: "/ajax/ajax_select.php",
					// data: { action: 'showTrener', id_section: id_section, id_user : id_user },
					// cache: false,
					// beforeSend: function(){
						// $("#id_trener").html(new_sel_load);
					// },
					// success: function(response){ 
						// $("#id_trener").html(new_sel);
						// $("#id_trener").html(response);
						// $("#id_trener").removeAttr('disabled');
					// }
			// });
		// };
	// });	
		
		
		
		
		
		
		
		
		
	
	
	
	
	
	
	
	///////////////////////////////////////////
	///////////////////////////////////////////
	//////// Token - автозаполнение ///////////
	///////////////////////////////////////////
	$('#sor_id_city').change(function(){
		if($(this).val()){
			token_org('add', 'zal', 'id_p_hid_city', 'p');
			$('#sor_id_zal').show();
		}else{
			$('#id_p_hid_zal').remove();
		};
	});

	// $("input[type=hidden][for][value!='']").each(function(index) {
	// var for_id = $(this).attr('for');
	// var val = $(this).val();
	// var type_token = $(this).attr('data-type-token');
	// $.ajax({
		// type: "POST",	url: "/ajax/ajax_autocomplete.php?action=show_"+type_token+"&type=id",
		// data: {id: val},
		// success: function(response){
			// var obj = JSON.parse(response);
			// $('#'+for_id).tokenInput("add",{'id': val, 'title':  obj[0].title, 'obl_title': obj[0].obl_title});
		// }
	// });
// });

// Автодобавление города при поиске

	
	//token_org('init', 'city');
	//token_org('init', 'zal');



		

	
	
	
	
/////////////////////////////////////////
//////// СОРЕВНОВАНИЯ ///////////////////
/////////////////////////////////////////	
	
	function sor_mest(){
		var mest = $('#sor_type option:selected').attr('mest');
		$('#mest').val(mest);
	};
	sor_mest();
	
	$('#sor_type').change(function() {
		sor_mest();
	});
	

	
	function check_sliders(pol){
/* 
		//Проверяем все слайдеры, кроме текущего, на ошибки
		//var kol = $('#sor_voz_add_'+pol+' div.slider').length;
	
		$('#sor_voz_add_'+pol+' div.slider').each(function(){
		 	var er = false;
			var current_s = $(this).slider("values",0);
			var current_f = $(this).slider("values",1);
			var current_table_slider = $(this).parents('table'); 
			$('#prov').val(i+" "+er+" "+current_s+" "+current_f);

			$('#sor_voz_add_'+pol+' div.slider').slice(i,kol).each(function(){
				var tek_s = $(this).slider("values" , 0);
				var tek_f = $(this).slider("values" , 1);
				
				if(tek_s<=cur_s && cur_s<=tek_f){er=true;};						
				if(tek_s<=cur_f && cur_f<=tek_f){er=true;};				
				if(tek_s>cur_s && cur_f>tek_f){er=true;};
				
			});
			
			
			
			
			// Если есть ошибка, сообщаем
			if(er || cur_s==6){
				cur_table_slider.attr('class','panel_er');
				cur_table_slider.find('input.er_voz').val('1');
			}else{
				cur_table_slider.attr('class','panel');
				cur_table_slider.find('.er_voz').val('0');
			};
			  
		 });*/
	};
	
	
	
	
	function add_slider(pol, val_age_from, val_age_to, arr_ves, rounds, time, rounds_final, time_final){

		// Генерируем ID Слайдера
		var id_slider = randomNumber(10,99999);
		// Инициализируем переменные по умолчанию
		var pol = pol || 1;
		var val_age_from = val_age_from || 6;
		var val_age_to = val_age_to || 6;
		var amount_s = "";
		var amount_f = "";
		var def_ves = "";
		// var er_voz_val = '1';
		// var class_panel = "panel_age_er";
		var er = 0;
		var class_panel = "panel_age";
			
		// Проверяем весовые категории
		if(arr_ves){
			for (var i in arr_ves) {
				def_ves +="<input type='number' max=99 min=20 class='ves' value="+arr_ves[i]+">";
			};
		}else{
			def_ves = "<input type='number' max=99 min=20 class='ves'>";
		};
		
		if(val_age_from!=6 && val_age_to!=6){
			class_panel = "panel_age";
			if(val_age_to==22){amount_f = '->';}else{	amount_f = val_age_to;};
			amount_s = val_age_from;
			er = 0;
		};
		
		// Текст нового слайдера
		var new_slider = "		<table class='"+class_panel+"'>			<tr>	<td  width='150' align='left'>Возраст(лет):</td>				<td width='50' align='left'><input type='hidden' class='pol' value='"+pol+"'><input type='hidden' class='age_from' value='"+val_age_from+"'><input type='hidden' class='age_to' value='"+val_age_to+"'><input type='hidden' class='er' value='" + er + "'><input type=text class='amount' readonly value='"+amount_s+"-"+amount_f+"'> </td>				<td colspan='2'><div class='slider' id_slider='"+id_slider+"'></div></td>				<td width=70><button class='but_del' id='del_age'>Удалить</button></td>			</tr>				<tr class='tr_ves_kat'>				<td align='left'>Весовые категории:</td>				<td colspan='3' align='left'>"+def_ves+"</td>			<td><button class='but_add_ves' id='add_ves'>Добавить</button></td>	</tr>	<tr>				<td align='left'>Раунды/Время(сек):</td>				<td colspan='2' wirth='230' align='left'><input type='number' min='1' max='5' step='1' class='rounds' value='"+rounds+"'> / <input type='number' min='60' max='300' step='15' class='time' value='"+time+"'></td> <td width='230'>Финал: <input type='number' min='1' max='5' step='1' class='rounds_final' value='"+rounds_final+"'> / <input type='number' min='60' max='300' step='15' class='time_final' value='"+time_final+"'></td>			<td></td>	</tr>	</table>";
		$('#sor_voz_add_'+pol).append(new_slider);
		
		// Инициализируем кнопки
		$('#sor_voz_add_'+pol+' button.but_del' ).button({ icons: {primary:'ui-icon-circle-close'},text: false});
		$('#sor_voz_add_'+pol+' button.but_add_ves' ).button({ icons: {primary:'ui-icon-circle-plus'},text: false});
		
		// Инициализируем слайдер
		$('#sor_voz_add_'+pol+' div[id_slider='+id_slider+']').slider({
			range: true,
			min: 6,
			max: 22,
			values: [ val_age_from, val_age_to ],
			slide: function( event, ui ) {
				var table_slider = $(this).parents('table');
				var id_slider = $(this).attr('id_slider');				
				var cur_s = ui.values[ 0 ];
				var cur_f = ui.values[ 1 ];
				
				// Присваиваем переменные слайдера таблице и скрытым переменным
				if(cur_f==22){	var fi = "->";	}else if(cur_f==6){var fi = "";}else{	var fi = cur_f;};
				if(cur_s==6){	var si = "";	}else{	var si = cur_s;};
				table_slider.find('.amount').val(si + "-" +  fi);
				table_slider.find('.age_from').val(cur_s);
				table_slider.find('.age_to').val(cur_f);
				
				// Проверяем текущий слайдер на ошибки	
				check_sliders(pol);
			}
		});

	};
	
	
	
	
	$('#add_age').click(function(){
		var pol = $(this).attr('pol');
		add_slider(pol);
		return false;
	});
	$('#add_def').click(function(){
		var id_sports = $('#sor_sports option:selected').val();
		if(id_sports!=0){
			var pol = $(this).attr('pol');
			
			// очищаем для текущего пола возрастные группы
			$('#sor_voz_add_'+pol+' div.slider').parents('table').remove();
			
			// получаем данные по умолчания для вида спорта
			$.ajax({
                        type: "POST",
                        url: "/ajax/ajax_sports.php",
                        data: {action: 'get_def_ves',  id_sports: id_sports, pol: pol},
                        cache: false,
                        success: function(data){ 
							var obj = JSON.parse(data);
							$.each(obj, function (index, age) {
								// Запускаем создание
								add_slider(pol,age.from,age.to,age.ves,age.rounds,age.time,age.rounds_final,age.time_final);
							}); 				
						}
              });
		}else{
			alert ('Введите правила соревнований');
		};		
		return false;
	});
	$('#save_def, #save_sor').click(function(){
		var id_sports = $('#sor_sports option:selected').val();
		var save_type = $(this).attr('id');
		if(id_sports!=0){

			var vess = new Array();
			$('table.panel_age').each(function(){
				var age_from = $(this).find('input.age_from').val();
				var age_to = $(this).find('input.age_to').val();
				var pol = $(this).find('input.pol').val();
				var er = $(this).find('input.er').val();
				var ves = [];
				var time = $(this).find('input.time').val();
				var rounds = $(this).find('input.rounds').val();
				var time_final = $(this).find('input.time_final').val();
				var rounds_final = $(this).find('input.rounds_final').val();
				
				$(this).find('input.ves').each(function(){
				var val = $(this).val();
					if(!in_array(val,ves) && val){
						ves.push(val);
					};
				});

				if(er==0 && ves.length){
					var age = {from:age_from, to:age_to, pol:pol, er:er, time:time, rounds:rounds, time_final:time_final, rounds_final:rounds_final, ves: ves.sort()};
					vess.push(age);
				};
			});
			

			// посылаем данные о весовых каттегориях
			var div_dialog = $('#dialog-save-sports');
				div_dialog.dialog({
				resizable: false,
				height:150,
				width:300,
				modal: true,
				buttons: {
					"Продолжить": function() {
						$.ajax({
							type: "POST",
							url: "/ajax/ajax_sports.php",
							data: { action: 'save_def_ves', id_sports: id_sports, vess:JSON.stringify(vess), save_type: save_type},
							cache: false,
							success: function(data){
								if(data){					
									$('#update').html(data).show();	
								}
							}
						});
						$( this ).dialog( "close" );
					}
				}
			});
		

		
		}else{
			alert ('Введите правила соревнований');
		};		
		return false;
	});
/* 	$('#del_age').live('click', function() {
		$(this).parents('table').remove();
		return false;
	});
	$('#sor_tabs_kat input.ves').live('blur', function() {
		if(!$(this).val()){	$(this).remove();};
		return false;
	});
	$('#add_ves').live('click', function() {
		var id_slider = $(this).parents('table').find('div.slider').attr('id_slider');
		$(this).parent('td').prev('td').append("<input type='number' max=99 min=20 class='ves' name='ves["+id_slider+"][]'>");
		return false;
	}); */
	$('#sor_sports').change(function(){
	// очищаем для текущего пола возрастные группы
			$('#sor_voz_add_1 div.slider').parents('table').remove();
			$('#sor_voz_add_0 div.slider').parents('table').remove();
			$('#text_id_sports').html($('#sor_sports option:selected').html());
	});
	
	

	
	
	
	
	
	
	
	
	
/////////////////////////////////////////
//////// СООБЩЕНИЯ //////////////////////
/////////////////////////////////////////	
	
	
	


		//Отправка сообщения
	$("#go_add_mess").click(function(){
		var to_id = $('#add_user_new_mess').val();
		var text = $('#text_new_mess').val();
		var theme_mess = $('#theme_mess').val();
		var sms = $('#check_sms').attr('checked');
		var email = $('#check_email').attr('checked');

		if(to_id!='' && text!=''){
			$.ajax({  
			type: "POST",  
			url: "/ajax/ajax_send_mess.php?add_mess",  
			data: "text=" + text + "&theme="+theme_mess+"&to_id=" + to_id+"&sms="+ sms + "&email=" +email,	
			beforeSend: function(){
				$('#go_add_mess').attr('disabled','true');
				$('#go_add_mess').attr('value','Отправка...');
				},
			success: function(html){  
				$("#window_add_mess").html(html);
				$("#messages").tabs('load', 0);
				$("#messages").tabs('load', 1);
				}  
			}); 
		}else{
			alert ('Не все поля заполнены');
		}
	});
	
	
	
	
	// Прочтение сообщения или новое сообщение через панель
	
	$("td.text_mess, div.panel[id=0]").click(function(){
		$.ajax({  
        type: "POST",  
        url: "/ajax/ajax_send_mess.php",  
        data: "id_mess="+$(this).attr('id'),  
        success: function(html){  
			$("#new_mess").html(html);
			$("#messages").tabs("select", 2);
			}  
		}); 
		
	});
	
	
	// Чекбоксы в сообщениях

	
	function clear_checkbox_mess(){
		$('input[type=checkbox]').removeAttr("checked");
		$('#check_null_in').css('display','none');
		$('#check_all_in').css('display','');
		$('#check_null_out').css('display','none');
		$('#check_all_out').css('display','');
		$('tr[status=1]').removeAttr('class');
		$('tr[status=0]').attr('class','row_table_mess_0');
		
	};
		
	$('table[class=table_message] input[type=checkbox]').click(function(){
		$(this).parents('tr').removeAttr('class');
		$('input[type=checkbox]:checked').parents('tr').attr('class','table_message_check');
		$('div[id=check_mess]').parents('tr').attr('class','');
	});	
	
	$('#check_null_in').click(function(){
		clear_checkbox_mess();
	});
	
	$('#check_null_out').click(function(){
		clear_checkbox_mess();
	});	
		
	$('#check_all_in').click(function(){
		clear_checkbox_mess();
		$('tr[type=in] input[type=checkbox]').attr('checked','true');
		$('input[type=checkbox]:checked').parents('tr[type=in]').attr('class','table_message_check');
		$('#check_null_in').css('display','');
		$('#check_all_in').css('display','none');	
	});
	
	$('#check_1_in').click(function(){
		clear_checkbox_mess();
		$('tr[status=1][type=in]').attr('class','table_message_check');
		$('tr[status=1][type=in] input[type=checkbox]').attr('checked','true');
	});
	
	$('#check_0_in').click(function(){
		clear_checkbox_mess();
		$('tr[status=0][type=in]').attr('class','table_message_check');
		$('tr[status=0][type=in] input[type=checkbox]').attr('checked','true');
	});
	
	$('#check_all_out').click(function(){
		clear_checkbox_mess();
		$('tr[type=out] input[type=checkbox]').attr('checked','true');
		$('input[type=checkbox]:checked').parents('tr[type=out]').attr('class','table_message_check');
		$('#check_null_out').css('display','');
		$('#check_all_out').css('display','none');	
	});
	
	$('#check_1_out').click(function(){
		clear_checkbox_mess();
		$('tr[status=1][type=out]').attr('class','table_message_check');
		$('tr[status=1][type=out] input[type=checkbox]').attr('checked','true');
	});
	
	$('#check_0_out').click(function(){
		clear_checkbox_mess();
		$('tr[status=0][type=out]').attr('class','table_message_check');
		$('tr[status=0][type=out] input[type=checkbox]').attr('checked','true');
	});


	
/////////////////////////////////////////
//////// ОПЛАТА /////////////////////////
/////////////////////////////////////////	
	
	
	
	$("td.opl_summa").click(function() {	
	
			var month=$(this).attr('month');
			var year=$(this).attr('year');
			var id_user=$(this).attr('id_user');
			var summa=$(this).text();
					
			$(this).html("<input type=text onBlur=auto_save_oplata(); style='width:39px; margin:0; text-align:center;' month="+month+" year="+year+" id_user="+id_user+" id='edit_opl_abonement' autofocus value="+summa+">");
			
	
	});
	
	$("#change_year_oplata").change(function() {	
		var year=$(this).val();
		var href = "?page=user&abonement&year="+year;
		location.href=href;
	});
	
	
	
	$("#but_oplata").click(function () {
		var to_id = $('#add_user_new_oplata').val();
		var summa = $('#summa').val();
		var oplata_oper = $('#oplata_oper option:selected').val();
		var opl_month = $('#opl_month option:selected').val();
		var opl_year = $('#opl_year option:selected').val();
			$.ajax({  
				type: "POST",  
				url: "/ajax/ajax_oplata.php",  
				data: "to_id="+to_id+"&summa="+summa+"&oplata_oper="+oplata_oper+"&opl_month="+opl_month+"&opl_year="+opl_year,  
				beforeSend: function(){
					$("#but_oplata").attr('disabled','disabled');
					},
				success: function(html){
					$("#update").html(html);
				}
			});
	});

	
	$("#oplata_oper, #add_user_new_oplata, #summa, #opl_month, #opl_year").change(function () {
		
		if($('#oplata_oper').val()=='0' || $("#add_user_new_oplata").val()=='' || $("#summa").val()=='' || (($('#oplata_oper').val()=='2' || $('#oplata_oper').val()=='3')  && ($("#opl_month").val()=='0' || $("#opl_year").val()=='0'))){
			$("#but_oplata").attr('disabled','disabled');
		}else{
			$("#but_oplata").removeAttr('disabled');
		};

		if($('#oplata_oper').val()=='2' || $('#oplata_oper').val()=='3'){
			$("#opl_month").removeAttr('disabled');
		}else{
			$("#opl_month").attr('disabled','disabled');
		}
		
		if($('#opl_month').val()!='0'){
			$("#opl_year").removeAttr('disabled');
		}else{
			$("#opl_year").attr('disabled','disabled');
		}
		
	});



 });