
	
///////////////////////////////////////////
///////////////////////////////////////////
//////// ЗАГРУЗКА ФОТО ////////////////////
///////////////////////////////////////////
	
	
	$('#fileupload').fileupload({
		dataType: 'json',
		url: 'ajax/ajax_upload_img.php',
		done: function (e, data) {
			$.each(data.result, function (index, file) {
				$('<p/>').text(file.name).appendTo('body');
			});
		}
	});
	
	function updateCoords(c){
		$('#crop_x').val(c.x);
		$('#crop_y').val(c.y);
		$('#crop_w').val(c.w);
		$('#crop_h').val(c.h);
	};
	
	
	$('#avatar_upload').fileupload({
		type: 'POST',
		acceptFileTypes: ' /(\.|\/)(jpe?g)$/i',
        dataType: 'json',
        url: 'ajax/ajax_upload_img.php',
		autoUpload: true,
		sequentialUploads: true,
		send: function (e, data) {
			$('#progressbar').show();
			$('#avatar').hide();
			$('#div_avatar_upload').hide();
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$("#progressbar").progressbar( "option", "value", progress );
		},
		done: function (e, data) {
			$.each(data.files, function (index, file) {
				$('#progressbar').hide();
				$('#avatar').attr('src',data.result[index].url);
				$('#avatar').show();
				$('#avatar').Jcrop({
					aspectRatio: 3/4,
					onSelect: updateCoords,
					onChange: updateCoords
				});
				
				$('#div_crop').show();
			});
		}
   });
   
   	$('#but_crop').click(function(){
		var x = $('#crop_x').val();
		var y = $('#crop_y').val();
		var w = $('#crop_w').val();
		var h = $('#crop_h').val();
		var src = $('#avatar').attr('src');
		if(w>150){
			$.ajax({
				type: "POST",
				dataType: 'json',
				url: "ajax/ajax_crop_img.php",
				data: { action: 'cropImg', src: src, x:x, y:y, w:w, h:h},
				beforeSend: function(){
					$('#progressbar').show();
					$('#div_avatar').hide();
					$('#div_crop').hide();
				},
				success: function(e,response){
					var obj = JSON.parse(response);
					alert(obj.dir);
					/* var src_img = response;
					$('#progressbar').hide();
					$('#avatar_crop').attr('src', src_img);
					$('#div_avatar_crop').show(); */
					
				}
			});
		}else{
			alert('Выделенная область должна быть больше 150px');
		};
	});
