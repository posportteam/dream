$(document).ready(function(){
	$('#albs .alb_del').live('click',function(e){
		var id = $(this).attr('data-alb-id');
		$.ajax({  
			type: "POST",  
			url: "/ajax/ajax_photo.php?action=alb_del",  
			data: {id: id},  
			beforeSend: function(){
				
			},
			success: function(response){ 
				// var obj = JSON.parse(response);
				// var error = obj['error'];
				// alert(error);
				$('#alb_'+id).hide(1000);
			}
		});
	});
	
	$('div.alb_avatar, div.alb_info').live('click',function(e){
		var href = $(this).attr('href');
		if(href){
			location.href = href;
		}
	});
	
});