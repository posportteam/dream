///////////////////////////////////////////
///////////////////////////////////////////
//////// АВТОЗАПОЛНЕНИЕ ///////////////////
///////////////////////////////////////////

function init_button(){
		//Работа кнопок
		$('div.buttonset').buttonset();
		$('button, input[type=submit]').button();
		
		
		//Работа кнопок с иконками
		$( ".but_add" ).button({ icons: {primary:'ui-icon-circle-plus'}});
		$( ".but_add_small" ).button({ icons: {primary:'ui-icon-circle-plus'},text: false});
		$( ".but_save" ).button({ icons: {primary:'ui-icon-disk'}});
		$( ".but_save_small" ).button({ icons: {primary:'ui-icon-disk'},text: false});
		$( ".but_del" ).button({ icons: {primary:'ui-icon-trash'}, text: false});
		$( ".but_edit_small" ).button({ icons: {primary:'ui-icon-wrench'},text: false});
		$( ".but_edit" ).button({ icons: {primary:'ui-icon-wrench'}});
		$( ".but_details" ).button({ icons: {primary:'ui-icon-script'},text: false});
		$( ".but_doc" ).button({ icons: {primary:'ui-icon-document'}});
		$( ".but_doc_small" ).button({ icons: {primary:'ui-icon-document'},text: false});
		$( ".but_my" ).button({ icons: {primary:'ui-icon-person'}});
		$( ".but_sub" ).button({ icons: {primary:'ui-icon-star'}});
		$( ".but_fav" ).button({ icons: {primary:'ui-icon-heart'}});
		$( ".but_sms" ).button({ icons: {primary:'ui-icon-mail-closed'}});
		$( ".but_print" ).button({ icons: {primary:'ui-icon-print'}});
		$( ".but_flag" ).button({ icons: {primary:'ui-icon-flag'}});
		$( ".but_search_full" ).button({ icons: {primary:'ui-icon-search'}});
		$( ".but_filter" ).button({ icons: {primary:'ui-icon-lightbulb'},text: false});
		$( ".but_more" ).button({ icons: {primary:'ui-icon-arrowstop-1-s'},text: false});
		$( ".but_users" ).button({ icons: {primary:'ui-icon-person'},text: false});
		$( ".but_arhiv_small" ).button({ icons: {primary:'ui-icon-cancel'},text: false});
		$( ".but_stat" ).button({ icons: {primary:'ui-icon-bookmark'}});
		$( ".but_sor_poloj" ).button({ icons: {primary:'ui-icon-document'}});
		$( ".but_sor_reg" ).button({ icons: {primary:'ui-icon-power'}});
		$( ".but_sor_req" ).button({ icons: {primary:'ui-icon-arrowthickstop-1-e'}});
		$( ".but_sor_result" ).button({ icons: {primary:'ui-icon-script'}});
		$( ".but_sor_video" ).button({ icons: {primary:'ui-icon-video'}});
		$( ".but_sor_photo" ).button({ icons: {primary:'ui-icon-image'}});
		
		// Toolbar
		$( ".but_search" ).button({ icons: {primary:'ui-icon-search'},text: false});
		$( ".but_cart" ).button({ icons: {primary:'ui-icon-cart'},text: false});
		$( ".but_home" ).button({ icons: {primary:'ui-icon-home'},text: false});
		$( ".but_my_small" ).button({ icons: {primary:'ui-icon-person'},text: false});
		
		// Регистрация
		$( ".but_reg_next" ).button({ icons: {secondary:'ui-icon-circle-triangle-e'}});
		$( ".but_reg_last" ).button({ icons: {primary:'ui-icon-circle-triangle-w'}});
		
		// Пагинатор
		$( ".but_first" ).button({ icons: {primary:'ui-icon-seek-first'},text: false});
		$( ".but_next" ).button({ icons: {primary:'ui-icon-seek-next'},text: false});
		$( ".but_prev" ).button({ icons: {primary:'ui-icon-seek-prev'},text: false});
		$( ".but_end" ).button({ icons: {primary:'ui-icon-seek-end'},text: false});
		$( ".but_page" ).button();

	};
	
	function setnull_for_obj(obj){
		obj.find('input[type!=hidden],select')
			.attr('data-defval','')
			.attr('data-id-club','')
			.attr('data-id-org','')
			.val('');
		obj.find('select').val(0).removeAttr('disabled');
		obj.find('input[type!=hidden]').val('').removeAttr('disabled');
	};
	
	
	function convert_date2sql(date){
		if(date){
			var arr = date.split('.');
			if(arr.length){
				var new_date = arr[2] + '-' + arr[1] + '-' + arr[0];
				return new_date;
			}else{
				alert('Error format date from SQL');
				return false;
			};
		};
	};

	function parse_url(){
		var tmp = new Array();      // два вспомагательных
		var tmp2 = new Array();     // массива
		var param = new Array();
		var get = location.search;  // строка GET запроса

		if(get != '') {
			tmp = (get.substr(1)).split('&');   // разделяем переменные
			for(var i=0; i < tmp.length; i++) {
				tmp2 = tmp[i].split('=');       // массив param будет содержать
				param[tmp2[0]] = tmp2[1];       // пары ключ(имя переменной)->значение
			}
		}
		return param;
	};
	
	function check_not_null_field(obj, er){
		var response = true;
		if(er){er = true;}else{er = false;};
		obj.find('input[required], select[required]').each(function(n,element){
			var id = $(this).attr('id');
			var val = $(this).val();
			if(!val){
				if(er){	$(this).addClass('error'); };
				response = false;
			}else{
				$(this).removeClass('error');
			};
		});
		return response;
	};

	
	// По умолчанию выставлем занчения города и спорта если таковые есть
/* 	function install_def_auto_city(val){
		
		var defval = $('input.auto_city').attr('data-defval');
		
		if(!val){
			if(defval){
				var val = defval
			}else{
				var val = parseInt($.cookie("site_city"));
			};
		};
		
		
		
		if(val){
			 $.ajax({
				type: "POST",	
				url: "/ajax/ajax_autocomplete.php?action=show_city&type=id",
                data: {id: val},
				success: function(response){
					var obj = JSON.parse(response);
					$('input.auto_city').val(obj[0].title);
					$('input.auto_city').tokenInput("add",{'id': obj[0].id, 'title':  obj[0].title, 'obl_title': obj[0].obl_title});
				}
			});
		};
	}; */

	
	function check_tel(telephone){

		var	response = $.ajax({
			type: "POST",	
			url: "ajax_auth.php", 
			cache: false, 
			async: false,
			data: { action: 'checkTel',  telephone:  telephone},
			dataType: 'json',
			beforeSend: function(){
				$('#img_ajax_checktel').show();
			}
		}).responseText;
		
		if(response!='null'){
			var checktel = true;
		}else{
			var checktel = false;
		};
		$('#img_ajax_checktel').hide();
		return checktel;
	};
	
	

	
	
//////////////////////////////////////////////
//////////////////////////////////////////////
//// Перевод в другой формат номера телефона//
//////////////////////////////////////////////

	function reform_mobile_phone(mobile_phone){
		var reg = /\D/g;
		var phone = mobile_phone.replace(reg,'');
		if(phone.length>9){
			var sim_st = phone.length - 10;
			phone = phone.substring(sim_st);
			if(phone.substring(0,1)=='9'){
				return phone;
			};
		};
	};
	
	
///////////////////////////////////////////
///////////////////////////////////////////
//////// СВЯЗНЫЙ СПИСОК ///////////////////
///////////////////////////////////////////

	function linklist(current_obj, load, current_load, data, par_obj){
		var par_obj = par_obj || 0;
		// Событие "загрузка по умолчанию", мы запускаем присвоение рекурсивным способом
		var defval = parseInt(current_obj.attr('data-defval'));
		if(defval && load){
			current_obj.val(defval);
		};

		
		var cur_select = parseInt(current_obj.val());
		
		
		
		var new_sel="<option value=0>НЕ ВЫБРАНО</option>";
		var new_sel_load="<option>Загрузка...</option>";

		if(par_obj){
			var next_select = current_obj.parent(par_obj).nextAll(par_obj+':first').find('select.linklist');
			current_obj.parent(par_obj).nextAll(par_obj).find('select.linklist').html(new_sel).attr('disabled','disabled');
       }else if(current_load){
			var next_select = current_obj;
			current_obj.nextAll('select.linklist').html(new_sel).attr('disabled','disabled');
	   }else{
			var next_select = current_obj.nextAll('select.linklist:first');
			current_obj.nextAll('select.linklist').html(new_sel).attr('disabled','disabled');
		};
		
		//alert(next_select.attr('data-linklist-name'));
		
		if(next_select.attr('data-linklist-name')){
			if(!data){
				var data = {
					'id'		:	cur_select
				};
			};
		
			//alert(cur_select);
			
			var action = 'show_'+next_select.attr('data-linklist-name');
                $.ajax({
                        type: "POST",
                        url: "/ajax/ajax_autocomplete.php?action="+action+"&type=select",
                        data: data,
						beforeSend: function(){
							next_select.html(new_sel_load);
						},
                        success: function(response){
							var obj = JSON.parse(response);
							var options = "";
							var one_val=0;
							$.each(obj, function (index, option) {
								if(option.value){
									var title = option.value;
								}else if(option.name && option.second_name){
									var title = option.name + " " + option.second_name;
								}
								options+="<option value='"+option.id+"'>"+title+"</option>";
								one_val=option.id;
							});
							
							next_select.html(new_sel).append(options).removeAttr('disabled');
							
							// Рекурсия
							if(obj.length==1){
								
								next_select.val(one_val);
							};
							//alert(one_val);
							linklist(next_select,1);

						}
                });
		};
	};	
	
	
	
///////////////////////////////////////////
///////////////////////////////////////////
//////// КАЛЕНДАРЬ ////////////////////////
///////////////////////////////////////////

function init_datepicker(){

	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '&#x3c;Пред',
		nextText: 'След&#x3e;',
		currentText: 'Сегодня',
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',	'Июл','Авг','Сен','Окт','Ноя','Дек'],
		dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		weekHeader: 'Нед',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	
	$.datepicker.setDefaults($.datepicker.regional['ru']);
	
	$('input.datepicker').datepicker();
	
	$('input.datepicker_from').datepicker({
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ){
			$(this).parent('div').find('.datepicker_to').datepicker("option","minDate",selectedDate);
		}
	});
	
	$('input.datepicker_to').datepicker({
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ){
			$(this).parent('div').find('.datepicker_from').datepicker("option","maxDate",selectedDate);
		}
	});
	
	
	/* $('input.datepicker_from').live('change',function(e){
		var val = $(this).val();
		if(val){
			$(this).parent('div').find('.datepicker_to').removeAttr('disabled');
		}else{
			$(this).parent('div').find('.datepicker_to').attr('disabled','true').val('');
			$(this).parent('div').find('.datepicker_from').datepicker("option","maxDate", null);
		}
	});	 */
};	
	
	
///////////////////////////////////////////
///////////////////////////////////////////
//////// УДАЛЕНИЕ ПРОБЕЛОВ ////////////////
///////////////////////////////////////////

function nospace(str) { 
	var VRegExp = new RegExp(/^(\s|\u00A0)+/g); 
	var VResult = str.replace(VRegExp, ''); 
	return VResult 
};


///////////////////////////////////////////
///////////////////////////////////////////
//////// ГЕНЕРАЦИЯ ЧИСЛА //////////////////
///////////////////////////////////////////
	function randomNumber(m,n){
		var m = parseInt(m); var n = parseInt(n);
		return Math.floor( Math.random() * (n - m + 1) ) + m;
	};
	
///////////////////////////////////////////
///////////////////////////////////////////
//////// ПРОВЕРКА В МАССИВЕ ///////////////
///////////////////////////////////////////
	
	function in_array(val, ar)	{ // Функция проверяет наличие элемента в массиве
		if (typeof(ar) != 'object' || !ar || !ar.length) {
			return false;
		}

		for (key in ar) {
			if (val == ar[key]) {
				return true;
			}
		}
		return false;
	}
	
