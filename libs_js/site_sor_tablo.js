 $(function() {
 
 	
///////////////////////////////////////////////
////////// Объявления /////////////////////////
///////////////////////////////////////////////
 
 
	$('input.play').button({  icons: {     primary: "ui-icon-play"    },     text: false    });
	$('input.stop').button({  icons: {     primary: "ui-icon-stop"    },     text: false    });
	
	var timerFlag = $('#timerFlag');
	var timerGo;
	var timerUder;
	var timeFight;
	
///////////////////////////////////////////////
////////// События ////////////////////////////
///////////////////////////////////////////////
	
	// Таймер
	$('#timerStartStop').click(function(){
		controlTimer();
	});
	
	
	// Удержание
	$('#uder1, #uder0').click(function(){
		if($(this).prop('id')=='uder1'){
			ugol = 1;
		}else{
			ugol = 0;
		};
		controlUder(ugol);
	});
	
	// События
	$('button.event').click(function(){
		var ugol = parseInt($(this).attr('data-ugol'));
		var key = $(this).attr('data-key');
		controlEvent(ugol, key);
	});
	
	$('input.event[type=radio]').change(function(){
		var ugol = parseInt($(this).attr('data-ugol'));
		var key = $(this).attr('data-key');
		if(ugol){ugol_prot = 0;}else{ugol_prot = 1;};
		
		// Баллы проигранные на нокдаунах
		var nnkdLimit = $('#nkdLimit'+ugol).val();
		var nkdLimit = $('#nkdLimit').val();
		var nkdBall = (nkdLimit-nnkdLimit)*2;
		
		// Баллы проигранные на нарушениях
		var keyZv = parseInt($('input[name=zv'+ugol+']:checked').attr('data-ball'));
		var keyZp = parseInt($('input[name=zp'+ugol+']:checked').attr('data-ball'));
		var zBall = keyZv+keyZp;
		
		// Общие баллы
		var ball = (zBall * (-1)) + nkdBall;
		$('#ballDop'+ugol_prot).val(ball);
		
		controlEvent(ugol, key);
	});
	
	// Новый боя
	$('#butNewFight').click(function(){
		newFight();
	});
	
	// Открыть настройки
	$('#butFightSettings').click(function(){
		$('#fightSettings').toggle();
	})
	
	// Опрежделить победителя
	$('#butWinner').click(function(){
		fightWinner();
	})
	

///////////////////////////////////////////////
////////// Горячие клавиши ////////////////////
///////////////////////////////////////////////
	
$('body').keydown(function(e) {
	// Основной таймер
	switch (e.which) {
		case 17:
			controlTimer();
			return false;
		break;
		case 16:
			newFight();
			return false;
		break;
		case 82:// R - Красный удержания
			controlUder(1);
			return false;
		break;
		case 79:// O - Синий удержание
			controlUder(0);
			return false;
		break;
		case 81:// Q - Красный активность
			controlEvent(1, 'A');
			return false;
		break;
		case 87:// W - Красный 1 балл
			controlEvent(1, '1');
			return false;
		break;
		case 69:// E - Красный 2 балла
			controlEvent(1, '2');
			return false;
		break;
		case 221:// } - Синий активность
			controlEvent(0, 'A');
			return false;
		break;
		case 219:// { - Синий 1 балл
			controlEvent(0, '1');
			return false;
		break;
		case 80:// P - Синий 2 балла
			controlEvent(0, '2');
			return false;
		break;
		
		case 65:// A - Красный нокдаун
			controlEvent(1, 'nkd');
			return false;
		break;
		case 83:// S - Красный Технический нокаут
			controlEvent(1, 'tn');
			return false;
		break;
		case 68:// D - Красный нокаут
			controlEvent(1, 'nk');
			return false;
		break;
		case 70:// F - Красный удушающий
			controlEvent(1, 'ud');
			return false;
		break;
		case 71:// G - Красный болевой
			controlEvent(1, 'bp');
			return false;
		break;
		
		case 222:// " - Синий нокдаун
			controlEvent(0, 'nkd');
			return false;
		break;
		case 186:// : - Синий Технический нокаут
			controlEvent(0, 'tn');
			return false;
		break;
		case 76:// L - Синий нокаут
			controlEvent(0, 'nk');
			return false;
		break;
		case 75:// K - Синий удушающий
			controlEvent(0, 'ud');
			return false;
		break;
		case 74:// J - Синий болевой
			controlEvent(0, 'bp');
			return false;
		break;
		case 90:// Z - Красный замечание прочее
			controlEvent(1, 'zp');
			return false;
		break;
		case 88:// X - Красный замечание выход
			controlEvent(1, 'zv');
			return false;
		break;
		case 191:// / - Синий замечание прочее
			controlEvent(0, 'zp');
			return false;
		break;
		case 190:// . - Синий замечание выход
			controlEvent(0, 'zv');
			return false;
		break;
		
	};

});	

///////////////////////////////////////////////
////////// Функции ////////////////////////////
///////////////////////////////////////////////
	
	// Получение таймера в формате времени
	function getTimer(time, minutes){
		var min = time / 60 | 0;
		var sec = time % 60;
		if(min<=9){min = '0' + min;};
		if(sec<=9){sec = '0' + sec;};
		if(minutes){
			return min + ':' + sec;
		}else{
			return ':' + sec;
		};
	};

	// Ведение лога
	function log(text,error,ugol){
		// Ошибки
		if(error){
			$('#logError').text(text).show().fadeOut(5000);

		// Ход боя
		}else{
			// Определение кем было дейстиве выполнено
			var timerSec = parseInt($('#timerSec').val());
			var timerText = getTimer(timerSec,1);
			
			if(ugol==0){
				var log_div = "<div style='color: #0000cc;display: table;'><div style='display: table-cell; width: 100px;'>" + timerText + "</div><div style='display: table-cell;'>" + text+"</div></div>";
			}else if(ugol==1){
				var log_div = "<div style='color: #cc0000;display: table;'><div style='display: table-cell; width: 100px;'>" + timerText + "</div><div style='display: table-cell;'>" + text+"</div></div>";
			}else{
				var log_div = "<div style='display: table;'><div style='display: table-cell; width: 100px;'>" + timerText + "</div><div style='display: table-cell;'>" + text+"</div></div>";
			};
			$('#log').show().append(log_div);
		};	
	}
	
	// Создание нового боя, обнуление
	function newFight(){
		var sec = parseInt($('#timerSec').val());
		var winner = parseInt($('#winner').val());
		if(sec==0 || winner==1 || winner==0){
			
			var timeFight = $('#setTimeFight').val();
			var razLimit = $('#setRazLimit').val();
			var nkdLimit = $('#setNkdLimit').val();
			var uderLimit = $('#setUderLimit').val();
		
			// Установить время боя
			$('#timerSec').val(timeFight);
			// Установить время на основное табло
			$('#timerTime').val(getTimer(timeFight,1));
			
			// Установить количество нокаутов
			$('#nkdLimit1, #nkdLimit0, #nkdLimit').val(nkdLimit)
			// Установить лимит удержаний (по 10 сек)
			$('#uderLimit1, #uderLimit0').val(uderLimit)
			// Установить разницу в баллах для чистой победы
			
			// Обнуление
			$('#prot1, #prot0').val('');
			$('#act1, #act0').val(0);
			$('#ball1, #ball0, #ballDop1,#ballDop0').val(0);
			$('#winner').val('');
			$('#log').hide().text('');
			$('td.win1, td.win0').text('');
			$('#bv0, #bv1, #bz0, #bz1').prop("checked", true);
			
			// Разрешение на таймер
			//$("#butNewFight").prop('disabled',true);
			//$("#timerStartStop").prop('disabled',false);
		}else{
			log('Нельзя начать новый бой пока неопределён победитель поединка',1);
		};
		
	}
	
	// Определение победителя
	function fightWinner(ugol){
		// Разрешение на новый бой
		//$("#butNewFight").removeProp('disabled');
		//$("#timerStartStop").prop('disabled','disabled');
		
		var winner = parseInt($('#winner').val());
		
		if(ugol==1 || ugol==0){
			log('Чистая победа',0,ugol);
			winner = ugol; 
		}else{
			var ballRed = parseInt($('#ball1').val()) + parseInt($('#ballDop1').val());
			var ballBlue = parseInt($('#ball0').val()) + parseInt($('#ballDop0').val());
			var actRed = parseInt($('#act1').val());
			var actBlue = parseInt($('#act0').val());
			var protBlue = $('#prot1').val().split('+');
			var protRed = $('#prot0').val().split('+');
			
			if(ballRed > ballBlue){
				winner = 1;
			}else if(ballBlue > ballRed){
				winner = 0;
			}else if((actRed > actBlue)){
				winner = 1;
			}else if(actBlue > actRed){
				winner = 0;
			}else if(protBlue[1]){
				winner = 1;
			}else if(protRed[1]){
				winner = 0;
			};
		};
		
		alert(winner);
		
		// Останоить бой
		mainTimer(0);
		// Записать победителя
		$('#winner').val(winner);
		// Показать победителя
		$('td.win'+winner).text('Победитель!');
		
	}
	
	// начисление баллов
	function countBall(ugol,ballsNew,dop,prot,protokol){	
		if(dop){
			var ballObj = $('#ballDop'+ugol);
			var balls = parseInt(ballObj.val());
		}else{
			var ballObj = $('#ball'+ugol);
			var balls = parseInt(ballObj.val());
		};
		

		/* var ballProtObj = $('#ball'+ugol_prot);
		var ballsProt = parseInt(ballProtObj.val());
		
		var ballDopProtObj = $('#ballDop'+ugol_prot);
		var ballsDopProt = parseInt(ballDopProtObj.val()); */
		
		ballObj.val(balls + ballsNew);
	}
	
	// начисление активности
	function countAct(ugol,balls,protokol){
		var actObj = $('#act'+ugol);
		var acts = parseInt(actObj.val());
		
		actObj.val(acts+balls);
	}
	
	// Запись в протокол
	function countProp(){
		var protObj = $('#prot'+ugol);
		var prots = protObj.val();
	}
	
	// Контроль событий
	function controlEvent(ugol, key){
		if(ugol){ugol_prot = 0;}else{ugol_prot = 1;};
		
		var nkdLimitProtObj = $('#nkdLimit'+ugol_prot);
		var nkdLimitProt = parseInt(nkdLimitProtObj.val());
		
		var winner = parseInt($('#winner').val());
	/* 	if(parseInt($('#uderSec0').val())>0 || parseInt($('#uderSec1').val())>0){
			log('Баллы во время удержания не считаются',1);
			var ballFlag = false;
		};
		 */

		
		if(winner==1 || winner==0){
			log('Баллы после окончания боя не засчитываются',1);
		}else{
			switch(key){
				case 'A':
					countAct(ugol,1);
					log('Активность',0,ugol);
				break;
				case '1':
					countBall(ugol,1,0);
					log('1 балл',0,ugol);
				break;
				case '2':
					countBall(ugol,2,0);
					log('2 балла',0,ugol);
				break;
				case 'nkd':
					countBall(ugol,2,1);
					if(nkdLimitProt==1){
						// Технический нокаут
						log('Отправил соперника в техничекий нокаут',0,ugol);
						fightWinner(ugol);
					}else{
						nkdLimitProtObj.val(nkdLimitProt-1);
						log('Отправил соперника в нокдаун',0,ugol);
					};	
				break;
				case 'tn':
					// Технический нокаут
					log('Отправил соперника в техничекий нокаут',0,ugol);
					fightWinner(ugol);
				break;
				case 'nk':
					// Нокаут
					log('Отправил соперника в нокаут',0,ugol);
					fightWinner(ugol);
				break;
				case 'bp':
					// Болевой
					log('Провёл болевой прием',0,ugol);
					fightWinner(ugol);
				break;
				case 'ud':
					// Удушающий
					log('Провёл удушающий приём',0,ugol);
					fightWinner(ugol);
				break;
				case 'bv':
					// Замечание выход
					log('Обнуление замечаний за выход',1);
				break;
				case 'zv':
					// Замечание выход
					log('Замечание за выход с площадки',0,ugol);
				break;
				case 'pv1':
					// Замечание выход
					log('Первое предупреждение за выход с площадки',0,ugol);
				break;
				case 'pv2':
					// Замечание выход
					log('Второе предупреждение за выход с площадки',0,ugol);
				break;
				case 'pv3':
					// Замечание выход
					log('Дисквалификация за выход с площадки',0,ugol);
					fightWinner(ugol_prot);
				break;
				case 'bz':
					// Замечание выход
					log('Обнуление замечаний за нарушения',1);
				break;
				case 'zp':
					// Замечание выход
					log('Замечание за нарушение правил',0,ugol);
				break;
				case 'pr1':
					// Замечание выход
					log('Первое предупреждение за нарушение правил',0,ugol);
				break;
				case 'pr2':
					// Замечание выход
					log('Второе предупреждение за нарушение правил',0,ugol);
				break;
				case 'pr3':
					// Замечание выход
					log('Дисквалификация за нарушение правил',0,ugol);
					fightWinner(ugol_prot);
				break;
			};
		};
	};
	
	// Контроль удержаний
	function controlUder(ugol){

		var timer = parseInt(timerFlag.val());
		var sec = parseInt($('#uderSec'+ugol).val());
		var limit = parseInt($('#uderLimit'+ugol).val());
		
		// Проверяем запущен ли у соперника счётчик удержаний.
		// Если удержание запущенно то мы его останавливаем
		
		if(ugol){
			var sec_sop = parseInt($('#uderSec0').val());
			if(sec_sop){uderTimer(0, 0);};
		}else{
			var sec_sop = parseInt($('#uderSec1').val());
			if(sec_sop){uderTimer(0, 1);};
		};
		
		// Запустить удержание
		if(timer && !sec && limit){
			$('td.uder').addClass('timeStart');
			uderTimer(1, ugol);
			log('Считать удержание',0,ugol);
		}else if(!limit){
			log('Удержаний больше нет',1);
		}else if(!timer){
			log('Удержания не считаются во время остновки боя',1);
		}else if(sec){
			uderTimer(0, ugol);
			log('Несчитать удержание',0,ugol);
		};
		
	};
	
	// Контроллер основного таймера
	function controlTimer(){
		if(parseInt(timerFlag.val())){
			timerFlag.val(0);
			mainTimer(0);
		}else{
			timerFlag.val(1);
			mainTimer(1);
		};
		
	};
	
	
	
///////////////////////////////////////////////
///////////////////////////////////////////////	
///////////////////////////////////////////////
///////////////////////////////////////////////
	

	
	function uderTimer(stat, ugol){
		var limit = parseInt($('#uderLimit'+ugol).val());
		
		// Стартуем
		if(stat){
			timerUder = setInterval(function(){
				var sec = parseInt($('#uderSec'+ugol).val());
				var sec_new = sec + 1;
				
				if((sec_new>10 && limit==1) || (sec_new>20 && limit==2) ){
					uderTimer(0, ugol);
				}else{
					$('#uderSec'+ugol).val(sec_new);
					$('#uderTimer'+ugol).val(getTimer(sec_new, 0));
				};
				
				// звук секунд запустить
				document.getElementById('sec').play();
			
			},1000);
			
		// Останавливаем	
		}else{
			var sec = parseInt($('#uderSec'+ugol).val());
			var limitObj = $('#uderLimit'+ugol);
			var limit = parseInt(limitObj.val());
			
			$('#uderSec'+ugol).val(0);
			$('#uderTimer'+ugol).val(getTimer(0, 0));
			clearInterval(timerUder);
			
			if(sec>0 && sec<10){
				countAct(ugol,1);
			}else if(sec>=10 && sec<20){
				limitObj.val(limit-1);
				countBall(ugol,1);
			}else if(sec>=20){
				limitObj.val(limit-2);
				countBall(ugol,2);
			};
			
			// звук секунд остановить и занулить
			$('td.uder').removeClass('timeStart');
			
			document.getElementById('sec').pause();
			//document.getElementById('sec').currentTime = 0
		};
	};
	

	// Основной таймер
	function mainTimer(stat){
		// СТАРТ
		if(stat){
			var timerLong = parseInt($('#setTimeFight').val());

			
			if(parseInt($('#timerSec').val())){
				// Подсветка
				$('#timer').addClass('timeStart');
				
				timerGo = setInterval(function(){					
					var timeSec = parseInt($('#timerSec').val());
					
					if(timeSec==timerLong){
						document.getElementById('gong').play();
						log('Бой начался',0);
					};
					var timerSecNew = timeSec - 1;
					var timerText = getTimer(timerSecNew,1);

					
					
					$('#timerSec').val(timerSecNew);
					$('#timerTime').val(timerText);
					//$('#timer_tablo', tablo.document).text(timerText);
					
					// Время вышло
					if(!timerSecNew){
						document.getElementById('gong').play();
						mainTimer(0);
						log('Время поединка истекло',0);
					};
					
					// Меняем флаг таймера
					timerFlag.val(1);
					
				},1000);
			};
			
		// СТОП
		}else{
			// Подсветка
			$('#timer').removeClass('timeStart');
			//остановка таймер
			clearInterval(timerGo);
			// Меняем флаг таймера
			timerFlag.val(0);$('#timer').removeClass('timeStart');
			// Останавливаем удержания
			uderTimer(0,0);
			uderTimer(0,1);
			
		};
	};
	
	
	
/*


	
	var tablo = window.open('tablo.php', 'Wnd', 'location=no, left=0, top=0, fullscreen=yes, scrollbars=no');
	var uderTimerGo;
	
	$('#timer span').html(getTimer(timeFight, 1));
	
	
	ajaxSetGet();
	
	$('#newFightGo').click(function(){
		
		var nameRed = $('#nameRed').val().trim().toUpperCase();
		var nameBlue = $('#nameBlue').val().trim().toUpperCase();
		var longFight = parseInt($('#longFight').val());
		var win = $('#fight td.win').text().trim();
		
		if(!nameRed && !nameBlue){
			alert('Фамилии участников не заполнены!');
		}else if(!longFight){
			alert('Не указано время поединка!');
		}else if(win==''){
			alert('Текущий бой не окончен');
		}else{
		

			// Новые данные на основное табло
			$('#timerLong').val(longFight);
			$('#timerSec').val(longFight);
			$('#timer span').html(getTimer(longFight, 1));
			
			$('#fight td.name.red').text(nameRed);
			$('#fight td.name.blue').text(nameBlue);
			
			$('#fight td.ball input, #fight td.act input').val(0);
			$('#fight td.prot input').val('');
			$('#fight td.win, #nameRed, #nameBlue').text('');
			
			$('#fight input.uderTimeSec').val(0);
			$('#fight input.uderLimit').val(2);
			
			$('#fight button').prop('disabled',false).css('color','#555555');
			$('button.pr1,button.pr2,button.pr3,button.tn, button.uderStop').prop('disabled', true).css('color','#cccccc');
			
			// Новые данные на дополнительное табло
			// Занулить баллы
			$("div.ball", tablo.document).text('0');
			
			// Обновить имя
			$('#tabloBlue div.name', tablo.document).text(nameBlue);
			$('#tabloRed div.name', tablo.document).text(nameRed);
			
			// Занулить активности
			$('div.act', tablo.document).text('');
			
			
			$('td.uder,td.nkd,td.zamOut,td.zamOther,', tablo.document).text('');
			
			//Занулить добавление
			$('#nameRed').val('');
			$('#nameBlue').val('');
			listFightUp(1);

		};
		
		return false;
	});
	
	function listFightUp(n){
		
		var n = n || 1;
		
		var obj = $('div.fightList').eq(n);
		var red = obj.find('input.red').val().trim();
		var blue = obj.find('input.blue').val().trim();
		
		if(red!='' && blue!='' && n>0){
			var n_new = n-1;
			var obj_new = $('div.fightList').eq(n_new);
			obj_new.find('input.red').val(red);
			obj_new.find('input.blue').val(blue);
			obj.find('input.red').val('');
			obj.find('input.blue').val('');
			listFightUp(n+1);
		};
	};
	
	function ajaxSetGet(data){
		$.ajax({
				type: "POST",
				url: "/ajax_tablo.php",
				data: data,
				beforeSend: function(){
					$('#listFight').html("<center><img src='/images/loading.gif'></center>");		
				},
				success: function(response){
					$('#listFight').html(response);
				}
			});
	};
	

	

	// Определение победитля
	function fightWinner(u){
		u = u || 0;
		var uu = 0;
		if(!u){
			var ballRed = parseInt($('td.ball.red input').val());
			var ballBlue = parseInt($('td.ball.blue input').val());
			var actRed = parseInt($('td.act.red input').val());
			var actBlue = parseInt($('td.act.blue input').val());
			var protBlue = $('td.prot.blue input').val().split('+');
			var protRed = $('td.prot.red input').val().split('+');
			
			if(ballRed > ballBlue){
				u = "red";
			}else if(ballBlue > ballRed){
				u = "blue"; 
			}else if((actRed > actBlue)){
				u = "red"; 
			}else if(actBlue > actRed){
				u = "blue"; 
			}else if(protBlue[1]){
				u = "red";
			}else if(protRed[1]){
				u = "blue";
			};
	
			if(timerFlag==0 && $('#fight td.name.red').text().trim()!='' && $('#fight td.name.blue').text().trim()!=''){
				data = {
					red			: $('#fight td.name.red').text(),
					blue		: $('#fight td.name.blue').text(),
					red_prot	: $('td.prot.red input').val(),
					blue_prot	: $('td.prot.blue input').val(),
					red_balls	: $('#fight td.ball.red input').val(),
					blue_balls	: $('#fight td.ball.blue input').val(),
					red_act		: $('#fight td.act.red input').val(),
					blue_act	: $('#fight td.act.blue input').val(),
					winner		: $('#winner').val(),
				};

				ajaxSetGet(data);
				
				timerFlag = 1;
			};
	
	
		};
		
		
		if(u=='red'){
			$('#winner').val(1);
		}else{
			$('#winner').val(0);
		};
		
		alert('Победил ' + u);
		$('td.win.'+u).text('ПОБЕДА!');
	};
	
	
	$('button.pr1,button.pr2,button.pr3,button.tn, button.uderStop').prop('disabled', true).css('color','#cccccc');
	
	// Обработка действий, оценка
	$('#fight button.event').click(function(e){
		var text = $(this).text();
		var event = 0;
		
		if($(this).parent('td').hasClass('red')){
			var u = 'red';
			var u2 = 'blue';
		}else{
			var u = 'blue';
			var u2 = 'red';
		};
		
		if($(this).hasClass('act')){
			act(u);
			event = 'act';
		}else if($(this).hasClass('ball')){
			ball(u,text);
			event = 'ball';
		}else if($(this).hasClass('nkd')){
			$(this).prop('disabled',true).css('color','#cccccc');
			$(this).parents('td').find('button.tn').prop('disabled',false).css('color','#555555');
			ball(u2, 2);
			prot(u2, '(2)', event);
			event = 'nkd';
			$('td.nkd.'+u, tablo.document).text(text);
		}else if($(this).hasClass('tn')){
			$(this).prop('disabled', true).css('color','#cccccc');
			event = 'tn';
			prot(u2, '(X)', event);
			fightWinner(u2);
		}else if($(this).hasClass('zam')){
			$(this).prop('disabled', true).css('color','#cccccc');
			$(this).parents('td').find('button.pr1').prop('disabled',false).css('color','#555555');
			event = 'zam';
			
			if($(this).hasClass('out')){
				$('td.zamOut.'+u, tablo.document).text(text);
			}else if($(this).hasClass('other')){
				$('td.zamOther.'+u, tablo.document).text(text);
			};
			
		}else if($(this).hasClass('pr1')){
			$(this).prop('disabled', true).css('color','#cccccc');
			$(this).parents('td').find('button.pr2').prop('disabled',false).css('color','#555555');
			ball(u2, 1);
			event = 'pr1';
			prot(u2, '(1)', event);
			
			if($(this).hasClass('out')){
				$('td.zamOut.'+u, tablo.document).text(text);
			}else if($(this).hasClass('other')){
				$('td.zamOther.'+u, tablo.document).text(text);
			};
			
		}else if($(this).hasClass('pr2')){
			$(this).prop('disabled', true).css('color','#cccccc');
			$(this).parents('td').find('button.pr3').prop('disabled',false).css('color','#555555');
			ball(u2, 2);
			event = 'pr2';
			prot(u2, '(2)', event);
			
			if($(this).hasClass('out')){
				$('td.zamOut.'+u, tablo.document).text(text);
			}else if($(this).hasClass('other')){
				$('td.zamOther.'+u, tablo.document).text(text);
			};
			
		}else if($(this).hasClass('pr3')){
			$(this).prop('disabled', true).css('color','#cccccc');
			event = 'pr3';
			prot(u2, '(X)', event);
			fightWinner(u2);
		}else if($(this).hasClass('chp')){
			$(this).prop('disabled', true).css('color','#cccccc');
			event = 'chp';
			prot(u2, '(X)', event);
			fightWinner(u2);
		};
		
		prot(u, text, event);
		
	});
	
	// Считать удержание
	$('#fight button.uderPlay').click(function(e){
		var event = 'uder';
		var uderTimerObj = $(this).parent('div').find('button.uderTimer span');
		
		var uderTimeSecObj = $(this).parent('div').find('input.uderTimeSec');
		
		var uderLimitObj = $(this).parent('div').find('input.uderLimit');
		var uderLimit = parseInt(uderLimitObj.val());
		
		if($(this).parents('td').hasClass('red')){
			var u = 'red';
		}else{
			var u = 'blue';
		};
		
		$(this).prop('disabled', true).css('color','#cccccc');
		$(this).parent('div').find('button.uderStop').prop('disabled', false).css('color','#555555');
		
		uderTimerGo = setInterval(function(){
		
			var sec = parseInt(uderTimeSecObj.val());
			var secNew = sec + 1;
				
			if((sec>=10 && uderLimit==1) || (sec>=20 && uderLimit==2) ){
				timerStopUder(uderTimerGo, uderTimeSecObj);
			}else if(uderLimit){
				uderTimeSecObj.val(secNew);
				uderTimerObj.text(getTimer(secNew,0));
			};
		},1000);

		document.getElementById('sec').play();
	});
	
	// Остановаить удержание
	$('#fight button.uderStop').click(function(e){
		$(this).prop('disabled', true).css('color','#cccccc');
		$(this).parent('div').find('button.uderPlay').prop('disabled', false).css('color','#555555');
		timerStopUder(uderTimerGo, $(this));
	});
	
	// Подсчёт удержания по окончанию действия
	function timerStopUder(Timer, obj){

		timerStop(Timer);
		var uderTimeSecObj = obj.parent('div').find('input.uderTimeSec');
		var uderTimeSec = parseInt(uderTimeSecObj.val());
		
		var uderLimitObj = obj.parent('div').find('input.uderLimit');
		var uderLimit =  uderLimitObj.val();
	
		if(obj.parents('td').hasClass('red')){
			var u = 'red';
		}else{
			var u = 'blue';
		};
		
		if(uderTimeSec<10){
			//alert('Начало удержания');
			act(u);
			prot(u, '(А)', 'uder');
		}else if(uderTimeSec>=10 && uderTimeSec<20){
			//alert('Неполное удержание');
			ball(u, 1);
			prot(u, '(1)', 'uder');
			uderLimitObj.val(uderLimit-1);
			$('td.uder.'+u, tablo.document).text('Уд');
		}else if(uderTimeSec>=20){
			//alert('Полное удержание');
			ball(u, 2);
			prot(u, '(2)', 'uder');
			uderLimitObj.val(uderLimit-2);
			$('td.uder.'+u, tablo.document).text('Уд').css('color','#666666');
		};
		
		uderTimeSecObj.val(0);
		obj.parent('div').find('button.uderTimer span').text(':00');
		
		// Удержаний больше нет, если выполнены два не полных
		if(uderLimitObj.val()==0){
			obj.parent('div').find('input, button').prop('disabled', true).css('color','#cccccc');
			$('td.uder.'+u, tablo.document).text('Уд').css('color','#666666');
		};
		
		document.getElementById('sec').pause();
		document.getElementById('sec').currentTime = 0
	};
	
	// Начисление балла
	function ball(u, ball){
		var ball = parseInt(ball);
		protballs = $('td.ball.'+u+' input');
		balls = parseInt(protballs.val()) + ball;
		protballs.val(balls);
		
		var balsRed = parseInt($('td.ball.red input').val());
		var balsBlue = parseInt($('td.ball.blue input').val());
		
		// Табло
		$('div.ball.'+u, tablo.document).text(balls);
		
		
		// Если разница в баллах 7 и более то определяем победителя
		var reit = balsBlue-balsRed;
		
		if(reit >= 10){
			fightWinner('blue');
		}else if(reit <= -10){
			fightWinner('red');
		};
	};
	
	// Начисление активности
	function act(u){
		protact = $('td.act.'+u+' input');
		acts = parseInt($('td.act.'+u+' input').val()) + 1;
		protact.val(acts);
		
		$('div.act.'+u, tablo.document).append("<img src='images/act.png'>&nbsp;");
	}
	
	// Запись в протокол
	function prot(u, text, event){
		var prot = $('td.prot.'+u+' input').val().trim();
		if( $('td.prot.red input').val().trim()=='' && $('td.prot.blue  input').val().trim()=='' && event!='zam' && event!='pr1' && event!='pr2' && event!='nkd' && event!='chp'){text = '+' + text;};
		$('td.prot.'+u+' input').val(prot + '+' + text);
	}
	
	
	
	

	$('td.act input, td.prot input, td.ball input').change(function(){
		var text;
		var u;
		var obj_td = $(this).parent('td');
		
		if(obj_td.hasClass('red')){
			u = 'red';
		}else{
			u = 'blue';
		};

		if(obj_td.hasClass('ball')){
			text = parseInt($(this).val().trim());
			$('div.ball.'+u, tablo.document).text(text);
		}else if(obj_td.hasClass('act')){
			text = parseInt($(this).val().trim());
			
			$('div.act.'+u, tablo.document).text('');
			for(var i = 0; i < text; i++){
				$('div.act.'+u, tablo.document).append("<img src='images/act.png'>&nbsp;");
			};
		
			
			
		}else if(obj_td.hasClass('prot')){
			text = $(this).val().trim();
		};
		
	
	});

	
	
	
	
	
	
	
	
	 */
	
	
});