
	function dialog_add_city(title_city_val, id_add_token){
		var div_dialog = $("#dialog-add-city");
		if(title_city_val){div_dialog.find('input[name=title_city]').val(title_city_val.trim());};
		div_dialog.dialog({
			resizable: false,
			height:200,
			width: 460,
			modal: true,
			buttons: {
				"Добавить": function() {
					
					var title_city = div_dialog.find('input[name=title_city]').val().trim();
					var chisl = div_dialog.find('input[name=chisl]').val().trim();
					var id_obl = div_dialog.find('select[data-linklist=obl]').val();
					var er = false;
										
					if(title_city==='' || !parseInt(id_obl)){er=true;};

					if(!er){
						$.ajax({
							type: "POST",
							url: "ajax/ajax_send_form.php",
							data: { action: 'add_city', title_city:title_city, id_obl:id_obl, chisl:chisl},
							cache: false,
							success: function(response){
								
								var obj = JSON.parse(response);
								if(id_add_token && response){
									$('#'+id_add_token).tokenInput("add",{"id":obj.id,"name":obj.name,"obl":obj.obl});
								};
								
							}
						});
						$( this ).dialog( "close" );
					}else{
						alert("Ошибка ввода");
					};

				}
			}
		});
	};
	
	
	function dialog_add_zal(id_zal, id_add_token){
	
		// Диалог
		var div_dialog = $("#dialog-edit-zal");
		var table_zals = $("#zals");
		var er=false;
		
		// Если редактировать, то получаем значение по ID о зале и вставляем в форму
		if(id_zal){
			$.ajax({
				type: "POST",
				url: "ajax/ajax_autocomplete.php",
				data: { action: 'show_zal', id_zal:id_zal},
				cache: true,
				success: function(data){ 
					var obj = JSON.parse(data);
					div_dialog.find('input[name=title]').val(obj.title);
					div_dialog.find('input[name=kr_title]').val(obj.kr_title);
					div_dialog.find('input[name=adress]').val(obj.adress);
					div_dialog.find('input[name=telephone]').val(obj.telephone);
					// Запускаем формирование связанного списка
					linklist(div_dialog.find('select[data-linklist=fedok]'), [obj.id_fedok, obj.id_obl, obj.id_city]);
					}
				});
		}else{
			// Если Добавляем новый зал, все поля пустые (обнуляем)
			var id_zal = 0;
			div_dialog.find('input').val('');
			div_dialog.find('select').val(0).change();
		};
		
		// Теперь вызываем диалог в котором уже все данные есть
		div_dialog.dialog({
			resizable: false,
			height:280,
			width: 460,
			modal: true,
			buttons: {
				"Сохранить": function() {
				
					// Получаем данные из формы
					var title = div_dialog.find('input[name=title]').val().trim();
					var kr_title = div_dialog.find('input[name=kr_title]').val().trim();
					var id_city = div_dialog.find('select[data-linklist=city]').val();
					var adress = div_dialog.find('input[name=adress]').val().trim();
					var telephone = div_dialog.find('input[name=telephone]').val().trim();

					if(title==='' || kr_title==='' || id_city==='0' || adress==='0'){
						er=true;
					};
					
					if(!er){
					// СОХРАНЯЕМ, Отправляем данные серверу
						$.ajax({
							type: "POST",
							url: "ajax/ajax_send_form.php",
							data: { action: 'save_zal', id_zal:id_zal, title:title, kr_title:kr_title, id_city:id_city, adress:adress, telephone:telephone},
							cache: false,
							success: function(response){
								var obj = JSON.parse(response);
								if(id_add_token && response){
									alert(id_add_token+obj.id+obj.name+obj.adress);
									$('#'+id_add_token).tokenInput("add",{"id":obj.id,"name":obj.name,"adress":obj.adress});
								}else{
									if(id_zal){
										var cur_tr = table_zals.find('tr[id_zal='+id_zal+']');
										cur_tr.find('td.zal_title').html(title);
										cur_tr.find('td.zal_kr_title').html(kr_title);
										cur_tr.find('td.zal_title_city').html(div_dialog.find('select[data-linklist=city] :selected').text());
										cur_tr.find('td.zal_adress').html(adress);
										cur_tr.find('td.zal_telephone').html(telephone);
										$('#update').html("Сохранено!").show();	
									}else{
										var new_tr = "<tr id_zal='"+obj.id+"'><td  class='zal_title'>"+title+"</td><td class='zal_kr_title'>"+kr_title+"</td><td class='zal_title_city'>"+div_dialog.find('select[data-linklist=city] :selected').text()+"</td><td  class='zal_adress'>"+adress+"</td><td align='center'  class='zal_telephone'>"+telephone+"</td><td><div class='buttonset'><button class='but_edit' name='edit_zal'>Редактировать</button><button class='but_del' name='del_zal'>Удалить</button></div></td>			</tr>";
										table_zals.prepend(new_tr);
										$( ".but_del" ).button({ icons: {primary:'ui-icon-trash'},text: false});
										$( ".but_edit" ).button({ icons: {primary:'ui-icon-wrench'},text: false});
										$('#update').html("Зал успешно добавлен").show();	
									};
								};
								
							}
						});
						$( this ).dialog( "close" );
					}else{
						alert("Ошибка ввода");
					};

					
				}
			}
		});
	};
	
	
	
	