<?php
class Model_photo extends Model{
	public function get_data($param){		
		if(!$param['id']){
			if($_ENV['skin']){
				$skin_q = ("SELECT * FROM skins WHERE id_skin = '$_ENV[skin]'");
				$skin_r = mysql_query($skin_q) or die("Ошибка запроса Скина"); 
				$skin = mysql_fetch_array($skin_r);
				$usl = "AND photo_gal.skin=".$_ENV['skin'];
			};
		}else{
			$usl = "AND photo_alb.id_photo_gal = ".$param['id'];
		};
		

		
		
		// Все альбомы принадлежат к организации (ее id в photo_gal). Все номера обрамляются буквами "p". Есть альбомы с id_photo_gal = 'all'
		$albs_q=("
			SELECT
				photo_alb.title, photo_img.name, photo_alb.id_photo_alb, photo_img.id_photo_img, photo_alb.author
			FROM
				photo_gal, photo_alb LEFT JOIN photo_img ON photo_img.id_photo_img = photo_alb.thumb
			WHERE
				photo_alb.dostup = 1
				AND photo_alb.id_photo_gal = photo_gal.id_photo_gal
				$usl
			ORDER BY date_add
			");
		$albs_r = mysql_query($albs_q) or die("Ошибка запроса альбомов"); 
		

		
		// Вот здесь проверка на добавление альбома. Нужно добавить проверку для каждой гелереи
		if($_ENV['user_admin']){
			$div_edit = $this->tpl->generate('photo_gal_editor', $data);
		};

		

		while($albs = mysql_fetch_array($albs_r)){	
			// Проверка для редактирования альбома
			if($_ENV['user_admin'] || $_ENV['id_user']==$albs['author']){
				$but_editor_alb = "
					<button data-alb-id='".$albs['id_photo_alb']."' class='but_del alb_del'>{LT_ALB_DEL}</button>
					<button data-alb-id='".$albs['id_photo_alb']."' class='but_edit_small alb_edit'>{LT_ALB_EDIT}</button>
				";
			}else{
				$but_editor_alb = "";
			};
		
			$data = array(
				'{ID_ALB}' => $albs['id_photo_alb'],
				'{TITLE_ALB}' => $albs[0],
				'{THUMB_ALB}' => $albs['id_photo_img']."_".$albs['name'],
				'{BUT_EDITOR_ALB}' => $but_editor_alb
				);
			$ALBS.= $this->tpl->generate('photo_view_small_alb', $data);
		};
		


				$data = array(
				'{DIV_EDIT_GAL}' => $div_edit,
				'{ALBS}' => $ALBS
				
				);
		

		return $data;
	}
	
	
	
	
	function get_alb($param){
	
		$alb_q=("SELECT dostup FROM photo_alb WHERE id_photo_alb = '$param[id]'");
		$alb_r = mysql_query($alb_q) or die("Ошибка запроса фотографий"); 
		$alb = mysql_fetch_array($alb_r);
		
		if($alb['dostup']){
	
			$photo_q=("
				SELECT
					*
				FROM
					photo_img
				WHERE
					photo_img.id_photo_alb = '$param[id]'
					AND photo_img.dostup = 1
				");
			$photo_r = mysql_query($photo_q) or die("Ошибка запроса фотографий"); 
			while($photo = mysql_fetch_array($photo_r)){				
				$photo_name = $photo['id_photo_img'].'_'.$photo['name'];
				$photo_path_view = "/files/photo/view/".$photo_name;
				$photo_path_thumb = "/files/photo/thumb/".$photo_name;
				$PHOTOS.="<a class='fancybox' rel='group' href='".$photo_path_view."'><img class ='photo' src='".$photo_path_thumb."' height='100' alt='' /></a>";
			};
			
			if(!$PHOTOS){
				$PHOTOS = reform_error('{LT_PHOTOS_NO}');
			};
		}else{
			$PHOTOS = reform_error('{LT_ALB_NO}');
		};
		
		
		
		$data = array(
			'{PHOTOS}' => $PHOTOS			
		);
		
		return $data;

	}
	

	
	
/* 		function get_create($param){
	
		$PHOTOS = 2;
	
		$data = array(
			'{PHOTOS}' => $PHOTOS			
				
		);
		
		return $data;

	}
 */}
?>
