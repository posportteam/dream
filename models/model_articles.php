<?php
class Model_articles extends Model{
	public function get_article($param=null){	
		$data = $this->sql_query->getArticle(array('id_art' => $param['id_art']));
		return $data;
	}

	public function get_articles($param=null){	
		$data_filter = array(
		
		);
	
		$data = array(
			'{ARTICLES}'	=> $this->sql_query->getArticles(array('id_user' => $_ENV['id_user'])),
			'{FILTER}'		=> $this->tpl->generate('filter_articles', $data_filter),
		);
		return $data;
	}
	
	public function article_edit($param=null){	
		$data = array(
			'{ARTICLES}'	=> $this->sql_query->articlesEdit(array('id_art' => $param['id_art']))
		);
		return $data;
	}
	
	public function article_add($param=null){	
		$data = array(
			'{ARTICLES}'	=> $this->sql_query->articlesEdit()
		);
		return $data;
	}
}
?>
