<?php
class Model_sors extends Model{
	
	public function get_sors($param){	

		// Определение актуальных дат месяц вперед и месяц назад с текущего дня
		$month_cur = date('m');
		$year_cur = date('Y');
		
		if($param['date']){
			$date_start = $param['date'];
		}else{
			
			$month_last = $month_cur - 1;
			$month_next = $month_cur + 1;	

			if($month_last=='0'){
				$month_last = 12;
				$year_last = $year_cur - 1;
			}else{
				$year_last = $year_cur;
			};
			
			if($month_next=='13'){
				$month_next = 1;
				$year_next = $year_cur + 1;
			}else{
				$year_next = $year_cur;
			};
			
		
			$date_start = $year_last.'-'.$month_last.'-01';
			$date_finish = $year_next.'-'.$month_next.'-31';
		};
		$data_filter = array(
			'{SELECT_SPORTS}' 	=> $this->select_linklist->generate_sport(array('sport_type' => $_ENV['site_sport_type'], 'sport' => $_ENV['site_sport'])),
			'{SELECT_REGION}' 	=> $this->select_linklist->generate_region(array('fedok' => $_ENV['site_fedok'], 'obl' => $_ENV['site_obl'], 'city' => $_ENV['site_city'])),
		);
		
		$data_sor = array(
			'fedok' 		=> $_ENV['site_fedok'], 
			'obl' 			=> $_ENV['site_obl'], 
			'city' 			=> $_ENV['site_city'],
			'sport_type' 	=> $_ENV['site_sport_type'], 
			'sport' 		=> $_ENV['site_sport'],
			'status'		=> 0,
			'date_start'	=> $date_start,
			'date_finish'	=> $date_finish
		);
		
		$sors = $this->sql_query->getSors($data_sor);
		
		foreach($sors AS $key => $sor){
			// Зануление переменных
			$title_sor_dists = $button_poloj = $button_edit = $button_reg = $button_req = $button_online = $button_result = $status = $zal = $button_photo = $button_video = "";
			
			// Сравниваем текущую дату и дату соревнований
			$time_sor_finish = $sor['date_finish'].' 22:59:59';
			$cur_sor = srav_date($sor['date_start'], $time_sor_finish);
			
			// Просмотр соревнований
			//$button_view="<button href='/sor".$sors[id_sor]."' class='inner'>{SOR_BUT_VIEW}</button>";
			
			// Сегодня день регистрации
			if($sors['date_reg']==date("Y-m-d")){
				$status = "{LT_SOR_STATUS_REG}";
				$class_cur_sor = 'sor_reg';
				$reg = true;
			// Кнопка начала регистрации доступна только секретарю организации или администратору или главному судье
			//	if(check_prava($_ENV['id_user'],'sec',$sors['id_org']) || $_ENV['user_admin'] || $sors['id_user_glavsud']==$_ENV['id_user']){
			//		$button_online="<button href='/sor/reg?id=".$sors['id_sor']."'  class='inner'>{LT_SOR_REG}</button>";
			//	};
			
			//Соревнования идут
			}elseif($cur_sor==1){
				$reg = true;
				$class_cur_sor = 'sor_cur';
				$status = "{LT_SOR_STATUS_NOW}";
			//	$button_online="<button href='/sor/online' class='inner'>{LT_SOR_ONLINE}</button>";
				
			// Соревнования отменены
			}elseif($sors['date_old'] && !$sors['date_start']){
				$status = "{LT_SOR_STATUS_CONCEL}";
			// Соревнования Были перенесены
			}elseif($sors['date_old']){
				$status = "{LT_SOR_STATUS_TRANS}";
			// Соревнования предстоят
			}elseif($cur_sor==2){
				$class_cur_sor = 'sor_next';
				$reg = true;
				//if($_ENV['user_trener']){
					//$button_req="<button href='/sor/req'  class='inner'>{LT_SOR_REQ}</button>";
				//};
				$status = "{LT_SOR_STATUS_FUT}";
			// Соревнования уже состоялись
			}else{
				$class_cur_sor = 'sor_prev';
				$status = "{LT_SOR_STATUS_LAST}";
			//	$button_result = "<button href='/sor/result'  class='inner'>{LT_SOR_RESULT}</button>";
				
				// Если есть в галлерее, то выводим ссылку на видео и(или) фото
				// $button_video = "<button href='/video/sor?id=$sors[id_sor]'  class='inner'>{LT_VIDEO}</button>";
			//	$button_photo = "<button href='/photo/sor?id=".$sors['id_sor']."'  class='inner'>{LT_PHOTO}</button>";
			};
			
			// Кнопка положения, если оно конечно выгружено на сервер
			//$poloj ='./files/docs/poloj/'.$sors['id_sor'].'.rar';
			//if(file_exists($poloj)){
			//	$button_poloj="<button href='$poloj' target='_blank' class='inner'>{LT_SOR_POLOJ}</button>";
			//};		
			
			// Кнопка редактировать доступна только организатору или его директору или секретарю
			//if($sors['id_user_author']==$_ENV['id_user'] || check_prava($_ENV['id_user'],'sec',$sors['id_org']) || check_prava($_ENV['id_user'],'dir',$sors['id_org']) || $_ENV['user_admin']  || $sors['id_user_glavsud']==$_ENV['id_user']){
			//	$button_edit="<button href='/sor/edit?id='".$sors['id_sor']." class='inner'>{LT_EDIT}</button>";
			//};
			
			$dist_q=("
				SELECT COUNT(*)
				FROM sor_dist
				WHERE sor_dist.id_sor = '$sors[id_sor]'
			");
			$dist_r = mysql_query($dist_q) or die("Ошибка запроса дисциплин"); 
			$dist_num = mysql_fetch_row($dist_r);
			if($dist_num[0]){
				$title_sor_dists = '/ {LT_SOR_DIST}: '.$dist_num[0];
			};

		
			// Проверка зала
			if($sor['zal'][0]['id_zal']){
				$zal = ", ".$sor['zal'][0]['title_kr']." (".$sor['zal'][0]['adress'].")"; 
			};
			
			if(($_ENV['id_user']==$sor['id_user_glavsud'] || $_ENV['id_user']==$sor['id_user_glavsec'] || $_ENV['admin']) && $reg){
				$button_reg = "<a href='/sors/reg?id_sor=".$sor['id_sor']."'><button class=''>{LT_REG}</button></a>";
			};
			
			
			$data = array(
				'{SOR_CLASS_STATUS}' 	=> $class_cur_sor,
				'{SOR_ID}' 				=> $sor['id_sor'],
				'{SOR_TITLE}'			=> $sor['title'],
				'{SOR_DATE}' 			=> $sor['date'],
				'{SOR_SPORT}' 			=> $sor['sport'][0]['title'],
				'{SOR_SPORT_ID}' 		=> $sor['sport'][0]['id_sport'],
				'{SOR_SPORT_TYPE_ID}'	=> $sor['sport'][0]['id_sport_type'],
				'{SOR_CITY}' 			=> $sor['city'][0]['title'],
				'{SOR_ZAL}' 			=> $zal,
				'{SOR_BUT_VIEW}'		=> $button_v,
				'{SOR_BUT_POLOJ}' 		=> $button_poloj,
				'{SOR_BUT_EDIT}' 		=> $button_edit,
				'{SOR_BUT_REG}' 		=> $button_reg,
				'{SOR_BUT_REQ}' 		=> $button_req,
				'{SOR_BUT_ONLINE}'		=> $button_online,
				'{SOR_BUT_RESULT}'		=> $button_result,
				'{SOR_STATUS}'			=> $status,
				'{SOR_BUT_PHOTO}'		=> $button_photo,
				'{SOR_BUT_VIDEO}'		=> $button_video,
				'{SOR_BUT_USERS}'		=> "<a href='/sor/users?id_sor=".$sor['id_sor']."'><button>{LT_SOR_USERS}</button></a>",
				'{SOR_DISTS}'			=> $title_sor_dists
			);
		
			$SORS.= $this->tpl->generate('sor_preview', $data);
		};	
		
		if(!$SORS){$SORS= $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "", "{NO_RESULT_TEXT}" => "{LT_SOR_NO}"));};
		
		$data = array(
				'{SORS}' 			=> $SORS,
				'{FILTER}'			=> $this->tpl->generate('filter_sors', $data_filter),
				'data-type-search'	=> 'getSorByTitle'
				);
		
		return $data;
	}
	
	public function get_reg($param){	
		// Запрос соревнования
		
		$sors = $this->sql_query->getSors(array("id_sor" => $param['id_sor']));
		$sor=$sors[0];
		
		if(!$param['id_sor'] || !$sor['id_sor']){
			$REGS = reform_error("{LT_NO_ID}");
		// Регистрация не доступна
		}elseif($sor['date_reg']!=date("Y-m-d") && !srav_date($sor['date_start'], $sor['date_finish'])){
			$REGS = reform_error("{LT_SOR_REG_CLOSED_DATE}");
		}elseif($_ENV['id_user']!=$sor['id_user_glavsud'] && $_ENV['id_user']!=$sor['id_user_glavsec'] && !$_ENV['admin']){
			$REGS = reform_error("{LT_SOR_REG_CLOSED_ACCESS}");
		}else{

	
			$fedoks = $this->sql_query->getFedok();
			foreach($fedoks AS $key => $fedok){
				$FEDOK.="<option value='".$fedok['id_fedok']."'>".$fedok['title']."</option>";
			};
			
			$REG_TEAMS = showSorTeams($sor['teams']);
			
			
			$rangs_sp = $this->sql_query->getRangs(array('id_rang_type' => '1'));
			foreach($rangs_sp AS $key => $rang){
				$RANGS_SPORT.= "<option value='".$rang['id_rang']."'>".$rang['title']."</option>";
			};
			
			// Запрашиваем пользователя-участника соревнований
			foreach($sor['users'] AS $key => $user){
				//$age_option='';
				//$dist_option='';	
				// Пробегаемся по категориям соревнований
			//	foreach($sor['cats'] AS $key_cat => $cat){
					
					// Если категория подходит по половому признаку (или общие соревнования при SEX=2)
					//if($cat['sex']==$user['info'][0]['sex'] || $user['info'][0]['sex']==2){
						// Данная каттегория возраста принадлежит пользователю
						//if($cat['id_sor_age']==$user['id_sor_age']){
						//	$sel_age='selected';
						//	$my_age = true;
						//}else{
						//	$sel_age='';
						//	$my_age = false;
						//};
						//if(!$cat['age_to']){$cat['age_to']="->";};
						// Составляем выбор категорий
						//$age_option.="<option value='".$cat['id_sor_age']."' $sel_age>".$cat['age_from']."-".$cat['age_to']."</option>";
						// Пробег по дисциплинам в каттегории
						//foreach($cat['dist'] AS $key_dist => $dist){
							// Если каттегория возраста принадлежит пользователю
						//	if($my_age){
						//		if($user['id_sport_dist']==$dist['id_sport_dist']){
						//			$sel_dist='selected';
						//		}else{
						//			$sel_dist='';
						//		};
						//		$dist_option.="<option value='".$dist['id_sor_dist']."' $sel_dist>".$dist['title']."</option>";
						//	};
					//	};
				//	}
			//	};
				$user_data = array(
					'{ID_SOR}'			=> $sor['id_sor'],
					'{ID_USER}'			=> $user['info'][0]['id_user'],
					'{NAME}'			=> $user['info'][0]['name'],
					'{SECOND_NAME}'		=> $user['info'][0]['second_name'],
					'{CITY}'			=> $user['info'][0]['city_title'],
					'{ID_CITY}'			=> $user['info'][0]['id_city'],
					'{BORN}'			=> convert_date($user['info'][0]['born']),
					'{WEIGHT}'			=> $user['info'][0]['sport_data']['weight'],
					'{DIST_SELECT}'		=> $dist_option,
					'{AGE_SELECT}'		=> $age_option,
					'{AGE}'				=> $user['id_sor_age'],
					'{DIST}'			=> $user['id_sport_dist'],
					'{SEX}'				=> $user['info'][0]['sex'],
					"{RANGS_SPORT}"		=> $RANGS_SPORT
				);
				$REG_USERS.= $this->tpl->generate("sor_reg_user_".$sor['sport'][0]['id_sport'], $user_data);
			};
		};
		
		
		
		
		$reg_data = array(
			"{ID_SOR}"			=> $sor['id_sor'], 
			"{ID_SPORT}"		=> $sor['sport'][0]['id_sport'],
			"{LT_FEDOK_SELECT}"	=> $FEDOK,
			"{COUNT_REG_TEAMS}"	=> count($sor['teams']),
			"{COUNT_REG_USERS}"	=> count($sor['users']),
			"{REG_TEAMS}" 		=> $REG_TEAMS,
			"{REG_USERS}" 		=> $REG_USERS,
			"{REG_USER_ADD}"	=> $this->tpl->generate("sor_reg_user_".$sor['sport'][0]['id_sport'].'_add', array('{ID_SOR}'	=> $sor['id_sor'], "{ID_SPORT}"		=> $sor['sport'][0]['id_sport'], "{RANGS_SPORT}" => $RANGS_SPORT)),
			
		);
			

		$data = array(
			'{SORS}' 			=> $this->tpl->generate('sor_reg', $reg_data),
			'{FILTER}'			=> '',
			'data-type-search'	=> ''
		);
		return $data;
	}
		
	public function get_view($param){	
		// Запрос соревнования
		
		if($param['id_sor']){
			$sor = $this->sql_query->getSors(array("id_sor" => $param['id_sor']));
			
		
		}else{
		
		};

		$data = array(
			"{SOR_TITLE}"	=> $sor['title']
		);
		return $data;
	}
	
	public function get_edit($param){	
		// Запрос соревнования
		
		if($param['id']){
			$sor = $this->sql_query->getSors(array("id_sor" => $param['id_sor']));
			
		}else{
		
		};

		$data = array(
			"{SOR_TITLE}"	=> $sor['title']
		);
		return $data;
	}
	
	public function get_add($param){	
		
		// Запрос соревнования
		
		
		if($param['id']){
		
		}else{
		
		}

		$data = array(
		
		);
		return $data;
	}
	
	
	public function get_tablo($param){	
		
		if($param['id_sport']){
			$id_sport = $param['id_sport'];
			$response = sor_go($id_sport);
		}elseif($param['id_sor']){
			$sor = $this->sql_query->getSors(array("id_sor" => $param['id_sor']));
			$id_sport = $sor[0]['sport'][0]['id_sport'];
			$response = sor_go($id_sport,$param['id_sor']);
		}else{
			$response = "Не указан вид спорта";
		};
		
		

		$data = array(
			"{SORS}"	=> $response
		);
		return $data;
	}
}
?>
