<?php
class Model_groups extends Model{
	
	
	public function get_groups($param=null){
		$groups = $this->sql_query->getGroups($param);	
		
		
		foreach($groups AS $key => $group){
			$trener = '<div><label>{LT_TRENER}: </label>'.$group['trener_name'].' '.$group['trener_second_name'].' '.$group['trener_third_name'].'</div>';
			$zal = $group['zal_title'].', '.$group['zal_adress_street'].', '.$group['zal_adress_house'];
			
			if($group['group_age_to']){
				$age= $group['group_age_from'].'-'.$group['group_age_to'].' {LT_AGE_YEARS}';
			}else{
				$age= $group['group_age_from'].' {LT_AGE_MORE}';
			};
	
			$data_group = array(
				'{TRENER}'			=> $trener,
				'{ID_TRENER}'		=> $group['id_trener'],
				'{CLUB}' 			=> $group['club_title'],
				'{ID_GROUP}'		=> $group['id_group'],
				'{ZAL}'				=> $zal,
				'{TYPE}'			=> $group['group_type'],
				'{AGE}'				=> $age,
				'{TITLE}'			=> $group['group_title'],
			);
			
			$GROUPS.= $this->tpl->generate('group_preview', $data_group);
		};	
			
	
		if(!$GROUPS){$GROUPS= $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "", "{NO_RESULT_TEXT}" => "{LT_GROUP_NO}"));};
		
		$data = array(
			'{GROUPS}'		=> $GROUPS,
			'{GROUPS_TITLE}' 	=> '{LT_GROUPS}',
			'{FILTER}' 		=> $this->tpl->generate('filter_groups', array('{ID_GROUP}' => $param['id_group']))
		);
 
		return $data;
	}
	
	public function get_groupsStat($param=null){
		$data = array(
			'{GROUPS}' 			=>  $this->sql_query->getGroupsStat($param),
			'{GROUPS_TITLE}' 	=> '{LT_GROUPS_STAT}',
			'{FILTER}' 			=>  $this->tpl->generate('filter_groups')
		);
		return $data;
	}
	
	public function get_money($param=null){
		
		if($_ENV['id_user']){
			$month_array = $this->selecter->get_month_array(1);
			
			if($_GET['y']>0){
				$YEAR = (int)$_GET['y'];
			}else{
				$YEAR = date('Y');
			};
			
			//�������� ���������
			$TH.= "<th class='name'>{LT_FIO}</th>"; // ���
				
			foreach($month_array AS $key => $month_title){
				$TH.= "<th width='30'>".$month_title."</th>";
			};
			
			$users = $this->sql_query->getUsers(array('id_trener' => $_ENV['id_user']));
			$money = $this->sql_query->getMoney();
			
			foreach($money AS $key => $money_user){
				$money[$money_user['id_user']][$money_user['id_group']][$money_user['oplata_year']][$money_user['oplata_month']] = array('sum' => $money_user['oplata_summa'], 'oper' => $money_user['id_oplata_oper'], 'type_title' => $money_user['oplata_type_title']);
			};

			
			
			foreach($users AS $key => $user){
			
				foreach($user['careers'] AS $key => $group){
					if($group['id_group'] == $user['id_group']){
						$data_money_rek = $user['group_money']*(100 - $group['discount'])/100;
					};
				};
			
			
			
				$TR.= "<tr class='user' data-id-group='".$user['id_group']."' data-money-rek='".$data_money_rek."'>";
				$TR.="<td><a href='/id".$user['id_user']."'><b>".mb_strtoupper($user['name'], 'utf-8')."</b> ".$user['second_name']."</td>";
				for($m=1; $m<=12; $m++){
					
					
					
					
					
					$money_val = $money[$user['id_user']][$user['id_group']][$YEAR][$m]['sum'];
					$oper_id = $money[$user['id_user']][$user['id_group']][$YEAR][$m]['oper'];
					$oplata_type_title = $money[$user['id_user']][$user['id_group']][$YEAR][$m]['type_title'];
					$money_month[$m]=$money_month[$m]+$money_val;
					
					
					if($money_val){$dop_class='sum';}else{$dop_class='';};
					if($oper_id=='2'){$dis = " disabled='true' "; $dop_class='beznal';}else{$dis = '';};
					$TR.="<td class='money'><input type='text' class='money $dop_class' title='".$oplata_type_title."' ".$dis." value='".$money_val."' data-month='".$m."' data-id-user='".$user['id_user']."'></td>";
				};
				$TR.= "</tr>";
			};
			
			// �����
			$TR.= "<tr class='itogo'><td>{LT_ITOGO_MONEY}</td>";
			for($m=1; $m<=12; $m++){
				$TR.= "<td data-month-itog='".$m."' align='center'>".$money_month[$m]."</td>";
			};
			$TR.= "</tr>";
			
			$data_money = array(
				'{YEAR}'			=> $YEAR,
				'{SELECT_YEAR}'		=> $this->selecter->gen_select_year($param['y']),
				'{TH}'				=> $TH,
				'{TR}'				=> $TR,
			);
				
			$MONEY.= $this->tpl->generate('group_money', $data_money);
		};		
	
		if(!$MONEY){$MONEY= $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "", "{NO_RESULT_TEXT}" => "{LT_GROUP_NO}"));};
		
		
		$data = array(
			'{GROUPS}' 			=>  $MONEY,
			'{GROUPS_TITLE}' 	=> '{LT_MONEY}',
			'{FILTER}' 			=>  $this->tpl->generate('filter_groups')
		);
		
		return $data;
	}
	
	public function get_settings($param=null){
		
		if($param['id_group']){
			//  ������ �����
			$groups = $this->sql_query->getGroups(array('id_group' => $param['id_group']));
			$group = $groups[0];

			// ����������� ���������� � �������
			$group_long = "<select name='' id=''>";
			for($i = 30; $i<=150; $i+=30){
				if($i==$group['group_time']){$sel = "selected";}else{$sel='';};
				$group_long.= "<option value='".$i."' ".$sel.">".$i." {LT_MINUT}</option>";
			};
			$group_long.= "</select>";
			
			// ������ � ������
			if($group['group_arhiv']){$sel_1 = "selected";}else{$sel_0 = "selected";};
			$group_status = "<select name='' id=''>";
			$group_status.= "<option value='1' ".$sel_1.">{LT_GROUP_CLOSE}</option>";
			$group_status.= "<option value='0' ".$sel_0.">{LT_GROUP_OPEN}</option>";
			$group_status.= "</select>";
		
			// �����������
			$users_groups_q = ("
				SELECT COUNT(*) AS num_users
				FROM user_careers
				WHERE user_careers.id_group = ".$param['id_group']."
			");
			$users_groups_r = mysql_query($users_groups_q); 
			$users_group = mysql_fetch_array($users_groups_r);
		
			// ����������
			if($group['group_pn']){$rasp_arr = explode(':',$group['group_pn']); $rasp[1][0]=$rasp_arr[0]; $rasp[1][1]=$rasp_arr[1]; };
			if($group['group_vt']){$rasp_arr = explode(':',$group['group_vt']); $rasp[2][0]=$rasp_arr[0]; $rasp[2][1]=$rasp_arr[1]; };
			if($group['group_sr']){$rasp_arr = explode(':',$group['group_sr']); $rasp[3][0]=$rasp_arr[0]; $rasp[3][1]=$rasp_arr[1]; };
			if($group['group_ch']){$rasp_arr = explode(':',$group['group_ch']); $rasp[4][0]=$rasp_arr[0]; $rasp[4][1]=$rasp_arr[1]; };
			if($group['group_pt']){$rasp_arr = explode(':',$group['group_pt']); $rasp[5][0]=$rasp_arr[0]; $rasp[5][1]=$rasp_arr[1]; };
			if($group['group_sb']){$rasp_arr = explode(':',$group['group_sb']); $rasp[6][0]=$rasp_arr[0]; $rasp[6][1]=$rasp_arr[1]; };
			if($group['group_vs']){$rasp_arr = explode(':',$group['group_vs']); $rasp[7][0]=$rasp_arr[0]; $rasp[7][1]=$rasp_arr[1]; };

			for($i=1; $i<=7; $i++){
				$sel[$i].= "<select class='raspisanie' name='' id=''>";
				$sel[$i].= "<option value='0'>-</option>";
				for($s=7; $s<=22; $s++){
					if($rasp[$i][0]==$s && $rasp[$i][1]=='00'){$sel1='selected';}else{$sel1='';};
					if($rasp[$i][0]==$s && $rasp[$i][1]=='30'){$sel2='selected';}else{$sel2='';};
					$s1 = $s.":00";
					$s2 = $s.":30";
					$sel[$i].= "<option value='".$s1."' $sel1>".$s1."</option>";
					$sel[$i].= "<option value='".$s2."' $sel2>".$s2."</option>";
				};
				$sel[$i].= "</select>";
			};
			
			$raspisanie = "<tr>
				<td>".$sel[1]."</td>
				<td>".$sel[2]."</td>
				<td>".$sel[3]."</td>
				<td>".$sel[4]."</td>
				<td>".$sel[5]."</td>
				<td>".$sel[6]."</td>
				<td>".$sel[7]."</td>
			</tr>";
		
			$data_group = array(
				'{AGE_FROM}'			=> $group['group_age_from'],
				'{AGE_TO}'				=> $group['group_age_to'],
				'{NUM_USERS}'			=> $users_group['num_users'],
				'{RASPISANIE}'			=> $raspisanie,
				'{LONG}'				=> $group_long,
				'{CLUB}'				=> 5,
				'{SECTION}'				=> 6,
				'{STATUS}'				=> $group_status,
				'{TRENER}'				=> 6,
				'{GROUP_TITLE}'			=> $group['group_title'],
			);
		
		
			$SETTINGS = $this->tpl->generate('group_settings', $data_group);
			if(!$SETTINGS){$SETTINGS= $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "", "{NO_RESULT_TEXT}" => "{LT_GROUP_NO}"));};
			

			$data = array(
				'{GROUPS}' =>  $SETTINGS,
				'{GROUPS_TITLE}' => $group['group_title'],
				'{FILTER}' =>  $this->tpl->generate('filter_groups', array('{ID_GROUP}' => $param['id_group']))
			);
		}else{
			$data = array(
				'{GROUPS}' => $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "", "{NO_RESULT_TEXT}" => "{LT_GROUP_NO}"))
			);
		};
		return $data;
	}
	
	public function get_attendance($param=null){
		if($param['id_group']){
		
			// ������ �� ������� ������ � ����
			if($_GET['m']){
				$MONTH = (int)$_GET['m'];
			}else{
				$MONTH = date('n');
			};
			
			if($_GET['y']>0){
				$YEAR = (int)$_GET['y'];
			}else{
				$YEAR = date('Y');
			};
			
			$groups = $this->sql_query->getGroups(array('id_group' => $param['id_group']));
			$group = $groups[0];
			
			if($group['id_trener'] == $_ENV['id_user']){
			
				// ���������� ������ ���� ������ ��� ����������
				$day_of_week_for_training = array();
				if($group['group_pn']!='00:00:00'){$day_of_week_for_training[]='1';};
				if($group['group_vt']!='00:00:00'){$day_of_week_for_training[]='2';};
				if($group['group_sr']!='00:00:00'){$day_of_week_for_training[]='3';};
				if($group['group_ch']!='00:00:00'){$day_of_week_for_training[]='4';};
				if($group['group_pt']!='00:00:00'){$day_of_week_for_training[]='5';};
				if($group['group_sb']!='00:00:00'){$day_of_week_for_training[]='6';};
				if($group['group_vs']!='00:00:00'){$day_of_week_for_training[]='0';};
				
				
				// �������� ������ �� ������������� ���� � ������ ������
				$array_days_for_training = search_day_of_week_for_raspisanie($day_of_week_for_training, $YEAR, $MONTH);
				$num_tren = count($array_days_for_training[$YEAR][$MONTH]);
				
				//�������� ���������
				$th_width = floor(360/$num_tren);// ������ ������ ����
				$TH.= "<th class='name'>{LT_FIO}</th>"; // ���
				
				// ���� ����������
				foreach($array_days_for_training[$YEAR][$MONTH] as $key => $val){
					$TH.="<th width='".$th_width."'>".$val.".".$MONTH."</th>";
				};
				

				// �������� ����������� ������ ������
				$data_users = $this->sql_query->getUsers(array('id_group' => $param['id_group']));
				// �������� ������������
				$array_atten = $this->sql_query->getAttendance(array('id_group' => $param['id_group'], 'month' => $MONTH, 'year' => $YEAR));
				// ��� ������� ���������� ������ ������ ��� ������� � ���������� � ��������� ��
				foreach($data_users AS $key => $users_group){
					$USERS_ATTENDANCE.= "<tr>";
					$USERS_ATTENDANCE.="<td><a href='/id".$users_group['id_user']."'><b>".mb_strtoupper($users_group['name'], 'utf-8')."</b> ".$users_group['second_name']."</td>";
					
					foreach($array_days_for_training[$YEAR][$MONTH] as $key => $val){
						if($array_atten[$users_group['id_user']][$val]){
							$atten_status = "checked";
						}else{
							$atten_status = '';
						};
						
						// ��������� ������ �� ������� ���������
						$USERS_ATTENDANCE.="<td class='atten_status ".$atten_status."' data-day='".$val."' data-id-user='".$users_group['id_user']."'><input disabled='true' type='checkbox' name='' ".$atten_status."></td>";
					};
					$USERS_ATTENDANCE.="</tr>";
				};
				
				
				$data_attendance = array(
						'{SELECT_MONTH}'		=> $this->selecter->gen_select_month($param['m']),
						'{SELECT_YEAR}'			=> $this->selecter->gen_select_year($param['y']),
						'{TH_TRAINING}'			=> $TH,
						'{TR_USER}' 			=> $USERS_ATTENDANCE,
						'{ID_GROUP}'			=> $param['id_group'],
						'{MONTH}'				=> $MONTH,
						'{YEAR}'				=> $YEAR,
					);
					
				$data = array(
					'{GROUPS}'		 => $this->tpl->generate('group_attendance', $data_attendance),
					'{GROUPS_TITLE}' => $group['group_title'],
					'{FILTER}'		 => $this->tpl->generate('filter_groups', array('{ID_GROUP}' => $param['id_group']))
				);
				
			};
		};
		
		// ���� �� ������� ������ ��� � �� ������, �� ���������� ������ ��� ������ ���������� �� �������
		if(!$data){
			$data = array(
				'{GROUPS}' => $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "", "{NO_RESULT_TEXT}" => "{LT_GROUP_NO}")),
				'{GROUPS_TITLE}' => '{LT_ERROR}',
			);
		};
		
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
}
?>
