<?php
class Model_users extends Model{


	public function get_user($param){	

		if($param['id_user']){
			$id_user = $param['id_user'];
		}elseif($_ENV['id_user']){
			$id_user = $_ENV['id_user'];
		};

		if($id_user){
		$user = $this->sql_query->getUsers(array("id_user" => $id_user));
		};
		
		
		if($user[0]){
			$user = $user[0];
			
			// Собираем данные о карьере
			if($user['careers_sport'] || $user['careers_org']){
				foreach($user['careers_sport'] as $key => $val){
					if($val['date_out']){$date_out = $val['date_out'];}else{$date_out ='{LT_NAST_VR}';};
					if($val['sport']){$sport = $val['sport'].', ';};
					if($val['trener']){$trener = $val['trener'];}else{$trener = "{LT_NO_TRENER}";};
					$careers_sport.='<p><label>с '.$val['date_in'].' по '.$date_out.'</label>'.$val['club'].' ('.$sport.$trener.')</p>';
					if(!$val['date_out']){
						$userTreners[] = $val['id_trener'];
					};
				};
				
				if($careers_sport){
					$div_careers_sport = "<h3>{LT_USER_BLOCK_CAREERS_SPORT}</h3>".$careers_sport;
				};
			}
			
				foreach($user['careers_org'] as $key => $val){
					if($val['date_out']){$date_out = $val['date_out'];}else{$date_out ='{LT_NAST_VR}';};
					$careers_org.='<p><label>с '.$val['date_in'].' по '.$date_out.'</label>'.$val['title'].' <a href="/orgs/id_club='.$val['id_club'].'">'.$val['club'].'</a></p>';
				};
				
				if($careers_org){
					$div_careers_org = "<h3>{LT_USER_BLOCK_CAREERS_ORG}</h3>".$careers_org;
				};
			
			
			// Кнопки контроля над пользователем
			if($_ENV['id_user']){
				
				if($id_user==$_ENV['id_user']){
					$buttons.="<button class='but_user_control but_my' id='avatar_change_but'>{LT_EDIT_AVATAR}</button>	";
				}else{
					$buttons.="
						<button class='but_user_control but_sms'>{LT_SEND_MESS}</button>
						<button class='but_user_control but_flag' id='podpiska_blog' id_user_to='".$id_user."'>{LT_FAV}</button>
					";
				};

				if($_ENV['admin'] || in_array($_ENV['id_user'], $userTreners)){
					$buttons.= "<button class='but_user_control but_stat'>{LT_STAT}</button>";
				};	
				
				if($_ENV['admin'] || in_array($_ENV['id_user'], $userTreners) || $id_user==$_ENV['id_user']){
					$buttons.= "<a href = '/users/edit?id_user=".$id_user."'><button class='but_user_control but_edit'>{LT_EDIT_PROFILE}</button></a>";
				};	
				
				if($_ENV['admin']){
					$buttons.= "<button class='but_user_control but_print'>{LT_EDIT_MONEY}</button>";
				};	
			};
			// информация доступная тренеру, администратору, рководителю, секретарю организации
			if($_ENV['admin'] || in_array($_ENV['id_user'], $userTreners) || $_ENV['id_user']==$user['id_user']){			
				if($user['adress_street'] && $user['adress_house']){$adress = ", ".$user['adress_street'].", ".$user['adress_house']." - ".$user['adress_flat'];};
				if($user['phone']){$contacts.= "<p><label>{LT_PHONE}:</label>".$user['phone']."</p>";};
				if($user['polis']){$zachetka.= "<p><label>{LT_POLIS}:</label>".$user['polis']."</p>";};
				if($user['passport']){$zachetka.= "<p><label>{LT_PASSPORT}:</label>".$user['passport']."</p>";};
			};
			
			// Собираем спортивные данные
			if($user['sport_data']['weight']){$zachetka.= "<p><label>{LT_WEIGHT}:</label>".$user['sport_data']['weight']." кг.</p>";};
			if($user['sport_data']['height']){$zachetka.= "<p><label>{LT_HEIGTH}:</label>".$user['sport_data']['height']." см.</p>";};
			if($user['sport_data']['poyas']){$zachetka.= "<p><label>{LT_POYAS}:</label>".$user['sport_data']['poyas']."</p>";};
			if($user['sport_data']['dispanser']){$zachetka.= "<p><label>{LT_DISPANSER}:</label>".$user['sport_data']['dispanser']."</p>";};
			if($zachetka){
				$div_zachetka = "<h3>{LT_USER_BLOCK_SPORT}</h3>".$zachetka;
			};
			
			// Собираем контактах
			if($user['email']){$contacts.= "<p><label>{LT_EMAIL}:</label><a target='_blank' href='mailto:".$user['email']."?subject={LT_DEFAULT_TITLE_FOR_MAIL}'>".$user['email']."</a></p>";};
			if($user['www']){$contacts.= "<p><label>{LT_WWW}:</label><a target='_blank' href='http://".$user['www']."'>{LT_GO_HREF}</a></p>";};
			if($user['vk']){$contacts.= "<p><label>{LT_VK}:</label><a target='_blank' href='http://vk.com/id".$user['vk']."'>{LT_GO_HREF}</a></p>";};
			if($user['fb']){$contacts.= "<p><label>{LT_FACEBOOK}:</label><a target='_blank' href='".$user['fb']."'>{LT_GO_HREF}</a></p>";};
			if($user['icq']){$contacts.= "<p><label>{LT_ICQ}:</label>".$user['icq']."</p>";};
			if($user['skype']){$contacts.= "<p><label>{LT_SKYPE}:</label>".$user['skype']."</p>";};
			if($contacts){
				$div_contacts = "<h3>{LT_USER_BLOCK_CONTACT}</h3>".$contacts;
			};
			
			// Собираем общую информацию
			if($user['id_user']){$info.= "<p><label>{LT_ID}:</label>".number_posport($user['id_user'], $user['id_obl'])."</p>";};
			if($user['born']!='0000-00-00'){$info.= "<p><label>{LT_BORN}:</label>".convert_date($user['born'])."</p>";};
			if($user['work']){$info.= "<p><label>{LT_WORK}:</label>".$user['work']."</p>";};
			if($user['id_city']){$info.= "<p><label>{LT_ADRESS}:</label>".$user['city_title'].$adress."</p>";};		
			if($info){
				$div_info = "<h3>{LT_USER_BLOCK_INFO}</h3>".$info;
			};
			

			// Биография
			if($user['biography']){$biography = "<h4 class='h4_show'>{LT_USER_BLOCK_BOIGRAPHY}</h4><div id='user_boigraphy'>".$user['biography']."</div>";};
			
			// Собираем данные о званиях
			if($user['rangs']){
				foreach($user['rangs'] as $key => $val){
					if(($val['number_soc'] || $val['raport']) && $val['date']) {$sk = $val['date'];}else{$sk = '{LT_NO_RAPORT}';};
					$rangs.='<p><label>'.$val['sport'].'</label> '.$val['rang'].' ('.$sk.')</p>';
				};
				if($rangs){
					$div_rangs = "<h3>{LT_USER_BLOCK_SPORT_ZVANIYA}</h3>".$rangs;
				};
				
			};
			
			

			// Блог
			$blog_text = $this->sql_query->getArticles(array('id_user' => $id_user));
			if($blog_text){
				$blog = $this->tpl->generate('articles', array('{ARTICLES}' => $blog_text));
				$blog = "<h4>{LT_USER_BLOCK_BLOG}</h4>".$blog;
			};
			
			$data_user = array(
				'{USER_NAME}' 			=> $user['name'].' '.$user['second_name'].' '.$user['third_name'],
				'{USER_PHOTO_SRC}' 		=> $this->sql_query->getAvatar(array('id_user' => $id_user, 'type' => 'user')),
				'{USER_BUTTONS}' 		=> $buttons,
				'{USER_BOIGRAPHY}'		=> $biography,
				'{DIV_RANGS}'			=> $div_rangs,
				'{DIV_ZACHETKA}'		=> $div_zachetka,
				'{DIV_CONTACTS}'		=> $div_contacts,
				'{DIV_INFO}'			=> $div_info,
				'{DIV_CAREERS_SPORT}'	=> $div_careers_sport,
				'{DIV_CAREERS_ORG}'		=> $div_careers_org,
				'{USER_BLOG}'			=> $blog 
			);
			
			
			$USER = $this->tpl->generate('user_view', $data_user);
			
		}else{
			$USER = $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "", "{NO_RESULT_TEXT}" => "{LT_USER_NO}"));
		};
		
		$data = array(
			"{USERS}" => $USER
		);
		
		return $data;
	}

	public function get_user_edit($param){	
		$users = $this->sql_query->getUsers($param);// Запрос спортсмена
		$user = $users[0];
		
		$careers_sport.="<h3>{LT_ADD}</h3>";
		$careers_sport.= $this->tpl->generate('user_careers_sport_edit', array('{NEW}'		=> 'new', '{NUM_CAREERS}'=> 0));
		$careers_sport.="<h3 id='h3_career_sport'>{LT_CAREER}</h3>";
		foreach($user['careers_sport'] as $key => $career){
			if(!$career['date_out']){
				$userTreners[] = $career['id_trener'];
			};
			
			
			$data_career = array(
				'{NEW}'			=> '',
				'{CLUB}'		=> $career['id_club'],
				'{TRENER}'		=> $career['id_trener'],
				'{GROUP}'		=> $career['id_group'],
				'{DATE_IN}'		=> $career['date_in'],
				'{DATE_OUT}'	=> $career['date_out'],
				'{DISCOUNT}'	=> $career['discount'],
				'{NUM_CARRERS}'	=> $key,
				'{ID_CITY}'		=> $career['id_city'],
				'{CITY}'		=> $career['city'],
				'{SPORT}'		=> $career['sport'],
				'{ID_SPORT}'	=> $career['id_sport'],
				'{NUM_CAREERS}'=> mt_rand(1000,9999),
			);
			$careers_sport.= $this->tpl->generate('user_careers_sport_edit', $data_career);
		};
		

		
		$careers_org.="<h3>{LT_ADD}</h3>";
		$careers_org.= $this->tpl->generate('user_careers_org_edit', array('{NEW}'		=> 'new', '{SELECT_ACCESS_TYPE}' => $this->selecter->gen_select_accessType(), '{NUM_CAREERS}'=> 0));
		$careers_org.="<h3 id='h3_career_org'>{LT_CAREER}</h3>";
		foreach($user['careers_org'] as $key => $career){
			$selecter = $this->selecter->gen_select_accessType($career['access']);
			if($career['id_club']){
				$org = $career['club'];
			}elseif($career['id_org']){
				$org = $career['org'];
			};
			
			$data_career = array(
				'{NEW}'			=> '',
				'{ID_CLUB}'		=> $career['id_club'],
				'{ORG}'			=> $org,
				'{ID_ORG}'		=> $career['id_org'],
				'{DATE_IN}'		=> $career['date_in'],
				'{DATE_OUT}'	=> $career['date_out'],
				'{TITLE}'		=> $career['title'],
				'{TYPE}'		=> $career['type'],
				'{NUM_CARRERS}'	=> $key,
				'{ID_CITY}'		=> $career['id_city'],
				'{CITY}'		=> $career['city'],
				'{SELECT_ACCESS_TYPE}' =>  $selecter,
				'{NUM_CAREERS}'=> mt_rand(1000,9999),

			);
			$careers_org.= $this->tpl->generate('user_careers_org_edit', $data_career);
		};
		
		
		$rangs.="<h3>{LT_ADD}</h3>";
		$rangs.= $this->tpl->generate('user_rangs_edit', array('{NEW}'=> 'new', '{SELECTER_RANGS}'=> $this->selecter->gen_select_rangs(), '{NUM_RANGS}'=> 0));
		$rangs.="<h3 id='h3_rangs'>{LT_CAREER}</h3>";
		foreach($user['rangs'] as $key => $rang){
			
			$data_rang = array(
				'{NEW}'			=> '',
				'{DATE}'		=> $rang['date'],
				'{RAPORT}'		=> $rang['raport'],
				'{SPORT}'		=> $rang['sport'],
				'{ID_SPORT}'	=> $rang['id_sport'],
				'{SELECTER_RANGS}'=> $this->selecter->gen_select_rangs($rang['id_rang']),
				'{NUM_RANGS}'=> mt_rand(1000,9999),
			);
			$rangs.= $this->tpl->generate('user_rangs_edit', $data_rang);
		};
		
		if($user['sex']){
			$sex_m = 'selected';
		}else{
			$sex_w = 'selected';
		};

		$user_data = array (
				'{NAME}'			=> $user['name'],
				'{SECOND_NAME}'		=> $user['second_name'],
				'{THIRD_NAME}'		=> $user['third_name'],
				'{BORN}'			=> convert_date($user['born']),
				'{PHONE}'			=> $user['phone'],
				'{PHONE_ADD}'		=> $user['phone_add'],
				'{WORK}'			=> $user['work'],
				'{EMAIL}'			=> $user['email'],
				'{PASSPORT}'		=> $user['passport'],
				'{POLIS}'			=> $user['polis'],
				'{VK}'				=> $user['vk'],
				'{FB}'				=> $user['fb'],
				'{WWW}'				=> $user['www'],
				'{ICQ}'				=> $user['icq'],
				'{SKYPE}'			=> $user['skype'],
				'{WEIGHT}'			=> $user['sport_data']['weight'],
				'{HEIGHT}'			=> $user['sport_data']['height'],
				'{DISPANSER}'		=> convert_date($user['sport_data']['dispanser_date']),
				'{SPORT_INFO_OLD}'	=> $user['sport_data']['info'],
				'{POYAS}'			=> $this->selecter->get_poyas($user['sport_data']['id_poyas']),
				'{MOTHER_NAME}'		=> $user['family']['mother_name'],
				'{MOTHER_PHONE}'	=> $user['family']['mother_phone'],
				'{MOTHER_WORK}'		=> $user['family']['mother_work'],
				'{FATHER_NAME}'		=> $user['family']['father_name'],
				'{FATHER_PHONE}'	=> $user['family']['father_phone'],
				'{FATHER_WORK}'		=> $user['family']['father_work'],
				'{NUM_FAMILY}'		=> $user['family']['num_family'],
				'{STREET}'			=> $user['adress_street'],
				'{HOUSE}'			=> $user['adress_house'],
				'{BLOCK}'			=> $user['adress_block'],
				'{FLAT}'			=> $user['adress_flat'],
				'{ID_CITY}'			=> $user['id_city'],
				'{CITY}'			=> $user['city_title'],
				'{BIOGRAPHY}'		=> $user['biography'],
				'{CAREERS_SPORT}'	=> $careers_sport,
				'{CAREERS_ORG}'		=> $careers_org,
				'{RANGS}'			=> $rangs,
				'{ID_USER}'			=> $user['id_user'],
				'{SEL_SEX_M}'		=> $sex_m,
				'{SEL_SEX_W}'		=> $sex_w,

		);
		
		if(in_array($_ENV['id_user'], $userTreners) || $_ENV['admin']){
			$USERS = $this->tpl->generate('user_edit', $user_data);
		}else{
			$USERS = $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "{LT_NO_RULES_TITLE}", "{NO_RESULT_TEXT}" => "{LT_NO_RULES}"));
		};
		
		// $data_filter = array(
			
		// );
		
		$data = array(
			"{USERS}"		=> $USERS,
		//	"{FILTER}"		=> $this->tpl->generate('filter_users', $data_filter),
		);
		
		return $data;
	}

	public function get_users_my($param){	

	if($_ENV['user_trener']){

		$param['id_trener'] = $_ENV['id_user'];
		$data_users = $this->sql_query->getUsers($param);// Запрос спортсменов
		
		foreach($data_users AS $key => $user){
			if($i!=$user['id_group']){
				$USERS.="<h3>{LT_GROUP} &laquo;".$user['group_title']."&raquo;</h3>";
			};
			$i = $user['id_group'];
			
			$user = array (
				'{SELECT}'		=> $this->selecter->get_groups(array('id_trener' => $_ENV['id_user'], 'id_user' => $user['id_user'], 'id_group' => $user['id_group'], )),
				'{ID_GROUP}'	=> $user['id_group'],
				'{ID_GROUP}'	=> $user['id_group'],
				'{NAME}'		=> $user['name'],
				'{SECOND_NAME}'	=> $user['second_name'],
				'{PHONE}'		=> $user['phone'],
				'{ID_USER}'		=> $user['id_user'],
			);
			
			$USERS.= $this->tpl->generate('user_preview', $user);
			
		};
		

	}else{
		$USERS = "Error :(";
	};
	
	
		$data_filter = array(
			
		);
		
		$data = array(
			"{USERS}"		=> $USERS,
			"{FILTER}"		=> $this->tpl->generate('filter_users', $data_filter),
		);
		
		return $data;	

	
	
	}
}

?>
