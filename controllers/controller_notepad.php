<?php
class Controller_notepad{
	function __construct(){
		$this->model = new Model_notepad();	
		$this->tpl = new Tpl();	
	}

	function action(){
		$data = $this->model->get_data();
		$page = $this->tpl->generate('notepad', $data);
		return $page;
	}
}

