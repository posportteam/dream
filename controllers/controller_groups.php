<?php
class Controller_groups extends Controller{
	function __construct(){
		$this->model = new Model_groups();
		$this->view = new View_page();
	}
	
	
	function action_index($param=null){
		if($param['id_group']>0){
			$data = $this->model->get_attendance($param);
			$this->view->generate_page('groups', $data, '{LT_ATTENDANCE}');
		}else{
			$data = $this->model->get_groups($param);
			$this->view->generate_page('groups', $data, '{LT_TITLE_GROUP}');
		};
	}
	
	function action_my($param=null){
		$data = $this->model->get_groups(array('id_trener' => $_ENV['id_user']));
		$this->view->generate_page('groups', $data, '{LT_TITLE_GROUP_MY}');
	}
	
	function action_stat($param=null){
		$data = $this->model->get_groupsStat($param);
		$this->view->generate_page('groups', $data, '{LT_GROUPS_STAT}');
	}
	
	
	function action_add($param=null){
		$data = $this->model->get_add($param);
		$this->view->generate_page('groups', $data, '{LT_GROUPS_STAT}');
	}

	function action_attendance($param=null){
		$data = $this->model->get_attendance($param);
		$this->view->generate_page('groups', $data, '{LT_ATTENDANCE}');
	}
	
	function action_settings($param=null){
		$data = $this->model->get_settings($param);
		$this->view->generate_page('groups', $data);
	}
	
	function action_money($param=null){
		$data = $this->model->get_money($param);
		$this->view->generate_page('groups', $data);
	}
	
	
}
?>