<?php
class Controller_photo extends Controller{
	function __construct(){
		$this->model = new Model_photo();
		$this->view = new View_page();
	}
	
	
	function action_index($param=null){
	
		if(!$param){ 
			$param = array('id' => $_ENV['id_user'], 'site_sport_type' => $_ENV['site_sport_type'], 'site_obl' => $_ENV['site_obl'],'site_fedok' => $_ENV['site_fedok'],'site_sport' => $_ENV['site_sport'] );
		};
	

		$data = $this->model->get_data($param);
		$this->view->generate_page('photo', $data);
	}
	
	
	function action_album($param){
		$data = $this->model->get_alb($param);
		$this->view->generate_page('photo_album', $data);	
	}
	
	function action_create_album($param){
		$data = $this->model->get_create($param);
		$this->view->generate_page('photo_create_album', $data);	
	}
}
?>