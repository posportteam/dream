<?php
class Controller_sors extends Controller{
	function __construct(){
		$this->model = new Model_sors();
		$this->view = new View_page();
	}
	
	function action_index($param=null){
		$data = $this->model->get_sors($param);
		$this->view->generate_page('sors', $data, '{LT_TITLE_SOR}');
	}
	
	function action_reg($param=null){
		$data = $this->model->get_reg($param);
		$this->view->generate_page('sors', $data, '{LT_TITLE_SOR_REG}');
	}
	
	function action_view($param=null){
		$data = $this->model->get_view($param);
		$this->view->generate_page('sors', $data, '{LT_TITLE_SOR_VIEW}');
	}
	
	function action_edit($param=null){
		$data = $this->model->get_edit($param);
		$this->view->generate_page('sors', $data, '{LT_TITLE_SOR_EDIT}');
	}
	
	function action_tablo($param=null){
		$data = $this->model->get_tablo($param);
		$this->view->generate_page('sors', $data, '{LT_TABLO}');
	}

	}
?>