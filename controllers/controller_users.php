<?php
class Controller_users extends Controller{
	function __construct(){
		$this->model = new Model_users();
		$this->view = new View_page();
	}
	
	
	function action_index($param=null){
		if(!$param && $_ENV['id_user']){ 
			$param['id_user'] = $_ENV['id_user'];
		}elseif(!$param){
			$param['id_user'] = 0;
		};

		$data = $this->model->get_user($param);
		$this->view->generate_page('users', $data);
	}
	
	function action_my($param=null){
		$data = $this->model->get_users_my($param);
		$this->view->generate_page('users', $data, '{LT_USERS_FOR_TRENERS}');
	}
	
	function action_edit($param=null){
		if(!$param && $_ENV['id_user']){ 
			$param['id_user'] = $_ENV['id_user'];
		}elseif(!$param){
			$param['id_user'] = 0;
		};
		
		$data = $this->model->get_user_edit($param);
		$this->view->generate_page('users', $data, '{LT_USERS_FOR_TRENERS}');
	}
	
}
?>