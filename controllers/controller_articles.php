<?php
class Controller_articles extends Controller{
	function __construct(){
		$this->view = new View_page();
		$this->model = new Model_articles();
	}

	function action_index($param=null){
		$data = $this->model->get_articles($param);
		$this->view->generate_page('articles', $data, '{LT_TITLE_ARTICLES}');
	}
	
	function action_view($param=null){
		$data = $this->model->get_article($param);
		$this->view->generate_page('articles', $data, '{LT_TITLE_ARTICLE_VIEW}');
	}
	
	function action_edit($param=null){
		$data = $this->model->article_edit($param);
		$this->view->generate_page('articles', $data, '{LT_TITLE_ARTICLE_EDIT}');
	}
	
	function action_add($param=null){
		$data = $this->model->article_add($param);
		$this->view->generate_page('articles', $data, '{LT_TITLE_ARTICLE_ADD}');
	}

}