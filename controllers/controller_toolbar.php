<?php
class Controller_toolbar{
	function __construct(){
		$this->model = new Model_toolbar();	
		$this->tpl = new Tpl();	
	}

	function action(){
		$data = $this->model->get_data();
		$page = $this->tpl->generate('toolbar', $data);
		return $page;
	}
}

