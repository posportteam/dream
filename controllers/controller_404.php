<?php
class Controller_404 extends Controller{
	function __construct(){
		$this->view = new View_page();
	}

	
	function action_index(){
		$this->view->generate_page('error_404');
	}
}