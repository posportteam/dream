<?
class sql_query{

	function __construct(){
		$this->tpl = new Tpl();
		$this->select_linklist = new select_linklist();	
	}
	
	public function getSors($param){
		// фильтр-условие по дате
		if($param['id_sor']){
			$usl_id = " AND sors.id_sor = '".$param['id_sor']."'";
			$limit = " LIMIT 1 ";
		}else{
			$date_start = $param['date_start'];
			$date_finish = $param['date_finish'];
			
			if($date_start && !$date_finish){
				$usl_date = "AND ( ('$date_start' BETWEEN DATE_FORMAT(date_start, '%Y-%m-%d') AND date_finish) ||  '$date_start' = DATE_FORMAT(date_start, '%Y-%m-%d'))";
			}elseif($date_start && $date_finish){
				$usl_date = "AND ( (DATE_FORMAT(date_start, '%Y-%m-%d') BETWEEN '$date_start' AND '$date_finish') || (date_finish BETWEEN '$date_start' AND '$date_finish') )";
			};
			
			// фильтр-условие по спорту
			if($param['sport']>0){
				$usl_sport = " AND ( sports.id_sport = '$param[sport]')";
			}elseif($param['sport_type']>0){
				$usl_sport = " AND ( sports.id_sport_type = '$param[sport_type]')";
			}
			
			// Фильтр-условие по региону
			if($param['city']>0){
				$usl_region = " AND ( sors.id_city = '$param[city]')";
			}elseif($param['obl']>0){
				$usl_region = " AND ( geo_obl.id_obl = '$param[obl]')";
			}elseif($param['fedok']>0){
				$usl_region = " AND ( geo_obl.id_fedok = '$param[fedok]')";
			};
			
			
			// Фильтр по статусу на сегодняшний момент
			// 0 - отменены
			// 1 - состоялся
			// 2 - перенесён
			// 3 - регистрация
			// 4 - проходит
			// 5 - планируется

			switch ($param['status']){
				case 0:
				break;
				case 1:
					$usl_status = " AND (CURDATE() > sors.date_finish)";
				break;
				case 2:
					//$usl_status = " AND (sors.date_start > CURRENT_TIMESTAMP())";
				break;
				case 3:
					$usl_status = " AND (CURRENT_TIMESTAMP() BETWEEN sors.date_reg AND sors.date_start)";
				break;
				case 4:
					$usl_status = " AND (CURRENT_TIMESTAMP() >= sors.date_start AND CURDATE() <= sors.date_finish)";
				break;
				case 5:
					$usl_status = " AND (CURRENT_TIMESTAMP() < sors.date_start)";
				break;
			};

			if($param['status']==1){
				$order_by = "ORDER BY date_start DESC";
			}else{
				$order_by = "ORDER BY date_start ASC";
			};
			
			// фильтр по названию
			if($param['title']){
				$title = explode(' ',$param['title']);
				foreach($title as $key => $val){
					$usl_title.=" AND sors.title LIKE '%$val%' ";
				};
			};
		};
		
		// Большой запрос на выборку соревнований в соответсвии с фильтром в $Param
		$sor_q=("
			SELECT sors.id_sor, sors.title, geo_city.title, sports.title, sors.*, DATE_FORMAT(date_start,'%d.%m.%Y') AS date, sports.id_sport, sports.id_sport_type
			FROM  geo_city, geo_obl, sports, sors
			WHERE 
				geo_city.id_city	= sors.id_city
				AND geo_obl.id_obl = geo_city.id_obl
				AND sports.id_sport	= sors.id_sport
				$usl_id
				$usl_date
				$usl_sport
				$usl_region
				$usl_status
				$usl_title
			$order_by
			$limit
		");
		
		$sor_r = mysql_query($sor_q) or die("Ошибка запроса соревнований!"); 
		while($sors = mysql_fetch_array($sor_r)){
			$data_sors[] = array(
				"title"				=> $sors[1],
				"id_sor"			=> $sors[0],
				"city"				=> sql_query::getCity(array('id_city' => $sors['id_city'])),
				"zal"				=> sql_query::getZal(array('id_zal' => $sors['id_zal'])),
				"sport"				=> sql_query::getSport(array('id_sport' => $sors['id_sport'])),
				"users"				=> sql_query::getSorUsers(array('id_sor' => $sors['id_sor'])),
				"cats"				=> sql_query::getSorCats(array('id_sor' => $sors['id_sor'])),
				"teams"				=> sql_query::getSorTeams(array('id_sor' => $sors['id_sor'])),
				"date"				=> $sors['date'],
				"date_start"		=> $sors['date_start'],
				"date_finish"		=> $sors['date_finish'],
				"date_reg"			=> $sors['date_reg'],
				"date_older"		=> $sors['date_older'],
				"id_user_author"	=> $sors['id_user_author'],
				"id_user_glavsud"	=> $sors['id_user_glavsud'],
				"id_user_glavsec"	=> $sors['id_user_glavsec'],
				"id_org"			=> $sors['id_org'],
			);
		};	
		
		return $data_sors;
	}

	public function getSorTeams($param=null){
		if($param['id_sor']){
			$teams_q=("
				SELECT 
					org_teams.*, sor_teams.result
				FROM  
					sor_teams, org_teams
				WHERE	
					sor_teams.id_sor = '".$param['id_sor']."'
					AND sor_teams.id_team = org_teams.id_team
			");
			//echo $teams_q;
			$teams_r = mysql_query($teams_q); 
			while($team = mysql_fetch_array($teams_r)){
			
				if($team['id_section']){
					$t = sql_query::getSections(array('id_section' => $team['id_section']));
				}elseif($team['id_club']){
					$t = sql_query::getClubs(array('id_club' => $team['id_club']));
				}elseif($team['id_city']){
					$t = sql_query::getCity(array('id_city' => $team['id_city']));
				}elseif($team['id_obl']){
					$t= sql_query::getObl(array('id_obl' => $team['id_obl']));
				}elseif($team['id_fedok']){
					$t= sql_query::getFedok(array('id_fedok' => $team['id_fedok']));
					$dop_tit = " {LT_FEDOK_L}";
				}else{
					$t=false;
				};

				
				$users = sql_query::getSorUsers(array('id_sor' => $param['id_sor'], "id_team" => $team['id_team']));
				
				$teams_data[] = array(
					"title"			=> $t[0]['title'].$dop_tit,
					"id_fedok"		=> $team['id_fedok'],
					"id_obl"		=> $team['id_obl'],
					"id_city"		=> $team['id_city'],
					"id_club"		=> $team['id_club'],
					"id_section"	=> $team['id_section'],
					"result"		=> $team['result'],
					"team_num_users"=> count($users),
				);
			};
		};
		return $teams_data;
	}
	
	public function getSorCats($param=null){
		if($param['id_sor']){
			$cats_q=("
				SELECT 
					DISTINCT sor_dist.sex, sor_age.title AS age_title,  sor_age.age_from, sor_age.age_to,  sor_age.id_sor_age 
				FROM  
					sor_dist, sor_age
				WHERE	
					sor_dist.id_sor = '".$param['id_sor']."'
					AND sor_age.id_sor_age = sor_dist.id_sor_age
			");
			$cats_r = mysql_query($cats_q); 
			while($cat = mysql_fetch_array($cats_r)){
				$cats_data[] = array(
					"sex"			=> $cat['sex'],
					"id_sor_age"	=> $cat['id_sor_age'],
					"age"			=> $cat['age_title'],
					"age_from"		=> $cat['age_from'],
					"age_to"		=> $cat['age_to'],
					"dist"			=> sql_query::getSorDist(array('id_sor' => $param['id_sor'], 'sex' => $cat['sex'], 'id_sor_age' => $cat['id_sor_age'])),
				);
			};
		};
		return $cats_data;
	}
	
	public function getClubs($param=null){
		if($param['id_club']){
			$limit=' LIMIT 1 ';
			$usl_id =" AND org_clubs.id_club='".$param['id_club']."'";
		}; 
		
		if($param['id_city']){
			$usl_city =" AND org_clubs.id_city='".$param['id_city']."'";
		};
		
		if($param['id_club_type']){
			$usl_type =" AND org_clubs.id_club_type='".$param['id_club_type']."'";
		};
		
		if($param['id_org']){
			$usl_org =" AND org_clubs.id_org='".$param['id_org']."'";
		};
		
		if($param['id_sport']){
			$usl_org =" FIND_IN_SET('".$param['id_sport']."',org_sections.id_sports)";
		};
		
	
		$clubs_q=("
			SELECT 
				DISTINCT org_clubs.id_club, org_clubs.*
			FROM  
				org_clubs, org_sections
			WHERE	
				org_clubs.arhiv='0'
				AND org_sections.id_club = org_clubs.id_club
				$usl_city
				$usl_id
				$usl_type
				$usl_org
			$limit
		");
		
		//echo $clubs_q;
		$clubs_r = mysql_query($clubs_q); 
		while($club = mysql_fetch_array($clubs_r)){
			$clubs_data[] = array(
				"title"			=> $club['title'],
				"id_club"		=> $club['id_club'],
				"id_city"		=> $club['id_city'],
				"id_club_type"	=> $club['id_club_type'],
				"id_org"		=> $club['id_org'],
			);
		};
		
		return $clubs_data;
	}
	
	public function getTreners($param=null){
		if($param['id_club']){
			$usl_club =" AND user_careers_org.id_club='".$param['id_club']."'";
		};

		$treners_q=("
			SELECT 
				*
			FROM  
				user_careers_org
			WHERE	
				user_careers_org.id_access_type='3'
				$usl_club
		");

		//echo $sections_q;
		$treners_r = mysql_query($treners_q); 
		while($trener = mysql_fetch_array($treners_r)){
			
			$users = sql_query::getUsers(array('id_user' => $trener['id_user']));
			$user = $users[0];
			
			$treners_data[] = array(
				"id_user"	=> $user['id_user'],
				"name"		=> $user['FIO'],
			);
		};
		return $treners_data;
	}
	
	public function getSections($param=null){
		if($param['id_section']){
			$limit=' LIMIT 1 ';
			$usl_id =" AND org_sections.id_section='".$param['id_section']."'";
		}elseif($param['id_club']){
			$usl_club =" AND org_sections.id_club='".$param['id_club']."'";
		};
	
	
		$sections_q=("
			SELECT 
				DISTINCT org_sections.title AS section_title, org_zal.title_kr AS zal_title, org_zal.* , org_sections.*
			FROM  
				org_sections, org_zal
			WHERE	
				org_zal.id_zal = org_sections.id_zal
				AND org_sections.arhiv='0'
				$usl_club
				$usl_city
				$usl_id
				$usl_type
				$usl_org
			$limit
		");
		//echo $sections_q;
		$sections_r = mysql_query($sections_q); 
		while($section = mysql_fetch_array($sections_r)){
			if($section['section_title']){
				$title = $section['section_title'];
			}else{
				$title = $section['title_kr'];
			};
			
			$sections_data[] = array(
				"title"			=> $title,
				"id_section"	=> $section['id_section']
			);
		};
		return $sections_data;
	}
	
	public function getSorDist($param=null){
		if($param['id_sor'] && $param['id_sor_age'] && $param['sex']){
			$dist_q=("
				SELECT 
					sport_dist.title AS dist_title, sor_dist.*, sport_dist.*
				FROM  
					sor_dist, sport_dist, sors
				WHERE	
					sors.id_sor = sor_dist.id_sor
					AND sor_dist.id_sor = '".$param['id_sor']."'
					AND sor_dist.sex = '".$param['sex']."'
					AND sor_dist.id_sor_age = '".$param['id_sor_age']."'
					AND sport_dist.id_sport_dist = sor_dist.id_sport_dist
					AND sport_dist.id_sport = sors.id_sport
				ORDER BY sport_dist.id_sport_dist ASC 
			");
			
			$dist_r = mysql_query($dist_q); 
			while($dist = mysql_fetch_array($dist_r)){
				$dist_data[] = array(
					"id_sport_dist"	=> $dist['id_sport_dist'],
					"title"			=> $dist['dist_title']
				);
			};
		};
		return $dist_data;
	}
	
	public function getSorUsers($param=null){

		if($param['id_sor']){
			if($param['id_team']){
				$usl_team = " AND sor_users.id_team = '".$param['id_team']."' ";
			};
				
			
			$user_q=("
				SELECT 
					*
				FROM  
					sor_users
				WHERE	
					sor_users.id_sor = '".$param['id_sor']."'
					$usl_team
				ORDER BY id_sport_dist ASC 
			");
			//echo $user_q;
			$user_r = mysql_query($user_q); 
			while($user = mysql_fetch_array($user_r)){
				$users[] = array(
					"id_sport_dist"	=> $user['id_sport_dist'],
					"id_sor_age"	=> $user['id_sor_age'],
					"lot"			=> $user['lot'],
					"info"			=> sql_query::getUsers(array('id_user' => $user['id_user']))
				);
			};
		};
		return $users;
	}
	
	public function getRangs($param=null){	
		
		if($param['id_rang']){
			$usl_type = "AND rangs.id_rang='".$param['id_rang']."'";
			$usl_limit = " LIMIT 1 ";
		}elseif($param['id_rang_type']){
			$usl_type = " AND rangs.id_rang_type='".$param['id_rang_type']."'";
		};

		$rang_q=("
			SELECT 
				rangs.title AS rang_title,rangs.title_kr AS rang_title_kr, rangs.*, rangs_type.*
			FROM  
				rangs, rangs_type
			WHERE	
				rangs.id_rang_type = rangs_type.id_rang_type
				$usl_type
			ORDER BY rangs.id_rang_type ASC 
			$usl_limit 
		");
		//echo $rang_q;
		$rang_r = mysql_query($rang_q); 
		while($rang = mysql_fetch_array($rang_r)){
			$data_rang[] = array(
				'id_rang' 			=> $rang['id_rang'],
				'title' 			=> $rang['rang_title'],
				'title_kr' 			=> $rang['rang_title_kr'],
				'id_rang_type' 		=> $rang['id_rang_type'],
			);
		};
		return $data_rang;
	}
	
	public function getZal($param=null){	
		
		if($param['id_zal']){
			$usl_zal = "AND org_zal.id_zal='".$param['id_zal']."'";
			$usl_limit = " LIMIT 1 ";
		}elseif($param['id_city']){
			$usl_zal = "AND org_zal.id_city='".$param['id_city']."'";
		};

		$zal_q=("
			SELECT 
				org_zal.title AS zal_title, geo_city.title AS city_title, org_zal.*
			FROM  
				org_zal, geo_city
			WHERE	
				org_zal.id_city = geo_city.id_city
				$usl_zal
			ORDER BY id_zal ASC 
			$usl_limit 
		");

		$zal_r = mysql_query($zal_q); 
		while($zal = mysql_fetch_array($zal_r)){
			$data_zal[] = array(
				'id_zal' 			=> $zal['id_zal'],
				'id_city' 			=> $zal['id_city'],
				'city'				=> $zal['city_title'],
				'adress' 			=> $zal['adress_street'].', '.$zal['adress_house'],
				'title'				=> $zal['zal_title'],
				'title_kr'			=> $zal['title_kr'],
			);
		};
		return $data_zal;
	}
	
	public function getCity($param=null){	
		if($param['id_city']>0){
			$usl_city = "AND geo_city.id_city='".$param['id_city']."'";
			$usl_limit = " LIMIT 1 ";
		}elseif($param['id_obl']){
			$usl_city = "AND geo_city.id_obl='".$param['id_obl']."'";
		};
		
		if($param['query']){
			$usl_city = "AND geo_city.title LIKE '".$param['query']."%'";
		};
		
		$city_q=("
			SELECT 
				geo_obl.title AS obl_title, geo_city.title AS city_title, geo_city.*
			FROM  
				geo_obl, geo_city
			WHERE	
				geo_city.id_obl = geo_obl.id_obl
				$usl_city
			ORDER BY geo_city.title ASC 
			$usl_limit 
		");
		//echo $city_q;
		$city_r = mysql_query($city_q); 
		while($city = mysql_fetch_array($city_r)){
		
			$data_city[]=array(
				"id_city"			=> $city['id_city'],
				"id_obl"			=> $city['id_obl'],
				"title"				=> $city['city_title'],
				"obl_title"			=> $city['obl_title'],
			);
		
		};
		return $data_city;
	}
	
	public function getObl($param=null){	
		if($param['id_obl']>0){
			$usl_obl = "AND geo_obl.id_obl='".$param['id_obl']."'";
			$usl_limit = " LIMIT 1 ";
		}elseif($param['id_city']){
			$usl_obl = "AND geo_city.id_city='".$param['id_city']."' AND geo_city.id_obl=geo_obl.id_obl";
			$usl_limit = " LIMIT 1 ";
			$usl_from = ", geo_city";
			$usl_select = ", geo_city.title AS city_title";
		}elseif($param['id_fedok']){
			$usl_obl = "AND geo_obl.id_fedok='".$param['id_fedok']."'";
		};
		
		$obl_q=("
			SELECT 
				geo_obl.title AS obl_title, geo_fedok.title AS fedok_title, geo_obl.* $usl_select
			FROM  
				geo_obl, geo_fedok $usl_from
			WHERE	
				geo_obl.id_fedok=geo_fedok.id_fedok
				$usl_obl
			ORDER BY geo_obl.title ASC 
			$usl_limit 
		");
		
		
		//echo $obl_q;
		$obl_r = mysql_query($obl_q); 
		while($obl= mysql_fetch_array($obl_r)){
		
			$data_obl[]=array(
				"id_city"			=> $obl['id_city'],
				"id_obl"			=> $obl['id_obl'],
				"id_fedok"			=> $obl['id_fedok'],
				"city_title"		=> $obl['city_title'],
				"fedok_title"		=> $obl['fedok_title'],
				"title"				=> $obl['obl_title'],
			);
		
		};
		return $data_obl;
	}
	
	public function getFedok($param=null){	
		if($param['id_fedok']>0){
			$usl_fedok = "WHERE geo_fedok.id_fedok='".$param['id_fedok']."'";
			$usl_limit = " LIMIT 1 ";
		}elseif($param['id_obl']){
			//$usl_fedok = "geo_obl.id_fedok=geo_fedok.id_fedok AND geo_obl.id_obl='".$param['id_obl']."'";
		};
		
		$fedok_q=("
			SELECT geo_fedok.title AS fedok_title, geo_fedok.*
			FROM  geo_fedok
			$usl_fedok
			ORDER BY geo_fedok.title ASC 
			$usl_limit 
		");
		//echo $fedok_q;
		$fedok_r = mysql_query($fedok_q); 
		while($fedok= mysql_fetch_array($fedok_r)){
		
			$data_fedok[]=array(
				"id_obl"			=> $fedok['id_obl'],
				"id_fedok"			=> $fedok['id_fedok'],
				"title"				=> $fedok['fedok_title'],
				"obl_title"			=> $fedok['obl_title'],
			);
		
		};
		return $data_fedok;
	}
	
	public function getSport($param=null){
		if($param['id_sport']){
			$usl_sport = " AND sports.id_sport='".$param['id_sport']."'";
			$usl_limit = " LIMIT 1 ";
		}elseif($param['id_sport_type']){
			$usl_sport = " AND sports.id_sport_type='".$param['id_sport_type']."'";
		}elseif($param['query']){
			$usl_sport = " AND sports.title LIKE '".$param['query']."%'";
		};
		
		$sport_q=("
			SELECT 
				sports.title AS sport_title, sport_type.title AS sport_type_title, sports.*
			FROM  
				sports, sport_type
			WHERE	
				sports.id_sport_type = sport_type.id_sport_type
				$usl_sport
			ORDER BY id_sport ASC 
			$usl_limit 
		");
		$sport_r = mysql_query($sport_q); 
		while($sport = mysql_fetch_array($sport_r)){
			$data_sport[]=array(
				"title"				=> $sport['sport_title'],
				"type_title"		=> $sport['sport_type_title'],
				"id_sport"			=> $sport['id_sport'],
				"id_sport_type"		=> $sport['id_sport_type'],
			);
		};
		
		return $data_sport;
	}
	
	public function getArticle($param=null){	
	
		$article_q = ("
			SELECT	users.name, users.second_name,  users.id_user, articles.*
			FROM users, articles
			WHERE
				articles.id_user = users.id_user
				AND articles.id_art = '".$param['id_art']."'
			LIMIT 1
		");

		$article_r = mysql_query($article_q); 
		$article = mysql_fetch_array($article_r);
		
		if($_ENV['id_user']){
			$article_fav_q = ("	SELECT	*	FROM articles_fav	WHERE	articles_fav.id_art = '$param[id_art]' AND articles_fav.id_user = '$_ENV[id_user]'	LIMIT 1	");
			$article_fav_r = mysql_query($article_fav_q); 
			$article_fav = mysql_num_rows($article_fav_r);
			
			if($article_fav){
				$art_fav_class = 'art_fav';
				$art_fav_event = 'artFavDel';
				$art_fav_title = '{LT_FAV_DEL}';
			}else{
				$art_fav_class = 'art_no_fav';
				$art_fav_event = 'artFavAdd';
				$art_fav_title = '{LT_FAV_ADD}';
			};
			
			if($article['id_user']==$_ENV['id_user']  || $_ENV['admin']){
				$buttonset = ' | <a href="/articles/edit?id_art='.$article['id_art'].'">{LT_EDIT}</a> | <a class="artDel" data-event="artDel">{LT_DEL}</a>';
			};
		};
		// Если статья не удалена
		if($article['visible']){
			$display_info = 'table-cell';
			$display_content = 'block';
			$display_rec = 'none';
			$class_art_visible = '';
		
		// Если статья удалена, и ее просмотривает администратор
		}elseif($_ENV['admin']){
			$display_info = $display_content= 'none';
			$display_rec = 'table-cell';
			$class_art_visible = 'deleting';
		
		// Если статья удалена, и ее просмотривает не администратор
		}else{
			$ART = $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "", "{NO_RESULT_TEXT}" => "{LT_ART_NO}"));
		};
		
		if(!$ART){
			$data_art = array(
				'{ART_ID}'				=> $param['id_art'],
				'{ART_TITLE}'			=> $article['title'],
				'{ART_TEXT}'			=> $article['text'],
				'{COMMENTS}'			=> $this->sql_query->getComments(array('id_art' => $param['id_art'])),
				'{AUTHOR}'				=> $article['id_user'],
				'{AUTHOR_NAME}'			=> $article['name'].' '.$article['second_name'],
				'{AVATAR}'				=> $this->sql_query->getAvatar(array('id_user' => $article['id_user'], 'type' => 'user', 'size' => 'small_rec')),
				'{DATE}'				=> convert_date($article['date']),
				'{ART_FAV_CLASS}'		=> $art_fav_class,
				'{ART_FAV_EVENT}'		=> $art_fav_event,
				'{ART_FAV_TITLE}'		=> $art_fav_title,
				'{BUTTONSET_EDIT_ART}'	=> $buttonset,
				'{DISPLAY_INFO}'		=> $display_info,
				'{DISPLAY_REC}'			=> $display_rec,
				'{DISPLAY_CONTENT}'		=> $display_content,
				'{CLASS_ART_VISIBLE}'	=> $class_art_visible,
				
			);
			$ART = $this->tpl->generate('articles_view', $data_art);
		};
		// Собираем данные о странице в целом
		$data = array(
			'{ARTICLES}' => $ART
		);
		
		return $data;
	}
	
	public function getArticles($param){
		$this->sql_query = new sql_query();
	
		// Условия
		if($param['id_user']){
			$usl_id_user = " AND articles.id_user = ".$param['id_user'];
			if($param['id_user']!=$_ENV['id_user']){
				$usl_visible = ' AND visible = 1';
			};
		};
		
		$limit_query = 'LIMIT 20';
		
		switch ($param['status']){
			case 1:
				// Мои заметки
				$limit_query = '';
			break;
			case 2:
				// Избранные заметки
				$usl_fav = " AND articles_fav.id_user = ".$param['id_user'];
				$from_dop = " RIGHT JOIN articles_fav ON articles_fav.id_art = articles.id_art";
			break;
			case 3:
				// Подписка на блог
				$usl_fav = " AND podpiska.id_user_to = articles.id_user ";
				$usl_id_user = " AND podpiska.id_user_from = ".$_ENV['id_user'];
				$from_dop = ", podpiska";
				$limit_query = '';
			break;

		};

		if($param['type']){		$usl_type = " AND articles.".$param['type']." = 1";		};
		if($param['id_art']){		$usl_id = " AND articles.id_art = ".$param['id_art'];		};
		
		// Запрос
		$articles_q = ("
			SELECT  articles.*, users.name, users.second_name
			FROM users, articles $from_dop
			WHERE
				articles.id_user = users.id_user
				$usl_id_user
				$usl_type
				$usl_id
				$usl_visible
				$usl_fav
			ORDER BY date DESC
			$limit_query
		");
		
		
		$articles_r = mysql_query($articles_q); 
		while($articles = mysql_fetch_array($articles_r)){
			
			if($articles['id_user']==$_ENV['id_user'] || $_ENV['admin']){
				$buttonset = ' | <a href="/articles/edit?id_art='.$articles['id_art'].'">{LT_EDIT}</a> | <a class="artDel" data-event="artDel">{LT_DEL}</a>';
			};
			// Если статья не удалена
			if($articles['visible']){
				$display_info = 'table-cell';
				$display_rec = 'none';
				$class_art_visible = '';
			
			// Если статья удалена, и ее просмотривает администратор
			}elseif($_ENV['admin']){
				$display_info = 'none';
				$display_rec = 'table-cell';
				$class_art_visible = 'deleting';
			};
			
			
			// Проверяем комментарии
			$num_comments = $this->sql_query->getComments(array('id_art' => $articles['id_art'], 'view' => 'count'));
			if($num_comments){$num_comments = "(".$num_comments.")";}else{$num_comments="";};
			
			// Обрезаем текст и форматируем его удаляя теги
			if($articles['anons']){
				$pretext = $articles['anons'];
			}else{
				$strlen = 400;
				$pretext = strip_tags($articles['text']);
				if(mb_strlen($pretext)>$strlen){
					$pretext = mb_substr($pretext,0,$strlen,'utf8')."... <a href='/article".$articles['id_art']."'>{LT_READ_MORE}</a>";
				};
			};
			
			// Собираем данные по каждому предосмотру статьи
			$data_art = array(
				'{ART_ID}'				=> $articles['id_art'],
				'{ART_TEXT}'			=> $pretext,
				'{ART_TITLE}'			=> $articles['title'],
				'{AVATAR}'				=> $this->sql_query->getAvatar(array('id_user' => $articles['id_user'], 'type' => 'user', 'size' => 'small_rec')),
				'{DATE}'				=> convert_date($articles['date']),
				'{AUTHOR}'				=> $articles['id_user'],
				'{AUTHOR_NAME}'			=> $articles['name'].' '.$articles['second_name'],
				'{COMMENT_NUM}'			=> $num_comments,
				'{BUTTONSET_EDIT_ART}'	=> $buttonset,
				'{DISPLAY_INFO}'		=> $display_info,
				'{DISPLAY_REC}'			=> $display_rec,
				'{CLASS_ART_VISIBLE}'	=> $class_art_visible,
			);
			$ARTS.= $this->tpl->generate('articles_preview', $data_art);
		};
		
		return $ARTS;
	}
	
	public function articlesEdit($param){

		if($param['id_art']){
			$article_q = ("
				SELECT	users.name, users.second_name,  users.id_user, articles.*
				FROM users, articles
				WHERE
					articles.id_user = users.id_user
					AND articles.id_art = '".$param['id_art']."'
				LIMIT 1
			");
			
			$article_r = mysql_query($article_q); 
			$article = mysql_fetch_array($article_r);
		
		};

		if(1==1){
			if($article['objav']){
				$sel_objav = 'selected';
			}elseif($article['news']){
				$sel_news = 'selected';
			}elseif($article['azbuka']){
				$sel_azbuka = 'selected';
			}elseif($article['tvor']){
				$sel_tvor = 'selected';
			}elseif($article['id_sor']){
				$sel_sor = 'selected';
			};
			
			if(!$article['id_sor']){
				$p_art_sor_hid = "style='display: none'";
			};
			
			$edit_type = "
			<p><label for='article_type'>{LT_ART_TYPE}:</label>
			<select id='article_type' name='article_type'>
				<option value='0'>{LT_SEL_NO_OPT}</option>
				<option value='objav' $sel_objav>{LT_OBJAV}</option>
				<option value='news' $sel_news>{LT_NEWS}</option>
				<option value='azbuka' $sel_azbuka>{LT_AZBUKA}</option>
				<option value='tvor' $sel_tvor>{LT_TVOR}</option>
				<option value='sor' $sel_sor>{LT_ART_SOR}</option>
			</select></p>
			
			<p $p_art_sor_hid id='p_article_id_sor'><label for='article_id_sor'>{LT_SOR}:</label>
			<select id='article_id_sor' name='article_id_sor'>
			
			</select></p>
			";
		};
		$data = array(
			'{ART_TEXT}'		=> $article['text'],
			'{ART_ID}'			=> $article['id_art'],
			'{ART_AUTHOR}'		=> $article['id_user'],
			'{ART_ANONS}'		=> $article['anons'],
			'{ART_TITLE}'		=> $article['title'],
			'{ART_EDIT_TYPE}'	=> $edit_type,
			
		);
		$ART = $this->tpl->generate('articles_edit', $data);
		return $ART;
	}
	
	public function getComments($param){
		$this->sql_query = new sql_query();
		
		if($param['id_art']){
			$usl_id = "AND comments.id_art = ".$param['id_art'];
		}elseif($param['id_photo']){
		//
		};
		
		if(!$_ENV['admin']){
			$usl_visible = "AND comments.visible = 1";
		};
		
		$comments_q = ("
			SELECT users.name, users.second_name, users.id_user, comments.*
			FROM comments, users
			WHERE
				comments.id_user = users.id_user
				$usl_id
				$usl_visible
			ORDER BY date DESC
		");
		$comments_r = mysql_query($comments_q);
		$num_comments = mysql_num_rows($comments_r);
		while($comments = mysql_fetch_array($comments_r)){
			if($comments['date_correct']!='0000-00-00 00:00:00'){
				$date = $comments['date_correct'];
				$date_lt = '{LT_DATE_EDIT}';
			}else{
				$date = $comments['date'];
				$date_lt = '{LT_DATE_RELEASE}';
			};
			
			$but_edit = '';
			if($_ENV['admin']){
				$but_edit.= ' | <a href="#" class="comment_del" data-event="commentDel">{LT_DEL}</a> | <a class="comment_edit">{LT_EDIT}</a>';
			}elseif($comments['id_user']==$_ENV['id_user']){
				$but_edit.= ' | <a href="#" class="comment_del" data-event="commentDel">{LT_DEL}</a>';
				if(!$i){
					$but_edit.= ' | <a class="comment_edit">{LT_EDIT}</a>';
				};
			};
			
			if($comments['id_user']!=$_ENV['id_user'] && $_ENV['id_user']){
				$but_edit.= ' | <a class="comment_otv">{LT_ANSWER}</a>';
				$i=1;
			};

			if(!$comments['visible']){
				$class_comment_visible = "deleting";
				$display_info = 'none';
				$display_rec = 'table-cell';
			}else{
				$class_comment_visible = "";
				$display_info = 'table-cell';
				$display_rec = 'none';
			};
			
			
			
			$data_comment = array(
				'{AVATAR}'				=> $this->sql_query->getAvatar(array('id_user' => $comments['id_user'], 'type' => 'user', 'size' => 'small_rec')),
				'{DATE}'				=> convert_date($date),
				'{AUTHOR}'				=> $comments['id_user'],
				'{AUTHOR_NAME}'			=> $comments['name'],
				'{AUTHOR_SECOND_NAME}'	=> $comments['second_name'],
				'{COMMENT_TEXT}'		=> $comments['text'],
				'{DATE_LT}'				=> $date_lt,
				'{COMMENT_BUT_EDIT}' 	=> $but_edit,
				'{COMMENT_ID}'			=> $comments['id_comment'],
				'{CLASS_COMMENT_VISIBLE}'=>$class_comment_visible,
				'{DISPLAY_INFO}'		=> $display_info,
				'{DISPLAY_REC}'			=> $display_rec,
				
			);
			$COMMENTS_ALL.= $this->tpl->generate('comment_view', $data_comment);
		};
		
		
		if($param['view']=='no_add'){
			$COMMENTS = $COMMENTS_ALL;
		}elseif($param['view']=='count'){	
			$COMMENTS = $num_comments;	
		}else{
			$data_comments = array(	
				'{AVATAR}'			=> $this->sql_query->getAvatar(array('id_user' => $_ENV['id_user'], 'type' => 'user', 'size' => 'small_rec')),
				'{AUTHOR}'			=> $_ENV['id_user'],
				'{COMMENTS}'		=> $COMMENTS_ALL	
			);
			$COMMENTS = $this->tpl->generate('comments', $data_comments);
		};
		
		return $COMMENTS;
	}
	
	public function getAvatar($param){
		if($param['type']=='user'){
			$file_name = "avatar";
			if($param['size']){
				$file_name = $file_name."_".$param['size'];
			};
			$avatar = "images/photo/users/".$param['id_user']."/".$file_name.".jpg";
			
			if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$avatar)){
				$avatar="images/photo/users/no_avatar.jpg";
			};
		};
		return $avatar;
	}
	
	public function getGroups($param){
		if($param['id_group']){				$usl_id = "AND org_groups.id_group = ".$param['id_group'];	$usl_limit = ' LIMIT 1 ';	
		}elseif($param['id_trener']){		$usl_trener = "AND org_groups.id_trener = ".$param['id_trener'];		
		}elseif($param['id_section']){		$usl_section = "AND org_groups.id_section = ".$param['id_section'];		
		}elseif($param['id_club']){			$usl_club = "AND org_sections.id_club = ".$param['id_club'];		
		};
		
			 
		$groups_q = ("
			SELECT geo_city.title, org_clubs.title, org_groups_type.title, org_groups.title, users.name, users.second_name, users.third_name, org_groups. *, org_zal.*, org_sections.*
			FROM geo_city, org_sections, org_groups, org_clubs, users, org_zal, org_groups_type
			WHERE org_groups.arhiv = 0	
				AND org_sections.id_club = org_clubs.id_club
				AND org_zal.id_city = geo_city.id_city
				AND org_groups.id_section = org_sections.id_section
				AND org_groups.id_trener = users.id_user
				AND org_zal.id_zal = org_sections.id_zal
				AND org_groups_type.id_groups_type = org_groups.id_groups_type
				$usl_section
				$usl_club
				$usl_trener
				$usl_id
			ORDER BY org_groups.title ASC
			$usl_limit
		");
		$groups_r = mysql_query($groups_q); 
		while($group = mysql_fetch_array($groups_r)){
			$data_group[] = array(
				'trener_name'			=> $group['name'],
				'trener_second_name'	=> $group['second_name'],
				'trener_third_name'		=> $group['third_name'],
				'id_trener'				=> $group['id_trener'],
				'club_title' 			=> $group[1],
				'id_club' 				=> $group['id_club'],
				'id_group'				=> $group['id_group'],
				'zal_title'				=> $group[0],
				'zal_adress_street'		=> $group['adress_street'],
				'zal_adress_house'		=> $group['adress_house'],
				'id_zal'				=> $group['id_zal'],
				'group_type'			=> $group[2],
				'group_age_from'		=> $group['age_from'],
				'group_age_to'			=> $group['age_to'],
				'group_title'			=> $group[3],
				'group_pn'				=> $group['pn'],
				'group_vt'				=> $group['vt'],
				'group_sr'				=> $group['sr'],
				'group_ch'				=> $group['ch'],
				'group_pt'				=> $group['pt'],
				'group_sb'				=> $group['sb'],
				'group_vs'				=> $group['vs'],
				'group_time'			=> $group['time'],
				'group_arhiv'			=> $group['arhiv'],
				'group_price_full'		=> $group['price_full'],
				'group_price_one'		=> $group['price_one'],
				'group_price_pt'		=> $group['price_pt'],
			);
		
		}
		return $data_group;
	}
	
	public function getGroupsStat($param){
		$data_attendance = array(
				// '{}'		=> '',
		);

		
		$GROUPS = $this->tpl->generate('groups_stat', $data_attendance);
		if(!$GROUPS){$GROUPS= $this->tpl->generate('no_result', array("{NO_RESULT_TITLE}" => "", "{NO_RESULT_TEXT}" => "{LT_GROUP_NO}"));};
		
		return $GROUPS;
	}
	
	public function getAttendance($param){

		// Получаем посещаемость в данном месяце
		$groups_attendance_q = ("
			SELECT org_groups_attendance.*, DAY(date) AS d
			FROM org_groups_attendance
			WHERE 
				org_groups_attendance.id_group = ".$param['id_group']."
				AND MONTH(date) = '".$param['month']."'
				AND YEAR(date) = '".$param['year']."'
		");
		$groups_attendance_r = mysql_query($groups_attendance_q); 
		while($groups_attendance = mysql_fetch_array($groups_attendance_r)){
			$id_user = $groups_attendance['id_user'];
			$status = $groups_attendance['status'];
			$d = $groups_attendance['d'];
			$array_atten[$id_user][$d] = $status;
		};


		return $array_atten;
		
	}	

	public function getMoney($param){
		if($param['id_user']>0){
			$usl_user = " AND oplata.id_user='".$param['id_user']."'";
		};
		
		$oplata_q=("
			SELECT 
				oplata.*,oplata_oper.*
			FROM  
				oplata,oplata_oper
			WHERE oplata_oper.id_oplata_oper = oplata.id_oplata_oper
				$usl_user
			ORDER BY id_user ASC
		");

		$oplata_r = mysql_query($oplata_q); 
		while($oplata = mysql_fetch_array($oplata_r)){
			$data_oplata[] = array(
				"id_oplata"				=> $oplata['id_oplata'],
				"id_user"				=> $oplata['id_user'],
				"oplata_summa"			=> $oplata['summa'],
				"id_oplata_oper"		=> $oplata['id_oplata_oper'],
				"oplata_month"			=> $oplata['opl_month'],
				"oplata_year"			=> $oplata['opl_year'],
				"oplata_date"			=> $oplata['date'],
				"oplata_type_title"		=> $oplata['title'],
				"id_group"				=> $oplata['id_group'],
			);
			

		};
		return $data_oplata;
	}
	
	public function getUsers($param){
		
		// Выборка по тренеру
		if($param['id_user']>0){
			$usl_id_user = " AND users.id_user = '".$param['id_user']."'";
			$LIMIT = " LIMIT 1 ";
		}else{
		
			// Выборка по архиву
			if($param['arhiv']){
				$usl_arhiv = " AND (user_careers_sport.date_out != 0 OR user_careers_sport.arhiv = '1'	OR org_groups.arhiv = '1')";
			}else{
				$usl_arhiv = " AND user_careers_sport.date_out = 0	AND user_careers_sport.arhiv = 0		AND org_groups.arhiv = 0 ";
			};
			
			// Выборка по группе
			if($param['nogroup'] && $param['id_trener']){
				$usl_group = " AND user_careers_sport.id_groups = 0 AND user_careers_sport.id_trener = '".$param['id_trener']."' ";
			}elseif($param['id_group']){
				$usl_group = " AND user_careers_sport.id_group = '".$param['id_group']."' ";
			}elseif($param['id_trener']){
				$usl_group = " AND org_groups.id_trener = '".$param['id_trener']."' ";
			};
			
			if($param['query']){
				$usl_query = " AND users.name LIKE  '".$param['query']."%'";
			};
			
			$ORDER = "ORDER BY id_group ASC, name ASC";
			
			$usl_trener_or_group = "AND user_careers_sport.id_user = users.id_user	AND user_careers_sport.id_group = org_groups.id_group AND org_sections.id_section = org_groups.id_section";
			$from_trener_or_group = ", org_groups, user_careers_sport, org_sections";
			$select_trener_or_group = " org_groups.title, org_groups.* , org_sections.price_full, ";
		};
	

		$user_q=("
			SELECT 
				geo_city.title, $select_trener_or_group geo_city.id_city, geo_obl.id_obl, users.* 
			FROM  
				users, geo_city, geo_obl $from_trener_or_group
			WHERE 
				geo_city.id_city = users.id_city
				AND geo_city.id_obl = geo_obl.id_obl
				$usl_query
				$usl_trener_or_group
				$usl_arhiv 
				$usl_group 
				$usl_id_user
			$ORDER
			$LIMIT
		");
		//echo $user_q;
		$user_r = mysql_query($user_q); 
		while($user = mysql_fetch_array($user_r)){
			$data_users[] = array(
				"careers_sport"			=> sql_query::getUserCareersSport(array('id_user' => $user['id_user'])),
				"careers_org"			=> sql_query::getUserCareersOrg(array('id_user' => $user['id_user'])),
				"sport_data"			=> sql_query::getUserSportData(array('id_user' => $user['id_user'])),
				"family"				=> sql_query::getUserFamily(array('id_user' => $user['id_user'])),
				"rangs"					=> sql_query::getUserRangs(array('id_user' => $user['id_user'])),
				"id_group" 				=> $user['id_group'],
				"group_title" 			=> $user[1],
				"group_money"			=> $user['price_full'],
				"id_user"				=> $user['id_user'],
				"name"					=> $user['name'],
				"FIO"					=> $user['name']." ".mb_substr($user['second_name'],0,1,'utf8').".".mb_substr($user['third_name'],0,1,'utf8').".",
				"second_name"			=> $user['second_name'],
				"third_name"			=> $user['third_name'],
				"phone"					=> $user['phone'],
				"phone_add"				=> $user['phone_add'],
				"born"					=> $user['born'],
				"polis"					=> $user['polis'],
				"passport"				=> $user['passport'],
				"biography"				=> $user['biography'],
				"id_city"				=> $user['id_city'],
				"id_obl"				=> $user['id_obl'],
				"city_title"			=> $user[0],
				"adress_street"			=> $user['adress_street'],
				"adress_house"			=> $user['adress_house'],
				"adress_block"			=> $user['adress_block'],
				"adress_flat"			=> $user['adress_flat'],
				"work"					=> $user['work'],
				"vk"					=> $user['vk'],
				"fb"					=> $user['fb'],
				"www"					=> $user['www'],
				"icq"					=> $user['icq'],
				"skype"					=> $user['skype'],
				"email"					=> $user['email'],
				"sex"					=> $user['sex'],
				"notepad"				=> $user['notepad'],
				"admin"					=> $user['admin'],
				"sex"					=> $user['sex'],
			);
			
			

		};
		
		return $data_users;
	}

	public function getUserSportData($param){
		$sport_data_q=("
			SELECT 
				poyas.title, DATEDIFF(CURDATE(),user_sport_data.dispanser) AS disp, user_sport_data.*
			FROM 
				user_sport_data LEFT JOIN poyas ON user_sport_data.id_poyas = poyas.id_poyas 
			WHERE 
				user_sport_data.id_user='".$param['id_user']."' 
			LIMIT 1
		");
		$sport_data_r = mysql_query($sport_data_q); 
		$sport_data = mysql_fetch_array($sport_data_r);
				
		// ЗАпрос диспансера
		if($sport_data['disp']=='0000-00-00'){
			$dispanser = "{LT_USER_NO_DISPANSER}";
		}elseif($sport_data['disp']){
			$dispanser = "{LT_USER_GO_DISPANSER} ".$sport_data['disp']." {LT_USER_LAST_DAY}";
		};

		$data_sport_data = array(
			"weight"				=> $sport_data['weight'],
			"height"				=> $sport_data['height'],
			"poyas"					=> $sport_data[0],
			"id_poyas"				=> $sport_data['id_poyas'],
			"dispanser"				=> $dispanser,
			"dispanser_date"		=> $sport_data['dispanser'],
			"info"					=> $sport_data['info'],
		);
			
		return $data_sport_data;
	}

	public function getUserCareersSport($param){
						// Запрос карьеры пользователя
				$careers = array();
				$career_q=("
					SELECT 
						org_clubs.title, sports.title, geo_city.title,  geo_city.id_city, user_careers_sport.*, users.name, users.second_name, users.third_name
					FROM 
						org_clubs, sports, users, user_careers_sport, geo_city
					WHERE 
						user_careers_sport.id_user='".$param['id_user']."' 
						AND users.id_user = user_careers_sport.id_trener
						AND org_clubs.id_club = user_careers_sport.id_club
						AND user_careers_sport.id_sport = sports.id_sport
						AND geo_city.id_city = org_clubs.id_city
					ORDER BY date_in DESC
				");
				$career_r = mysql_query($career_q); 
				while($career = mysql_fetch_array($career_r)){
					if($career['date_out']!='0000-00-00'){
						$date_out = convert_date($career['date_out']);
					}else{
						$date_out = 0;
					};
					if($career['id_trener']){
						$trener = "<a href='/user".$career['id_trener']."'>".$career['name']." ".mb_substr($career['second_name'],0,1,'utf8').".".mb_substr($career['third_name'],0,1,'utf8').".</a>";
					};
					
					
					$careers[] = array(
						'club' 		=> $career[0],
						'trener'	=> $trener,
						'id_trener'	=> $career['id_trener'],
						'id_club'	=> $career['id_club'],
						'id_sport'	=> $career['id_sport'],
						'id_city'	=> $career['id_city'],
						'id_group'	=> $career['id_group'],
						'date_in'	=> convert_date($career['date_in']),
						'date_out'	=> $date_out,
						'sport'		=> $career[1],
						'discount'	=> $career['discount'],
						'city'		=> $career[2],
					);
				};
		return $careers;
	}
	
	public function getUserCareersOrg($param){
						// Запрос карьеры пользователя
				$careers = array();
				$career_q=("
					SELECT 	user_careers_org.*
					FROM 	user_careers_org
					WHERE 	user_careers_org.id_user='".$param['id_user']."' 
					ORDER 	BY date_in DESC
				");
				$career_r = mysql_query($career_q); 
				//echo $career_q;
				while($career = mysql_fetch_array($career_r)){
					if($career['date_out']!='0000-00-00'){
						$date_out = convert_date($career['date_out']);
					};
					
					if($career['id_club']){
						$club_r = mysql_query("SELECT title FROM org_clubs WHERE org_clubs.id_club = '".$career['id_club']."'");
						$club = mysql_fetch_array($club_r);
						$id_club = $career['id_club'];
						$club_title = $club['title'];
					};
					
					if($career['id_org']){
						$org_r = mysql_query("SELECT title FROM orgs WHERE orgs.id_org = '".$career['id_org']."'");
						$org = mysql_fetch_array($org_r);
						$id_org = $career['id_org'];
						$org_title = $org['title'];
					};
					
					$careers[] = array(
						'id_org' 		=> $id_org,
						'org' 			=> $org_title,
						'id_club'		=> $id_club,
						'club'			=> $club_title,
						'title'			=> $career['title'],
						'access'		=> $career['id_access_type'],
						'date_in'		=> convert_date($career['date_in']),
						'date_out'		=> $date_out,
						
					);
				};
		return $careers;
	}
	
	public function getUserSettings($id_user){
		$user_config_q = ("SELECT * FROM user_settings WHERE id_user='".$id_user."' LIMIT 1");
		$user_config_r = mysql_query($user_config_q);
		$set =  mysql_fetch_array($user_config_r);
		
		return $set;
			
	}
	
	public function getUserFamily($param){

		$family_q=("
			SELECT 
				user_family.*
			FROM  
				user_family
			WHERE user_family.id_user = '".$param['id_user']."'
			LIMIT 1
		");

		$family_r = mysql_query($family_q); 
		$family= mysql_fetch_array($family_r);
		
		$family_data = array(
			'mother_phone' => $family['mother_phone'],
			'mother_work' => $family['mother_work'],
			'mother_name' => $family['mother_name'],
			'father_phone' => $family['father_phone'],
			'father_work' => $family['father_work'],
			'father_name' => $family['father_name'],
			'num_family' => $family['num_family'],
		);
		return $family_data;
	}
	
	public function getUserRangs($param){

			// Запрос списка званий
				$rang = array();
				$rangs_q=("
					SELECT 
						sports.title, rangs.title, user_rangs.*
					FROM 
						user_rangs,sports,rangs 
					WHERE 
						id_user='$param[id_user]' 
						AND sports.id_sport = user_rangs.id_sport 
						AND rangs.id_rang = user_rangs.id_rang
					ORDER BY user_rangs.id_rang DESC
				");
				$rangs_r = mysql_query($rangs_q); 
				while($rang = mysql_fetch_array($rangs_r)){
					if(!isset($rangs[$rang['id_sport']])){
						$rangs[$rang['id_sport']] = array(
							'sport' 		=> $rang[0],
							'id_sport' 		=> $rang['id_sport'],
							'id_rang'		=> $rang['id_rang'],
							'rang'			=> $rang[1],
							'date'			=> convert_date($rang['date']),
							'raport'		=> $rang['raport'],
							'number_doc' 	=> $rang['number_doc']
						);
					};
				};
		return $rangs;
	
	}
};

class selecter{

	public function gen_select_accessType($a=null){

		$SELECT.= "<select name='careers[org][accessType][]' class='select_accessType'><option value='0'>{LT_ACCESS}</option>";
		$access_type_r = mysql_query("SELECT * FROM access_type");
		while($access_type = mysql_fetch_array($access_type_r)){
			if($access_type['id_access_type'] == $a){$sel = 'selected';}else{$sel = '';};
			$SELECT.= "<option value='".$access_type['id_access_type']."' $sel>".$access_type['title']."</option>";
			
		};
		
		$SELECT.= "</select>";
		
		return $SELECT;
	}

	public function gen_select_rangs($r=null){

		$SELECT.= "<select name='careers[org][rangs][]' class='select_rangs'><option value='0'>{LT_RANGS}</option>";
		$rangs_r = mysql_query("SELECT rangs.title, rangs_type.title, rangs.* FROM rangs, rangs_type WHERE  rangs.id_rang_type = rangs_type.id_rang_type ORDER BY id_rang_type ASC");
		while($rangs = mysql_fetch_array($rangs_r)){
			if($rangs['id_rang'] == $r){$sel = 'selected';}else{$sel = '';};
			
			if($id_rang_type!=$rangs['id_rang_type']){$SELECT.="<optgroup label='".$rangs[1]."'>";};
			
				$SELECT.= "<option value='".$rangs['id_rang']."' $sel>".$rangs['title']."</option>";
			
			$id_rang_type = $rangs['id_rang_type'];
		};
		
		$SELECT.= "</select>";
		
		return $SELECT;
	}
	
	public function gen_select_year($y=null){

		if(!$y){$y = date('Y');};

		$SELECT.= "<select name='select_year' id='select_year'>";
		for ($i=2011; $i<=date('Y');$i++){
			if($i == $y){$sel = 'selected';}else{$sel = '';};
			$SELECT.= "<option value='".$i."' $sel>".$i."</option>";
		};
		$SELECT.= "</select>";
		
		return $SELECT;
	}
	
	public function gen_select_month($m=null){

		$month_title = selecter::get_month_array();
		if(!$m){$m = date('n');};
		
		// Месяц посещаемости. По умолчанию текущие
		$SELECT_MONTH.= "<select name='select_month' id='select_month'>";
		foreach($month_title as $key => $val){
			$month = $key+1;
			if($month == $m){$sel = 'selected';}else{$sel = '';};
			$SELECT_MONTH.= "<option value='".$month."' $sel>".$val."</option>";
		};	
		$SELECT_MONTH.= "</select>";
		return $SELECT_MONTH;
	}
	
	public function get_month_array($kr=null){
		if($kr){
			$month_title = array('Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек');
		}else{
			$month_title = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
		};
		return $month_title;
	}
	
	public function get_groups($param){
		if($param['id_trener']){
			$this->sql_query = new sql_query();
			$data_groups = $this->sql_query->getGroups(array('id_trener' => $param['id_trener']));// Запрос списка действующих групп для тренера
			
			$SELECT_MY_GROUP.="<select class='user_group' id='user_group_".$param['id_user']."' name='user_group[".$param['id_user']."]' data-id-group = '".$param['id_group']."'>
			<option value='0'>{LT_GROUP}</option>";

			foreach($data_groups AS $key => $group){
				$SELECT_MY_GROUP.="<option value='".$group['id_group']."'>".$group['group_title']."</option>";
			};
			$SELECT_MY_GROUP.="</select>";
			
			return $SELECT_MY_GROUP;
		};
	}
	
	
	public function get_poyas($p){
		$select_poyas="<select name='user_sport_data[poyas]'>";
		$select_poyas.="<option value=0>{LT_SEL_NO_OPT}</option>";
			$r_poyas= mysql_query("SELECT * FROM poyas") or die("Ошибка запроса федерального округа");  
			while($poyas = mysql_fetch_array($r_poyas)){
				if($poyas['id_poyas'] == $p){$sel = 'selected';}else{$sel = '';};
				$select_poyas.="<option value='".$poyas['id_poyas']."' ".$sel.">".$poyas['title']."</option>";
			};
		$select_poyas.="</select>";
		
		return $select_poyas;
	}
	
}

class select_linklist{
	public function generate_region($param){
		// Блок выбора города
		$select_region="<div><select data-defval='$param[fedok]' name='fedok' class='linklist autoload'>";
		$select_region.="<option value=0>{LT_FEDOK_ALL}</option>";
			$r_fedok = mysql_query("SELECT * FROM geo_fedok") or die("Ошибка запроса федерального округа");  
			while($fedok = mysql_fetch_array($r_fedok)){
				$select_region.="<option value='$fedok[id_fedok]'>$fedok[title]</option>";
			};
		$select_region.="</select>";
		$select_region.="<select data-defval='$param[obl]' data-linklist-name='obl' name='obl' class='linklist' disabled>";
		$select_region.="<option value=0>{LT_OBL_ALL}</option></select>";
		$select_region.="<select data-defval='$param[city]' data-linklist-name='city' name='city' class='linklist' disabled>";
		$select_region.="<option value=0>{LT_CITY_ALL}</option></select></div>";
		
		return $select_region;
	}
	
	public function generate_sport($param){
		// Блок выбора спорта	
		$select_sport="<div><select  data-defval='".$param['sport_type']."' data-linklist-name='sport_type' name='sport_type' class='linklist autoload' >";
		$select_sport.="<option value='0'>{LT_SPORTS_ALL_TYPE}</option>";
		$sport_type_r = mysql_query("SELECT * FROM sport_type");
		while($sport_type = mysql_fetch_array($sport_type_r)){
			$select_sport.="<option value='".$sport_type['id_sport_type']."'>".$sport_type['title']."</option>";
		};		
		$select_sport.="</select>";
		$select_sport.="<select data-defval='".$param['sport']."' data-linklist-name='sport' name='sport' class='linklist'  disabled>";
		$select_sport.="<option value='0'>{LT_SPORTS_ALL}</option></select></div>";
	
		return $select_sport;
	}

};

class SimpleImage{
 
   var $image;
   var $image_type;

   function load($filename) {
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      }
   }
   
   
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
      }
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   
   
   function output($image_type=IMAGETYPE_JPEG) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
	 // if (pathinfo($targetpath) == "jpg" || pathinfo($targetpath) == "jpeg" || pathinfo($targetpath) == "JPG"){
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image);
      }
   }
   function getWidth() {
       return imagesx($this->image);
   }
   function getHeight() {
       return imagesy($this->image);
   }
   function resizeToHeight($height) {
 
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
 
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
 
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }
 
   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
   }      
   
	function crop($x_val,$y_val,$width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, $x_val, $y_val, $width, $height, $width, $height);
      $this->image = $new_image;
   }  
 
}



