<?

function arts_date($date){
	global $tit_mon;
	$d=explode('.',convert_date($date));
	
	$mon=$d[1]-1;
	if($mon>=0){
		$month=$tit_mon[$mon];
	}else{
		$month='-';
	};
	$div_date="
		<div class='date'>
			<span class='month'>$month</span>
			<span class='day'>$d[0]</span>
			<span class='year'>$d[2]</span>
		</div>
	";
	return $div_date;
};


function edit_articles($id_art=0){
	// Подключение модулей редактора
	echo "<script type='text/javascript' src='libs/tiny_mce/tiny_mce.js'></script>";
	echo "<script type='text/javascript' src='libs/js/site_tinyMCE.js'></script>";
	
	// Редактирование заметки
	if($id_art){
		// Запрос заметки
		$art_q = ("SELECT * FROM articles WHERE id_art='$id_art'");
		$art_r = mysql_query($art_q) or die("Error");  
		$art = mysql_fetch_array($art_r);
		
		if($art[id_sor]>0){	$check_sor='checked';}else{$sor_hid='hidden';};
		if($art[news]){	$check_news='checked';};
		if($art[azbuka]){	$check_azbuka='checked';};
		if($art[tvor]){	$check_tvor='checked';};
		if($art[objav]){	$check_objav='checked';};
		if($art[statya]){	$check_statya='checked'; };
		
		if(!$art[news]){$file_hid='hidden';};
		if(!($art[statya] && $art[smi]==1)){$tr_publiс_bk_hid='hidden' ;};	
		if(!$art[statya]){$pub_hid='hidden' ;};	
		
		$title=$art[title];
		$text=$art[text];
		$anons=$art[anons];
		$id_sor=$art[id_sor];
		$id_art=$art[id_art];
		$author=$art[id_user];

	}else{ // Если новая заметка
		$pub_hid='hidden';
		$sor_hid='hidden';
		$file_hid='hidden';
		$tr_publiс_bk_hid='hidden';
		$tr_publiс_other_hid='hidden';
		$author=$_ENV[id_user];
	};

	// Селекты для дополнительных настроек
	// Соревнования
	$sor_q = ("SELECT date, id_sor, title FROM sor ORDER BY date DESC LIMIT 0,10");
	$sor_r = mysql_query($sor_q) or die("Query failed679711");  
	while($sor = mysql_fetch_array($sor_r)){
		$date=convert_date($sor[date]);
		if($sor[id_sor]==$art[id_sor]){$sel='selected';}else{$sel='';}
		$opt_sor.="<option value='$id_sor' $sel>$date / $sor[title]</option>";
	};
	
	// Разделы газеты Бойцовский клуб
	$bk_part_q = ("SELECT * FROM bk_part");
	$bk_part_r = mysql_query($bk_part_q) or die("Query failed6797111");  
	while ($bk_part = mysql_fetch_array($bk_part_r)){
		if($art[public_bk]==$bk_part[id_bk_part]){$sel='selected';}else{$sel='';};
		$opt_bk.="<option value='$bk_part[id_bk_part]' $sel>$bk_part[title]</option>";
	};
	
	// СМИ
	$opt_smi.="<option value=''>Не опубликовано</option>";
	$opt_smi.="<option value='0'>В другом издании</option>";
	$smi_q = ("SELECT * FROM smi");
	$smi_r = mysql_query($smi_q) or die("Query failed6797111");  
	while ($smi = mysql_fetch_array($smi_r)){
		if($art[smi]==$smi[id_smi]){$sel='selected';}else{$sel='';};
		$opt_smi.="<option value='$smi[id_smi]' $sel>$smi[title]</option>";
	};
	
	// Формируем ответ сервера
	$response.="
<form onsubmit='return arts_save();' align='center' action='?page=articles' method='post'>
	<table width='100%' id='art_edit_opt'>
		<tr>
			<td>
				<label for='art_title' class='art_label'>Название заметки</label>
				<input type='text' name='art_title' id='art_title' class='input_title' value='$title'>
				<input type='hidden' name='id_art' value='$id_art'>
				<input type='hidden' name='author' value='$author'>
			</td>
		</tr>
		<tr>
			<td>
				<label for='new_art_anons' class='art_label'>Анонс</label>
				<textarea name='art_anons' id='art_anons' class='input_title' rows='3' maxlength='200'>$anons</textarea>
			</td>
		</tr>
		<tr>
			<td>
				<label class='art_label'>Тип </label>
				<div class='buttonset'>
					<input type='checkbox' id='art_objav' name='art_objav' $check_objav><label for='art_objav'>Объявление</label>
					<input type='checkbox' id='art_news' name='art_news' $check_news><label for='art_news'>Новость</label>
					<input type='checkbox' id='art_azbuka' name='art_azbuka' $check_azbika><label for='art_azbuka'>Азбука</label>
					<input type='checkbox' id='art_statya' name='art_statya' $check_statya><label for='art_statya'>Статья</label>
					<input type='checkbox' id='art_tvor' name='art_tvor' $check_tvor><label for='art_tvor'>Творчество</label>
					<input type='checkbox' id='art_sor' name='art_sor' $check_sor><label for='art_sor'>Соревнование</label>
				</div>
			</td>
		</tr>
		<tr $file_hid id='tr_file'>
			<td>
				<label for='photo_news' class='art_label'>Фото (16x9):</label>
				<input type='file' name='photo_news' id='photo_news'>
			</td>
		</tr>
		<tr $pub_hid id='tr_pub'>
			<td>
				<label for='publiс_type' class='art_label'>Опубликовано в</label>
				<select name='publiс_type' id='publiс_type' class='input_title'>
					$opt_smi
				</select>
			</td>
		</tr>
		<tr $tr_publiс_bk_hid id='tr_publiс_bk'>
			<td>
				<label for='art_public_bk' class='art_label'>Раздел</label>
				<select name='art_public_bk' id='art_public_bk' class='input_title'>$option_0
					$opt_bk
				</select>
			</td>
		</tr>
		<tr hidden id='tr_public_new'>
			<td>
				<label for='art_public_new' class='art_label'>Новое издание:</label>
				<input type='text' name='art_public_new' id='art_public_new' class='input_title'>
			</td>
		</tr>
		<tr $sor_hid id='tr_id_sor'>
			<td>
				<label for='id_sor' class='art_label'>Соревнования</label>
				<select id='id_sor' name='id_sor' style='width: 505px;'><option value='0'>-</option>
					$opt_sor
				</select>
			</td>
		</tr>
	</table>
	<textarea id='art_text' name='art_text' class='editor'>$text</textarea>
	<input type='submit' value='Сохранить'></form>
";
	return $response;
};



function show_articles($id_art=0, $id_user=0, $sub_user=0, $fav=0, $news=0, $objav=1, $id_sor=0, $azbuka=0, $statya=0, $tvor=0, $num=5){
	
	// Если статья не указана
	if(!$id_art){
		// Если нужно вывести подписку
		if($sub_user && $id_user){
			$i=0;
			$sub_flag=true;
			$podpiska_q= ("SELECT * FROM podpiska WHERE id_user_from='$_ENV[id_user]'");
			$podpiska_r = mysql_query($podpiska_q); 
			while($podpiska = mysql_fetch_array($podpiska_r)){
				if($i>0){ $usl_type.=' OR ';};
				$usl_type.= "id_user=$podpiska[id_user_to]";
				$i++;	
			};
		// Если нужно вывести избранные заметки
		}elseif($id_user && $fav){
			$i=0;
			$fav_flag=true;
			$art_fav_q= ("SELECT * FROM articles_fav WHERE id_user='$id_user'");
			$art_fav_r = mysql_query($art_fav_q); 
			while($art_fav = mysql_fetch_array($art_fav_r)){
				if($i>0){ $usl_type.=' OR ';};
				$usl_type.= "id_art=$art_fav[id_art]";
				$i++;	
			};
		}elseif($id_user){
			$usl_id_art="id_user='$id_user'";
			$my_flag=true;
		}else{
			$usl_type='';
			if($news){$usl[]='news=1';};
			if($objav){$usl[]='objav=1';};
			if($id_sor){$usl[]='id_sor<>0';};
			if($azbuka){$usl[]='azbuka=1';};
			if($statya){$usl[]='statya=1';};
			if($tvor){$usl[]='tvor=1';};

				for($i=0; $i<count($usl); $i++){
					if($i>0){ $usl_type.=' OR ';};
					$usl_type.=" $usl[$i] ";
				};
		};
	}else{ // Если статья указан, то добвляем в зщапрос условие ID Статьи
		$usl_id_art="id_art='$id_art'";
	};
	
	$i=0; // Номер Статьи в запросе

		
		if($usl_type || $usl_id_art || $fav_flag || $sub_flag){// Если есть условия запроса, делаем
					

		$q_art= ("SELECT * FROM articles WHERE $usl_id_art $usl_type AND visible=1 ORDER BY id_art DESC LIMIT 0, $num");
		$r_art = mysql_query($q_art);
		$num_rows = mysql_num_rows($r_art);
		if($num_rows>0){
			while($arts = mysql_fetch_array($r_art)){
				unset($menu_type); // Очищаем разделы
				
				// Составляем разделы для статьи
				
				if($arts[news]){$menu_type[]="<a href='?page=articles&news'>Новости</a>";};
				if($arts[objav]){$menu_type[]=" <a href='?page=articles&objav'>Объявления</a>";};
				if($arts[id_sor]){$menu_type[]=" <a href='?page=articles&id_sor'>Соревнования</a>";};
				if($arts[azbuka]){$menu_type[]=" <a href='?page=articles&azbuka'>Азбука/a>";};
				if($arts[tvor]){$menu_type[]=" <a href='?page=articles&tvor'>Творчество</a>";};
				if($arts[statya]){$menu_type[]=" <a href='?page=articles&statya'>Статьи</a>";};

				
				$menu_type_href='';
				for($s=0; $s<count($menu_type); $s++){
					if($s>0){ $menu_type_href.=' | ';};
					$menu_type_href.=" $menu_type[$s] ";
				};
			
				$i++;// Номер статьи в запросе
				
				// Автор статьи
				$author_q= ("SELECT id_user,name,second_name FROM users WHERE id_user='$arts[id_user]'");
				$author_r = mysql_query($author_q) or die("Query failed123123"); 
				$author = mysql_fetch_array($author_r);
				
			
			// Если Неоткрыта статья		
			if(!$id_art){
				
				// Спрашиваем количество комментариев
				$comments_q= ("SELECT * FROM comments WHERE id_art='$arts[id_art]' AND visible=1 ORDER BY date DESC");
				$comments_r = mysql_query($comments_q) or die("ERROR"); 
				$comments_num = mysql_num_rows($comments_r);
				if($comments_num){$num_com='Комментарии ('.$comments_num.')';}else{$num_com='Комментировать';};
						
				if($arts[anons]!=''){
					$text="<p>$arts[anons]</p>";
				}else{
					$te=explode('</p>',$arts[text]);
					$text="$te[0]</p>";
				};
				$id_art_cur = $arts[id_art];
			}else{
				$text=$arts[text];
				$id_art_cur = $id_art;
			};
			
			// Стать в Избранное
			if($_ENV[id_user]){
				$art_fav_q= ("SELECT * FROM articles_fav WHERE id_art=$id_art_cur AND id_user='$_ENV[id_user]'");
				$art_fav_r = mysql_query($art_fav_q) or die("ERROR"); 
				$art_fav_num = mysql_num_rows($art_fav_r);
				if($art_fav_num){
					$art_fav_class='fav';
					$art_fav_event='artFavDel';
					$art_fav_title='Удалить из избранного';
				}else{
					$art_fav_class='no_fav';
					$art_fav_event='artFav';
					$art_fav_title='В избранное';
				};					
					
			};
			 
			
			if($i>$num){$hid='hidden';};
			$bl=ceil($i/$num);
				
				// Выводим статью
			if($my_flag){
				$date_art=convert_date($arts[date]);
				$user_avatar_thumb='';
				$text_art_hid='hidden';
			}else{
				$date_art = arts_date($arts[date]);
				$user_avatar_thumb=user_avatar_thumb($author[id_user],0);
				$text_art_hid='';
			};
			
				$response.="
					<table $hid class='article' id_art=$id_art_cur>
						<tr>
							<td>
								<table class='article_title' id='art_title_table_$arts[id_art]'>
									<tr>
										<td>
											<div class='title'><a href='?page=articles&id=$arts[id_art]'>$arts[title]</a>";
												if($author[id_user]==$_ENV[id_user] || $_ENV[id_user_type_ad]){
													$response.="
													<font style='font-size: 10px; font-weight: normal;'>(
														<a href='?page=articles&type=edit&id=$arts[id_art]' id='edit_news'>Редактировать</a>, 
														<a class='art_edit' data-event='artDel'>Удалить</a>
														<a class='art_edit' data-event='artRec' hidden>Восстановить заметку</a>)
													</font>";
												};
											$response.="</div>";
											if(!$my_flag){
												$response.="<div>";
												$response.="<a class='art_fav $art_fav_class' data-event='$art_fav_event' title='$art_fav_title'></a> | ";
												$second_name=mb_substr($author[second_name],0,1,'UTF-8');
												$response.="<a class='art_author' href='?page=user&id=$author[id_user]'>: $author[name] $second_name.</a>";
												if($menu_type_href!=''){$response.=" | <a class='art_menu'>:</a> $menu_type_href";};// Разделы статьи
												$response.="</div>";
											};
			$response.="
										</td>
										<td align=right>
											".$date_art."
										</td>
										<td>
											".$user_avatar_thumb."
										</td>
									</tr>
								</table>
							</td>
						</tr>
						";

			
			$response.="			
						<tr id='art_text_tr_$arts[id_art]' $text_art_hid >
							<td>
								<div class='article_text'>
									$text
								</div>";
		
		// Если обзор статьи то выводим кол-во комментариев и "Читать дальше"
				if($id_art==0){
					$response.="
					<table width='100%'><tr>
						<td><a href='?page=articles&id=$arts[id_art]'>Читать делее -> </a></td>
						<td align=right><a class='art_comment' href='?page=articles&id=$arts[id_art]#comments'>$num_com</a></td>
					</tr></table>
					";
				};
	$response.="	
							</td>
						</tr>
					</table>
	";
	
		};
		
		// Комментарии если открыта статья
		if($id_art){
			$response.=comments($id_art);
		};

	}else{ // Нет результатов запроса
		$response.="<div class='out_text'>Нет результатов поиска!</div>";
	};

}else{ // Нет корректных условий запроса
	$response.="<div class='out_text'>Ошибка запроса</div>";
};


return 	$response;
};
?>