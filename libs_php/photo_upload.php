<?php
header( 'content-type: text/html; charset="utf-8"' );
include_once('../libs_php/SimpleImage.php'); 
$image = new SimpleImage();
if( !isset($_FILES['file']) ) exit( "Ошибка: файлы не дошли." );
 
$errors = array();
$uploads_dir = "../files/photo/";
for( $i = 0, $length = count( $_FILES['file']['name'] ); $i < $length; $i++)
{
    // Если текущий элемент подмассива 'error' имеет значение отличное от 0
    // Значит с файлом проблемы...
    if( $_FILES['file']['error'][$i] !== 0 )
    {
        // Наполняем массив $errors - ключ это имя проблемного файла, а значение код ошибки.
        $errors[ $_FILES['file']['name'][$i] ] = $_FILES['file']['error'][$i];
        // Пропускаем итерацию:
        continue;
    }
    // Выводим информацию пользователю:

      
      $image->load($_FILES['file']['tmp_name'][$i]);
	  $image->resizeToWidth(100);
      //$image->output();
	  
  // Перемещаем файл из временной директории в постоянную 'upfiles' - директория должна существовать
  // и иметь соответствующие права, разрешающие запись в неё. К имени нового файла добавим префикс, 
  // состоящий из мд5 хеша времени загрузки в микросек. таким образом даже, если мы загрузим два идентичных
  // файла - ошибки задвоения не произойдет ( идея заимствована в Joomla ) 
					/*
					  move_uploaded_file( $_FILES['file']['tmp_name'][$i],
                      $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.
                      'upfiles'.DIRECTORY_SEPARATOR.md5( microtime() ).
                      '__'.$_FILES['file']['name'][$i] );
					*/
					  //$name = rand(1000,9999);
					  move_uploaded_file($_FILES["file"]["tmp_name"][$i], $uploads_dir.$_FILES["file"]["name"][$i]);
					  $image->save($uploads_dir."small_".$_FILES["file"]["name"][$i]);
				//	  echo '<img src="'.$uploads_dir.'small_'.$_FILES["file"]["name"][$i].'">';
					    echo '	<a class="fancybox" rel="group" href="'.$uploads_dir.$_FILES["file"]["name"][$i].'">
				<p>Файл успешно загружен : 
					<strong style="color:green"> '.$_FILES['file']['name'][$i].'</strong>
					- '.$_FILES['file']['size'][$i].' Byte
				</p>
				<img src="'.$uploads_dir.'small_'.$_FILES["file"]["name"][$i].'">
			</a>
			';  
					  
					  
}
 
// Смотрим были ли ошибки загрузки:
if( count($errors) > 0 )
{
    echo '<p>Эти файлы не были загружены: </p>';
     
        // $errors - ассоциативный массив, у которого ключ это имя проблемного файла,
        // а значение ключа - это код ошибки: 
        foreach( $errors AS $fileName => $error )
        {
            echo '<p>'.$fileName.' - <strong style="color:red">'.getError( $error ).'</strong></p>';
        }
}
 
/*
 * Получить описание ошибки по номеру.
 * @param string: $error - номер ошибки из иассива $_FILES['...']['error']
 * return string
 */
function getError( /*int*/ $error )
{
    switch( $error )
    {
        case UPLOAD_ERR_INI_SIZE   : ;
        case UPLOAD_ERR_FORM_SIZE  : return 'Файл слишком большой.';
          break;
         
        case UPLOAD_ERR_PARTIAL    : return 'Файл получен частично.';
            break;
             
        case UPLOAD_ERR_NO_FILE    : return 'Файл не был загружен.';
            break;
             
        case UPLOAD_ERR_NO_TMP_DIR : return 'Ошибка сервера: отсутствует временная папка.';
            break;
             
        case UPLOAD_ERR_CANT_WRITE : return 'Ошибка сервера: не удалось записать файл на диск.';
            break;
             
        case UPLOAD_ERR_EXTENSION  : return 'PHP-расширение остановило загрузку файла.';
            break;
    }
}
 
?>