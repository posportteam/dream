<?
$month_title_kr = array('Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек');
$option_0="<option value='0'>НЕ ВЫБРАНО</option>";

function sor_go($id_sport, $id_sor=0){
	$tpl = new Tpl();

	$response.=$tpl->generate("sor_tablo_".$id_sport, $team_data);
	
	return $response;
};

function showSorTeams($teams, $ajax=null){
	$tpl = new Tpl();
	$i=1;
	foreach($teams AS $key => $team){
		$team_data = array(
			"{TEAM_TITLE}"			=> $team['title'],
			"{TEAM_NUM}"			=> $i,
			"{TEAM_NUM_USERS}"		=> $team['team_num_users'],
			"{TEAM_RESULT}"			=> $team['result'],
			"{ID_TEAM}"				=> $team['id_team'],
			"{ID_SOR}"				=> $sor['id_sor'],
		);
		
		$response.=$tpl->generate("sor_reg_teams", $team_data, $ajax);
		$i++;
	};
	return $response;
};




function search_day_of_week_for_raspisanie($days, $y, $m = 0){

	if(!$y){$y = date("Y");};
	if(!$m){$m = date("n");};
	

	// Перевод в Юникс время, диапазон поиска
	$start = mktime(0, 0, 0, $m, 1, $y);
	$finish = mktime(0, 0, 0, $m, date('t',$start), $y);

	
	for($t=$start; $t<=$finish; $t+=86400){
		
		$day = date('j', $t);		//число
		$day_week = date('w', $t);	//день недели
		
		// Проверка в массиве допустимых дней недели из расписания
		if(in_array($day_week, $days)){
			$arr[$y][$m][] = $day;
		};
	};

	return $arr;
};

function number_posport($id_user, $obl){
	$serial = $obl;
	$number =  sprintf('%06d', $id_user);
	

	return $serial.' №'.$number;
};

function request_url(){
  $result = ''; // Пока результат пуст
  $default_port = 80; // Порт по-умолчанию
 
  // А не в защищенном-ли мы соединении?
  if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=='on')) {
	$result .= 'https://';// В защищенном! Добавим протокол...
	$default_port = 443; // ...и переназначим значение порта по-умолчанию
  } else {
    $result .= 'http://'; // Обычное соединение, обычный протокол
  }
  $result .= $_SERVER['SERVER_NAME'];// Имя сервера, напр. site.com или www.site.com
 
  // А порт у нас по-умолчанию?
  if ($_SERVER['SERVER_PORT'] != $default_port) {
    $result .= ':'.$_SERVER['SERVER_PORT']; // Если нет, то добавим порт в URL
  }
  
  // путь и GET-параметры).
  $result .= $_SERVER['REQUEST_URI'];
  // Результат
  return $result;
};

///// Напоминания
function show_notice($ret=0){
	if($_ENV[user_type_tr]){
		// Проверяем диспансер у спортсменов тренера
		
		
		$query = ("SELECT dispanser,name,second_name FROM users WHERE id_trener='$_ENV[id_user]' AND arhiv=0");
		$result = mysql_query($query) or die("Query failed1вапва");  
		$num_rows = mysql_num_rows($result);
		if ($num_rows>0){
			$text_notice.="<ul><h2>Спортcмены не прошедшие диспансер:</h2>";
			while($row = mysql_fetch_array($result)){
				if(srav_date($row['dispanser'])<=0){
					$text_notice.="<li>$row[name] $row[second_name]</li>";
				};
			};
			$text_notice.="</ul>";
			$num_notice++;
		};
	};
	

	if($ret){
		return $num_notice;
	}else{
		echo $text_notice;
	};
};

///// Отправка сообщений
function send_mess($mess, $theme, $to, $from=1,$sms=0, $email=0, $www=1, $file=0){
	// from - от кого
	// to - кому. Может быть передан массив ID
	// sms - Отправить копию по смс
	// email - Отправить копию по Мылу
	// www - Отправить копию на сайт
	// file - Прикрепить файл с директории сайта
	
	$file_path='files';
		
	
		//////////////////////////////////////////////////////////////
		///////////// Составляем списки Мыла и СМС для рассылки //////
		//////////////////////////////////////////////////////////////
		
		$phones='';$i=0;
		$to_q = ("SELECT id_user, email,telephone FROM users WHERE arhiv=0");
		$to_r = mysql_query($to_q) or die("Query failed1ss");    
		while($to_db = mysql_fetch_array($to_r)){
			
// Проверка на адреса из базы
		$idd=array_search($to_db[id_user], $to);
		
			if(gettype($idd)==integer){
				if($to_db[telephone]!=''){
					if($i!=0){$phones.=", ";};
					$phones.="+7$to_db[telephone]";
					$i++;
				};
				if($to_db[email]!=''){
					$emails[]="$to_db[email]";
				};
			};
		};
		
				
		//////////////////////////////////////////////////////////
		//////// Переводим в родительный падеж отправителя ////////
		///////////////////////////////////////////////////////////

		$us_q = ("SELECT name,second_name,third_name,email FROM users WHERE id_user='$from'");
		$us_r = mysql_query($us_q) or die("Query failed1s");    
		$us = mysql_fetch_array($us_r);
		
		$name=convert_name($us[name]);
		$second_name=convert_second_name($us[second_name]);
		$from_name="$name $second_name";
		if($us[email]!=''){
			$email_from=$us[email];
		}else{
			$email_from="admin@uniboy.ru";
		};
		//////////////////////////////////////////////
		//////// Процесс рассылки сообщений //////////
		//////////////////////////////////////////////
		$output_text.="Сообщение ";
		
				// WWW
		if($www){
			$date_send=date("Y-d-m");
			foreach ($to as $index => $val){
				$query="
					INSERT INTO  `message` (  `id_message` ,  `theme` ,  `from_id` ,  `to_id` ,  `text` ,  `status` ,  `date` ) 
					VALUES (NULL ,  '$theme', '$from',  '$val',  '$mess',  '0',  '$date_send');
				";
			mysql_query($query);
			};
			$output_text.= "отправлено пользователю на сайт";
		};
		
		
		
		
		// SMS
		if ($sms) {
		$output_text.=", ";
			include_once "scripts/smsc_api.php";
		// Сообщение
		$mess_sms="Сообщение от $from_name: $mess";
		// Отправляем
		//$sms_id = send_sms($phones, $mess_sms);
		// Проверяем доставлено ли сообщение по смс
			if ($sms_id > 0)
				$output_text.= "по SMS";
			else
				$output_text.= "НЕ отправлено по SMS (ошибка сервера)";
		};
		
		// E-mail
		if ($email) {
		$output_text.=", ";
			if(count($emails)>0){
			include "scripts/libmail.php"; // вставляем файл с классом
				$m= new Mail("utf-8"); // начинаем 
				$m->From( $email_from ); // от кого отправляется почта 
				$m->To( $emails ); // кому адресованно
				$m->Subject( "Uniboy.Ru: '$theme'" );
				$m->Body("$mess<br><br><i>C уваженеим, Администрация портала Uniboy.Ru (с)<br> Старухин Александр</i>", "html" );  
				//$m->Cc( "copy@asd.com"); // копия письма отправится по этому адресу
				//$m->Bcc( "bcopy@asd.com"); // скрытая копия отправится по этому адресу
				$m->Priority(3) ;    // приоритет письма
				if($file!=0){
					$m->Attach( "$file_path/$file", "image/gif" ) ; // прикрепленный файл
				};
				//$m->smtp_on( "smtp.yandex.ru", "staruhin", "20030", 587 ) ; // если указана эта команда, отправка пойдет через SMTP 
				$m->Send();    // а теперь пошла отправка
				$output_text.= 'на E-mail';
			}else{
				$output_text.= 'НЕ отправлено на E-mail!';
			};
		};
		
		return $output_text;
};

/////////////////////////////////////////
///// Генератор пароля //////////////////
/////////////////////////////////////////

function gen_pass($number)  {
    $arr = array('a','b','c','d','e','f','g','h','i','j','k','l',
                 'm','n','p','r','s','t','u','v','x','y','z',
                 'A','B','C','D','E','F','G','H','I','J','K','L',
                 'M','N','P','R','S','T','U','V','X','Y','Z',
                 '1','2','3','4','5','6','7','8','9');
    // Генерируем пароль
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
      // Вычисляем случайный индекс массива
      $index = rand(0, count($arr) - 1);
      $pass .= $arr[$index];
    }
    return $pass;
  }

/////////////////////////////////////////
//////// Конвреторы /////////////////////
/////////////////////////////////////////

function convert_date($date, $type=0){
	if(substr_count($date,'-')){
		$timestamp=explode(' ',$date);
		
		$date_f=explode('-',$timestamp[0]);
		$date="$date_f[2].$date_f[1].$date_f[0]";
		
		if(isset($timestamp[1])){
			$time_f=explode(':',$timestamp[1]);
			$time="$time_f[0]:$time_f[1]";
			if($type){
				return "$date в $time";
			}else{
				return "$date $time";
			};
		}else{
			return "$date";
		};
		
	}elseif(substr_count($date,'.')){
		$date_f=explode('.',$date);
		$date="$date_f[2]-$date_f[1]-$date_f[0]";
		return $date;
	}else{
		echo "неверный формат даты";
	};
};

function reform_mobile_phone($phone){
	$phone_new = preg_replace('/\D/i','',$phone);
	if(strlen($phone_new)>9){
		$phone_new=substr($phone_new,-10);	
		if(substr($phone_new,0,1)=='9'){
			return $phone_new;
		};
	};
}

function convert_title($title, $type){
	if($type=='p'){
		$str = array(
		"ый" => "ом",
		"ой" => "ом",
		"ир" => "ире",
		"ния" => "ниях",
		);
	}elseif($type=='d'){
		$str = array(
		"ый" => "ому",
		"ой" => "ою",
		"ий" => "ому",
		"кс" => "ксу",
		"ое" => "ому"
		);	
	}elseif($type=='r'){
		$str = array(
		"ск" => "ска",
		"ква" => "квы",
		"ская" => "ской",
		"сть" => "сти",
		"ий" => "ого"
		);	
	};
	return strtr($title,$str);
};

function convert_name($name){
	$c_name=mb_substr($name,-2,2,'UTF-8');
	if($c_name=='ий' || $c_name=='ей' || $c_name=='ой'){
		$name=str_replace($c_name,'ого',$name);
	}else{
		$name.='а';
	};
	return $name;
};

function convert_second_name($second_name){
	$second_name=trim($second_name);
	$c_second_name=mb_substr($second_name,-1,1,'UTF-8');
	$c_second_name_2=mb_substr($second_name,-2,2,'UTF-8');
	if($c_second_name=='й' || $c_second_name=='ь'){
		$second_name=str_replace($c_second_name,'я',$second_name);
	}elseif($c_second_name_2=='на'){
		$second_name=str_replace($c_second_name_2,'ны',$second_name);
	}elseif($c_second_name=='я'){
		$second_name=str_replace($c_second_name,'и',$second_name);
	}elseif($c_second_name=='а'){
		$second_name=str_replace($c_second_name,'и',$second_name);
	}else{
		$second_name.='а';
	};
	
	return $second_name;
};

function convert_third_name($third_name){
	$third_name.='а';
	return $third_name;
};

function sql2unix($datetime) {
    if ($datetime === '0000-00-00 00:00:00' || $datetime === '0000-00-00') return false;
    if (!preg_match('/^(\\d{4})-(\\d{2})-(\\d{2})(?: (\\d{2}):(\\d{2}):(\\d{2}))?$/', $datetime, $m)) return false;
    if (!isset($m[4])) $m[4] = $m[5] = $m[6] = 0;
    return gmmktime($m[4], $m[5], $m[6], $m[2], $m[3], $m[1]);
};

function srav_date($date_start,$date_finish='0000-00-00'){
	if($date_start){
		$date_cur = microtime(true);
		$date_start = sql2unix($date_start);
		$date_finish = sql2unix($date_finish);
		
		// В периоде
		if($date_start==$date_cur || ($date_start<=$date_cur && $date_cur<=$date_finish)){
			$response = 1;
		
		// Предстоит
		}elseif($date_start>$date_cur){
			$response = 2;
			
		// Прошло
		}else{
			$response = 0;
		};
		
		return $response;
		//return $date_cur.'/'.$date_start.'/'.$date_finish;
	};
};

function sbornaya_edit_show($id_user=0,$id){
	
	if(isset($id)){
	
	if($id>0){
	
		$text.="<input type=hidden name='id_sbornaya' value='$id'>";
		$query="SELECT * FROM sbornaya WHERE id_sbornaya='$id'";	
		$sbornaya = mysql_fetch_array(mysql_query($query));
	
		if($sbornaya[id_trener]){
			$trener = explode(',',$sbornaya[id_trener]);
			foreach ($trener as $key=>$val) {
				$q_user="SELECT * FROM users WHERE id_user='$val'";	
				$user = mysql_fetch_array(mysql_query($q_user));
				$q_sect = ("SELECT id_club FROM sections WHERE id_section='$user[id_section]'");
				$sect = mysql_fetch_array(mysql_query($q_sect));
				$q_club = ("SELECT title FROM clubs WHERE id_club='$sect[id_club]'");
				$club = mysql_fetch_array(mysql_query($q_club));
				$text.="<input type=hidden id='def_hid_trener_$val' second_name='$user[second_name]' name='$user[name]' value='$user[id_user]' club='$club[title]'>";
			};
		};
		
		if($sbornaya[id_user]){
			$users = explode(',',$sbornaya[id_user]);
			foreach ($users as $key=>$val) {
				$q_user="SELECT * FROM users WHERE id_user='$val'";	
				$user = mysql_fetch_array(mysql_query($q_user));
				$q_city = ("SELECT * FROM geo_city WHERE id_city='$user[id_city]'");
				$city = mysql_fetch_array(mysql_query($q_city));
				$q_sect = ("SELECT id_club FROM sections WHERE id_section='$user[id_section]'");
				$sect = mysql_fetch_array(mysql_query($q_sect));
				$q_club = ("SELECT title FROM clubs WHERE id_club='$sect[id_club]'");
				$club = mysql_fetch_array(mysql_query($q_club));
				$q_zvanie = ("SELECT title FROM zvanie WHERE id_zvanie='$user[id_zvanie]'");
				$zvanie = mysql_fetch_array(mysql_query($q_zvanie));
				$text.="<input type=hidden id='def_hid_users_$user[id_user]' zvanie='$zvanie[title]' club='$club[title]' value='$user[id_user]' ves='$user[ves]' city='$city[title]' second_name='$user[second_name]' name='$user[name]'>";
			};
		};
		
		if($sbornaya[id_fedok]){
			$q_fedok = ("SELECT * FROM geo_fedok WHERE id_fedok='$sbornaya[id_fedok]'");
			$fedok = mysql_fetch_array(mysql_query($q_fedok)); 
			$text.="<input type=hidden id='def_hid_org' title='$fedok[title]' value='$sbornaya[id_fedok]'>";
			$sel_fedok='selected';
		}elseif($sbornaya[id_obl]){
			$q_obl = ("SELECT * FROM geo_obl WHERE id_obl='$sbornaya[id_obl]'");
			$obl = mysql_fetch_array(mysql_query($q_obl));
			$q_fedok = ("SELECT title FROM geo_fedok WHERE id_fedok='$obl[id_fedok]'");
			$fedok = mysql_fetch_array(mysql_query($q_fedok)); 
			$text.="<input type=hidden id='def_hid_org' title='$obl[title]' fedok='$fedok[title]' value='$sbornaya[id_obl]'>";	
			$sel_obl='selected';			
		}elseif($sbornaya[id_city]){
			$q_city = ("SELECT * FROM geo_city WHERE id_city='$sbornaya[id_city]'");
			$city = mysql_fetch_array(mysql_query($q_city));
			$q_obl = ("SELECT title FROM geo_obl WHERE id_obl='$city[id_obl]'");
			$obl = mysql_fetch_array(mysql_query($q_obl));
			$text.="<input type=hidden id='def_hid_org' title='$city[title]' obl='$obl[title]' value='$sbornaya[id_city]'>";
			$sel_city='selected';				
		}elseif($sbornaya[id_club]){
			$q_club = ("SELECT * FROM clubs WHERE id_club='$sbornaya[id_club]'");
			$club = mysql_fetch_array(mysql_query($q_club));
			$q_city = ("SELECT title FROM geo_city WHERE id_city='$club[id_city]'");
			$city = mysql_fetch_array(mysql_query($q_city));
			$text.="<input type=hidden id='def_hid_org' title='$club[title]' city='$city[title]' value='$sbornaya[id_club]'>";
			$sel_club='selected';	
		};
		
	};
	
	$text_sbornaya_type.="<select id='sbornaya_type' name='sbornaya_type'>$option_0";
	$text_sbornaya_type.="<option value='fedok' $sel_fedok>Федерального округа</option>";
	$text_sbornaya_type.="<option value='obl' $sel_obl>Области</option>";
	$text_sbornaya_type.="<option value='city' $sel_city>Города</option>";
	$text_sbornaya_type.="<option value='club' $sel_club>Клуба</option>";
	$text_sbornaya_type.="</select>";

	
	$text_sbornaya_sports="<select name='sbornaya_sports'>";
	$q_sports = ("SELECT * FROM sports");
	$r_sports = mysql_query($q_sports) or die("Query failed123");  
	while($sports = mysql_fetch_array($r_sports)){
		if($sports[id_sports]==$sbornaya[id_sports]){$sel='selected';}else{$sel='';};
		$text_sbornaya_sports.= "<option value='$sports[id_sports]' $sel>$sports[title]</option>";
	};
	$text_sbornaya_sports.="</select>";
		
	$text.="
			<form id='form_sbornaya_edit' enctype='multipart/form-data' method='post'>
			<input type=hidden name='id_sbornaya' value='$id'>
			<input type=hidden name='user' value='$_ENV[id_user]'>
			<div class='body_tabs' id='sbornaya_tabs' align='center' style='width: 700px;'>
				<ul>
					<li><a href='#sbornaya_tabs_about'>Сборная</a></li>
					<li><a href='#sbornaya_tabs_users'>Участники</a></li>
				</ul>
				<div id='sbornaya_tabs_about' class='info'>
					<p id='p_sel_sbornaya_type'><label for='sbornaya_type'>Сборная</label>$text_sbornaya_type</p>
					<p>
						<label for='sbornaya_type'>Тренеры</label>$sbornaya_trener
						<input type='text' id='token_trener' name='token_trener' value=''>
					</p>
					<p><label for='sbornaya_sports'>Вид спорта</label>$text_sbornaya_sports</p>
				</div>
				<div id='sbornaya_tabs_users'>
					<table id='table_sbornaya_users'>
						<tr style='background: #cccccc; text-align: center; font-weight: bold; '>
							<td>Пользователь</td>
							<td>Вес</td>
							<td>Город</td>
							<td>Клуб</td>
							<td>Звание</td>
						</tr>
						<tr>
							<td><input type='text' name='sbornaya_users[]' id='token_user_1' value=''></td>
							<td><input type='number' min=15 max=200 step=1 name='ves[]' class='ves' value='' disabled></td>
							<td><input type='text' class='city' value='' disabled></td>
							<td><input type='text' class='club' value='' disabled></td>
							<td><input type='text' class='zvanie' value='' disabled></td>
						</tr>
					</table>
	
				</div>
				<input type='submit' name='sbornaya' value='Сохранить'>
			</div>
		
		</form>
	";
	}else{
	
	$text.="
				<table id='sbornaya_all' class='display' border=1>
					<thead>
						<tr>
							<th>Название</th>
							<th width=50>Вид</th>
							<th>Тренеры</th>
							<th width=200>Участники</th>
							<th width=100>Действия</th>
						</tr>
					</thead>
					<tbody>
	";
	
		$q_sbornaya = ("SELECT * FROM sbornaya");
		$r_sbornaya = mysql_query($q_sbornaya) or die("Query failed123"); 
		while($sbornaya = mysql_fetch_array($r_sbornaya)){
			$title="Сборная ";
			if($sbornaya[id_fedok]){
				$q_fedok = ("SELECT title FROM geo_fedok WHERE id_fedok='$sbornaya[id_fedok]'");
				$fedok = mysql_fetch_array(mysql_query($q_fedok)); 
				$r = convert_title($fedok[title], 'r');
				$title.="$r Федерального округа ";
			}elseif($sbornaya[id_obl]){
				$q_obl = ("SELECT * FROM geo_obl WHERE id_obl='$sbornaya[id_obl]'");
				$obl = mysql_fetch_array(mysql_query($q_obl));
				$r = convert_title($obl[title], 'r');
				$title.=$r;
			}elseif($sbornaya[id_city]){
				$q_city = ("SELECT title FROM geo_city WHERE id_city='$sbornaya[id_city]'");
				$city = mysql_fetch_array(mysql_query($q_city));
				$r = convert_title($city[title], 'r');
				$title.="города $r";
			}elseif($sbornaya[id_club]){
				$q_club = ("SELECT * FROM clubs WHERE id_club='$sbornaya[id_club]'");
				$club = mysql_fetch_array(mysql_query($q_club));
				$title.="СК $club[title]";
			};
			
			$q_sports = ("SELECT kr_title FROM sports WHERE id_sports='$sbornaya[id_sports]'");
			$sports = mysql_fetch_array(mysql_query($q_sports));
			
			$treners = explode(',',$sbornaya[id_trener]);
			$trener="<ol>";
			foreach ($treners as $key=>$val) {
				$q_user="SELECT id_user, name, second_name FROM users WHERE id_user='$val'";	
				$user = mysql_fetch_array(mysql_query($q_user));
				$trener.="<li><a href='?page=user&id_user=$user[id_user]'>$user[name] $user[second_name]</a></li>";
			};
			$trener.="</ol>";
			$sbornaya_users="<ol>";
			$users = explode(',',$sbornaya[id_user]);
			foreach ($users as $key=>$val) {
				$q_user="SELECT id_user, name, second_name FROM users WHERE id_user='$val'";	
				$user = mysql_fetch_array(mysql_query($q_user));
				$sbornaya_users.="<li><a href='?page=user&id_user=$user[id_user]'>$user[name] $user[second_name]</a></li>";
			};
			$sbornaya_users.="</ol>";
			$text.="
				<tr>
					<td>$title</td>
					<td align=center>$sports[kr_title]</td>
					<td>$trener</td>
					<td style=' text-align: center;'><span class='text_show'>Показать/Скрыть</span><div align='left' hidden>$sbornaya_users</div></td>
					<td></td>
				</tr>
			";
			
		};
	};
	$text.="</tbody></table>";
	
	echo $text;
};









	
/* 	
	
/////////////////////////////////////////
///////// Сообщения  ////////////////////
/////////////////////////////////////////
	
function list_mess($type){
				 echo "	
				<div id='panel_mess'>
					<div id='check'><b>Выделить:</b>

						<a id='check_all_$type'>Все</a>
						<a id='check_null_$type' style='display:none;'>Сбросить</a>
						<a id='check_0_$type'>Новые</a>
						<a id='check_1_$type'>Прочитаные</a>

					</div>
					<div id='check_event_$type' style='display:none;'>Удалить</div>
				</div>
				";
			
	
						if($type=='in'){
							$query_mess = ("SELECT * FROM message WHERE to_id='$_ENV[id_user]' ORDER BY ID_MESSAGE DESC");
						}else{
							$query_mess = ("SELECT * FROM message WHERE from_id='$_ENV[id_user]' ORDER BY ID_MESSAGE DESC");
						};
						
						$result_mess = mysql_query($query_mess) or die("Query failed123"); 
						$num_rows_mess = mysql_num_rows($result_mess);
						
						if($num_rows_mess>0){
							echo "<table width=100% class='table_message' cellpadding='0' cellspacing='0'>";
							while($row_mess = mysql_fetch_array($result_mess)){
								if($type=='in'){
									$query_from = ("SELECT * FROM users WHERE id_user=$row_mess[from_id]");
								}else{
									$query_from = ("SELECT * FROM users WHERE id_user=$row_mess[to_id]");
								};
								$result_from = mysql_query($query_from) or die("Query failed123"); 
								$row_from = mysql_fetch_array($result_from);
								
								if($row_mess[status]){
									echo"<tr class='row_table_mess_1' status='1' type='$type'>";
								}else{
									echo"<tr class='row_table_mess_0' status='0' type='$type'>";
								};
								
								$photo="images/photo/users/small/$row_from[id_user].jpg";
								$photo_no="images/photo/users/small/0.jpg";
								if(file_exists($photo)){
									$photo_from="<img src='$photo' height='60'>";
								}else{
									$photo_from="<img src='$photo_no' height='60'>";
								}
								
								if(trim($row_mess[theme])!=''){
									$theme="<font style='color: #cccccc; font-size: 10px;'>Тема:$row_mess[theme]</font><br>";
								}else{
									$theme='';
								};
								
								echo"
									<td width='30'><input type='checkbox' id='check_$row_mess[id_message]'></td>
									<td width='60'><a href='?page=user&sportsmens&id_user=$row_from[id_user]'>$photo_from</a></td>
									<td width='200' ><b><a href='?page=user&id_user=$row_from[id_user]'>$row_from[name] $row_from[second_name]</a></b><br><font size='1' color='#cccccc'>$row_mess[date]</font></td>
									<td class='text_mess' id='$row_mess[id_message]'>
									$theme
									$row_mess[text]</td>
									<td width='70'><a href='?page=user&message&delete_mess=$row_mess[id_message]'>Удалить</a></td>
								</tr>
								";
							};
							echo "</table>";
						}else{
							echo "<h2>Нет сообщений</h2>";
						};
				
	
	};
	
function show_mess($type=all){
		
		echo"
		
		<div class='body_tabs' id='messages' align='center'>
			<ul>
				<li><a href='#in_mess'>Входящие</a></li>
				<li><a href='#out_mess'>Исходящие</a></li>
				<li><a href='#add_mess'>Сообщение</a></li>
			</ul>
 
 			<div id='in_mess'>

			</div>
			
			<div id='out_mess'>

			</div>
 
			<div id='add_mess'>
				<div class='panel' id='0' name='div_new_mess'>
					Новое сообщение
				</div>
				<div id='new_mess'>	</div>
			</div>
		</div>
		";
		
		
	};
		
/////////////////////////////////////////
///////// Авторизация ///////////////////
/////////////////////////////////////////		
		
function auth_uniboy($type){
		echo"
			<script>
			function sendform() {
				var number = document.getElementById('telephone');
				var numberStr = String(number.value);
  				var reg = /[^\d]/;
				if (reg.test(numberStr) || numberStr=='') {
					alert ('Не верный формат телефонного номера.');
					document.getElementById('telephone').focus();
					return false;
				}else{
					return true;
				};
			};
		</script>
		
		<h1>Войти на сайт</h1>
		<form action='?page=auth' method='post' onsubmit='return sendform()' name='reg_user'>
		";
			if($type==2){
			echo"
			<p>
				<label>Фамилия:</label>
				<input id='reg_name' placeholder='Ваша фамилия' type='text' name='name'>
			</p>
			<p>
				<label>Имя:</label>
				<input id='second_name' placeholder='Полное имя' type='text' name='second_name'>
			</p>
			";
			};
			
			
			echo"
			<p>
				<label>Ваш действующий телефон:</label>
				<input id='telephone' maxlenght=10  placeholder='Мобильный телефон без 8' type='text' name='telephone'>
			</p>
			";
			
			
			if($type==1){
				echo"<p>
					<label>Пароль:</label>
					<input id='pass' maxlenght=10  placeholder='От 5 до 10 символов' type='password' name='pass'>
				</p>
				";
			};
			
			
			// Кнопка
			if($type==3){
				echo"
				<br><input type='submit'  name='vos_send_pass' id='vos_send_pass' value='Восстановить пароль'>
				";
			}elseif($type==1){
				echo"
				<br><input type='submit' name='reg_go' id='reg_go' value='Войти'>
				";
			}elseif($type==2){
				echo"
				<br><input type='submit'  name='reg_send_pass' id='vos_send_pass' value='Зарегестрироваться'>
				";
			};
			
			echo"
		</form>
		";
	};
 */


?>