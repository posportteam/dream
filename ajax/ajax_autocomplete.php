<?
header('Content-Type: text/html; charset=utf-8');
require '../config.php';
require '../libs_php/db_connect.php';
require '../libs_php/functions.php';
include '../libs_php/core.php';
include '../libs_php/classes.php';
require '../libs_php/user_connect.php';

$sql_query = new sql_query();

$response=array();

if($_GET['term']){
	$term = $_GET['term'];
}elseif($_POST['term']){
	$term = $_POST['term'];
};

$query = mb_strtolower(trim($term),"UTF-8");

switch ($_GET['action']){
	//запрос залов
	/* case "show_zal":
	
		if($_GET['type']=='token'){
			$query = mb_strtolower($_GET['zal'],"UTF-8");
			$zal_q = ("
				SELECT 		org_zal.*
				FROM		org_zal 
				WHERE 		org_zal.arhiv=0 AND (org_zal.title LIKE '%".$query."%' OR org_zal.title_kr LIKE '%".$query."%' OR org_zal.adress_street LIKE '%".$query."%')
				");
		}elseif($_GET['type']=='select'){
			$zal_q = ("
				SELECT 		org_zal.*
				FROM		org_zal, org_clubs, org_sections
				WHERE 		org_zal.arhiv=0 AND
							org_sections.id_zal=org_zal.id_zal AND			
							org_sections.id_club='".$_POST['id']."'
				");
		}elseif($_GET['type']=='id'){	
			$zal_q = ("
				SELECT 		org_zal.*, geo_obl.id_fedok, geo_obl.id_obl
				FROM		org_zal, geo_city, geo_obl
				WHERE 		org_zal.id_city=geo_city.id_city AND geo_city.id_obl=geo_obl.id_obl AND org_zal.arhiv=0 AND org_zal.id_zal='".$_POST['id']."'
				");
		};
		
		$zal_r = mysql_query($zal_q);
		while($zal = mysql_fetch_array($zal_r)){
			$response[]=array(
				"id" => $zal['id_zal'], 
				"title" => $zal['title'], 
				"kr_title" => $zal['kr_title'],
				"id_obl" => $zal['id_obl'],
				"id_fedok" => $zal['id_fedok'],
				"id_city" => $zal['id_city'], 
				"adress_id" => $zal['adress_id'],
				"adress_index" => $zal['adress_index'], 
				"adress_street" => $zal['adress_street'], 
				"adress_house" => $zal['adress_house'], 
				"phone" => $zal['phone']
				);
		};
	break; */
	
	
	// Запрос городов
	case "show_city":
		$q = $sql_query -> getCity(array('id_obl' => $_POST['id'], 'query' => $query));
		foreach ($q as $key => $city){
			$response[]=array(
				"id"	 => $city['id_city'],
				"value" => $city['title'], 
				"label" => $city['title'],
			);
		};
	break;

	// Запрос области
	case "show_obl":
		$q = $sql_query -> getObl(array('id_fedok' 	=>	$_POST['id']));
		foreach ($q as $key => $obl){
			$response[]=array(
				"id" => 	$obl['id_obl'], 
				"value" =>	$obl['title'],
				"label" =>	$obl['title'],
				);
		};
	break;
	
	
	// Запрос видов спорта
	case "show_sport":
		$q = $sql_query -> getSport(array('id_sport_type' => $_POST['id'] , 'query' => $query));
		foreach ($q as $key => $sport){
			$response[]=array(
				"id"	 	=> $sport['id_sport'], 
				"value" 	=> $sport['title'], 
				"label" 	=> $sport['title'],
				);
		};
	break;

	// Запрос Клуба
	case "show_club":
		$q = $sql_query -> getClubs(array('id_city' => $_POST['id']));
		foreach ($q as $key => $club){
			$response[]=array(
				"id" 		=> $club['id_club'],
				"value" 	=> $club['title'],
				"label" 	=> $club['title'],
				);
		};
	break;
	
	case "show_section":
		$q = $sql_query -> getSections(array('id_club' => $_POST['id']));
		foreach ($q as $key => $section){
			$response[]=array(
				"id" =>		$section['id_section'],
				"value" =>	$section['title'],
				"label" =>	$section['title'],
				);
		}; 
	break;
	
	
	// Запрос тренера
 	case "show_trener":
		$q = $sql_query -> getTreners(array('id_club' => $_POST['id']));
		foreach ($q as $key => $trener){
			$response[]=array(
				"id" => $trener['id_user'],
				"value" => $trener['name'],
				"label" => $trener['name'],
				);
		};
	break;
	
	// Запрос тренера
 	case "show_user":
		$q = $sql_query -> getUsers(array('id_user' => $_POST['id'], 'query' =>  $query));
		if($q){
			foreach ($q as $key => $user){
				// Проверка разряда по данному виду спорта
				$user_rang = '';
				if($user['rangs']){
					foreach($user['rangs'] AS $key => $rang){
						if($rang['id_sport']==$_POST['id_sport']){$user_rang = $rang['id_rang'];};
					};
				};
				
				// Собираем клуба, где приписан спортсмен (клуб должен быть не покинут)
				$clubs = '';
				if($user['careers_sport']){
					foreach($user['careers_sport'] AS $key => $career_sport){
						if(!$career_sport['date_out']){
							$clubs.=$career_sport['club'].' ';
						};
					};
					
					if($clubs){$clubs=', '.$clubs;};
				};
				
				
				$response[]=array(
					"id" 			=> $user['id_user'],
					"value" 		=> $user['name'],
					"label" 		=> $user['name'].' '.$user['second_name'].' ('.$user['city_title'].$clubs.')',
					"name"			=> $user['name'],
					"second_name"	=> $user['second_name'],
					"born"			=> convert_date($user['born']),
					"sex"			=> $user['sex'],
					"rang"			=> $user_rang
					);
			};
		};
	break;
	
	
	
	/* case "show_rayon":
		if($_GET[type]=='token'){
	
		}elseif($_GET[type]=='select'){
			$rayon_q = ("
				SELECT 		DISTINCT geo_rayon.title, geo_rayon.id_rayon
				FROM		geo_rayon, org_zal
				WHERE 		geo_rayon.id_rayon = org_zal.id_rayon AND org_zal.id_city = '$_POST[id]'
				");
		}elseif($_GET[type]=='id'){	

		};
		
		$rayon_r = mysql_query($rayon_q);
		while($rayon = mysql_fetch_array($rayon_r)){
			$response[]=array(
				"id" => $rayon[id_rayon], 
				"title" => $rayon[title]
				);
		};
	break; */
/* 
	
	case "show_group":
		if($_GET[type]=='token'){
			$group_q = ("");
		}elseif($_GET[type]=='select'){
			$group_q = ("
					SELECT 		DISTINCT org_groups.title, org_groups.id_group 
					FROM		org_groups, org_sections
					WHERE 		org_sections.id_zal='$_POST[id]' AND org_groups.id_section=org_sections.id_section
					");
			}elseif($_GET[type]=='id'){
			$group_q = ("");
		};
		$group_r = mysql_query($group_q);
		while($group = mysql_fetch_array($group_r)){
			$response[]=array(
				"id" => $group[id_group], 
				"title" => $group[title],
				);
		};
	break; */
	
	
			
/* 	case "show_user":
		if($_GET[type]=='token'){
			$user_q = ("");
		}elseif($_GET[type]=='select'){
			$user_q = ("
					SELECT 		DISTINCT  users.id_user, users.name, users.second_name
					FROM		users, org_groups
					WHERE 		org_groups.id_group='$_POST[id]' AND users.id_user=org_groups.id_trener
					");
			}elseif($_GET[type]=='id'){
			$user_q = ("");
		};
		$user_r = mysql_query($user_q);
		while($user = mysql_fetch_array($user_r)){
			$response[]=array(
				"id" => $user[id_user], 
				"name" => $user[name],
				"second_name" => $user[second_name],
				);
		};
	break; */
	
	/* case "show_org":
		$query = mb_strtolower($_GET['term'],"UTF-8");
		if($_GET['id_city']>0){$usl_city=" AND org_clubs.id_city = '".$_GET['id_city']."'";};

		$club_q = ("
			SELECT orgs.title_kr AS org_tit, org_clubs.title AS club_tit, orgs.*,  org_clubs.*
			FROM org_clubs LEFT JOIN orgs ON org_clubs.id_org = orgs.id_org
			WHERE org_clubs.title LIKE '%".$query."%' $usl_city
		");
		$club_r = mysql_query($club_q);
		while($club = mysql_fetch_array($club_r)){
			if($club['org_tit']){$org = " (".$club['org_tit'].")";}else{$org='';};
			$response[]=array(
				"id_club" => $club['id_club'], 
				"id_org" => $club['id_org'], 
				"value" => $club['club_tit'],
				"label" => $club['club_tit'].$org,
				);
		};
		
		if($_GET['id_city']>0){$usl_city=" AND orgs.id_city = '".$_GET['id_city']."'";};
		$org_q = ("
			SELECT orgs.title AS org_tit, orgs.title_kr AS org_tit_kr, orgs.*
			FROM orgs
			WHERE (orgs.title LIKE '%".$query."%' OR orgs.title_kr LIKE '%".$query."%') $usl_city
		");
		$org_r = mysql_query($org_q);
		while($org = mysql_fetch_array($org_r)){
			if($org['org_tit']){$org_tit=$org['org_tit'];}else{$org_tit=$org['org_tit_kr'];};
			$response[]=array(
				"id_club" => 0, 
				"id_org" => $org['id_org'], 
				"value" => $org_tit,
				"label" => $org_tit,
				);
		};
		
		
	break;
 */
	
	
};
	
	
	
	

print json_encode($response);
?>